// Phaser Konfiguration
var config = {
	//width: 1024,
	width: 1274,
	height: 576,
	renderer: Phaser.AUTO,
	antialias: true,
	multiTexture: true,
	enableDebug: true
};

const aufgaben = [];

aufgaben.push(IceRun);
aufgaben.push(IceRun_horizontal);
aufgaben.push(CTR);
aufgaben.push(Bugkiller);
aufgaben.push(CodePuzzle);

//aufgaben.push(GUI_Bau);
//aufgaben.push(Bonus_Run);
//aufgaben.push(RedBtn);

// Phaser Game Objekt
const game = new Phaser.Game(config);

//localStorage.clear();
const dashboard = new Dashboard();
const offset = -250;

//Hub Raum erstellen
var hub = new Raum(new Hub());
hub.key = 'Hub';
hub.up = 'OOP10';
hub.right = 'OOP20';
hub.down = 'MCI0';
hub.left = 'ISY0';

// Haupt Raum setzen
game.state.add('Hub', hub, true);

//Dungeons erstellen
var oop1Dungeon = new Dungeon(game, 'OOP1');
var oop2Dungeon = new Dungeon(game, 'OOP2');
var mciDungeon = new Dungeon(game, 'MCI');
var isyDungeon = new Dungeon(game, 'ISY');

var dungeons = {
	OOP1: oop1Dungeon,
	OOP2: oop2Dungeon,
	MCI: mciDungeon,
	ISY: isyDungeon
}
