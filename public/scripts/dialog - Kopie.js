class Dialog {

	constructor(_x,_y,headline,option1,option2,option3,option4) {
        this.width=240;
        this.height=80;
        this.x=_x+20;
        this.y=_y-20;
        
        this.style = { font: "16px Arial", fill: "black",  align: "center",};
         this.textstyle = {
              font: "16px Verdana",
              fill: "#ffffff",
              backgroundColor: "rgba(67, 72, 79, 0.5)",
              align: "left",
              wordWrap: true,
              wordWrapWidth: 250
         };
        
        this.x+=20;
        
        console.log(headline);
         this.header =headline;
        
         console.log(this.header);
  
        this.op1=option1;
        this.op2=option2;
        this.op3=option3;
        this.op4=option4;
        
            if(option1==""){
                this.width=120;
                this.height=80;

               this.dialogheader = game.add.text(this.x+this.width/3,
                                              this.y+this.height/2,
                                              this.header,this.textstyle);
      }
        else{
            
             this.dialogheader = game.add.text(this.x,
                                          this.y+16,
                                          this.header,this.textstyle);
        }
        
      //  this.bubble.width=2*this.width;
    //    this.bubble.height=2*this.height;
        
          this.antwort1     = game.add.text(this.x,
                                            this.y+2*this.height/3,
                                            this.op1,this.textstyle);
          this.antwort2     = game.add.text(this.x+this.width/2,
                                            this.y+2*this.height/3,
                                            this.op2,this.textstyle);
          this.antwort3     = game.add.text(this.x,
                                            this.y+this.height,
                                            this.op3,this.textstyle);
          this.antwort4     = game.add.text(this.x+this.width/2,
                                            this.y+this.height,
                                            this.op4,this.textstyle);
        
        this.antwortarray=[this.antwort1,this.antwort2,this.antwort3,this.antwort4];
        
        console.log("DEEEEBUG");
        console.log(option1);
        console.log(option1=="");
        console.log(option1.size);
         console.log(option1.size=="undefined");

        
        
        
        this.isActive=true;
        
        
    }
    
    
    
    
    hide(id){
        console.log("Hide");
      this.isActive=false;  
        
        this.dialogheader.setText("");
        this.antwort1.setText("");
        this.antwort2.setText("");
        this.antwort3.setText("");
        this.antwort4.setText("");
        
        if(id==1){
                
                this.width=120;
                this.height=80;
            //    this.bubble.width=2*this.width;
            //    this.bubble.height=2*this.height;
            
               this.dialogheader = game.add.text(this.x+this.width/3,
                                              this.y+this.height/2,
                                             "Richtige Antwort",this.textstyle);
            
           // this.dialogheader.setText("Richtige Antwort");
        }
        if(id==0){
                this.dialogheader = game.add.text(this.x+this.width/3,
                                              this.y+this.height/2,
                                             "Das war leider Falsch!\nDu kommst hier nicht vorbei.\nVerzieh dich!",this.textstyle);
            this.dialogheader.setText("Das war leider Falsch!\nDu kommst hier nicht vorbei.\nVerzieh dich!");
        }
        
        
    }
    
    
    destroy(){
      //  this.bubble.kill();
        this.dialogheader.setText("");
    }
    
    colorAnswer(id){
        for(var i=0;i<this.antwortarray.length;i++){
            if(i==id-1){
                this.antwortarray[i].addColor("blue", 0);
            }
            else{
                this.antwortarray[i].addColor("white",0);
            }
        }
    }
    
    

}
