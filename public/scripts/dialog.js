class Dialog {

	constructor(_x,_y,headline,option1,option2,option3,option4) {
        this.width=240;
        this.height=80;
        this.x=_x+20;

        //anpassen je nach gesamtheight
        this.y=_y-20;

      
        
        if((game.width-250)-this.x<350){
            this.x=(game.width-250) - 400;
        }
        
        this.style = { font: "16px Arial", fill: "black",  align: "center",};
         this.headerstyle = {
              font: "16px Verdana",
              fill: "#ffffff",
              backgroundColor: "rgba(67, 72, 79, 0.5)",
              align: "left",
              wordWrap: true,
              wordWrapWidth: 350
         };

        this.optionstyle = {
              font: "16px Verdana",
              fill: "#ffffff",
              backgroundColor: "rgba(67, 72, 79, 0.5)",
              align: "left",
              wordWrap: true,
              wordWrapWidth: 150
         };

        this.x+=20;

    
         this.header =headline;

      

        this.op1=option1;
        this.op2=option2;
        this.op3=option3;
        this.op4=option4;

        
        
        
        
        if(option1==""){
                this.width=120;
                this.height=80;

               this.dialogheader = game.add.text(this.x+this.width/3,
                                              this.y+this.height/2,
                                              this.header,this.headerstyle);
      }
        else{

             this.dialogheader = game.add.text(this.x,
                                          this.y+16,
                                          this.header,this.headerstyle);
        }
        
        

        
         this.antwort1     = game.add.text(this.x,
                                            this.dialogheader.y+16+this.dialogheader.height,
                                            this.op1,this.optionstyle);
          this.antwort2     = game.add.text(this.x+170,
                                            this.dialogheader.y+16+this.dialogheader.height,
                                            this.op2,this.optionstyle);


        var offset=Math.max(this.antwort1.height,this.antwort2.height);


          this.antwort3     = game.add.text(this.x,
                                            this.antwort1.y+offset+16,
                                            this.op3,this.optionstyle);
          this.antwort4     = game.add.text(this.x+170,
                                             this.antwort1.y+offset+16,
                                            this.op4,this.optionstyle);


        //Verschiebung des Blockes nach oben je nach gesamthöhe

        var complete_height=(2*(this.dialogheader.height+this.antwort1.height+this.antwort2.height+this.antwort3.height+this.antwort4.height)/3);
        
      
        
        
        
        //wenn platz ist zum hochschieben
        if(this.y>complete_height){
           
            this.dialogheader.y=this.dialogheader.y-complete_height;
            this.antwort1.y= this.antwort1.y-complete_height;
            this.antwort2.y= this.antwort2.y-complete_height;
            this.antwort3.y= this.antwort3.y-complete_height;
            this.antwort4.y= this.antwort4.y-complete_height;
        }
        else{
       
            var needed_offset=this.y-20;
            
        
            
            
            this.dialogheader.y=this.dialogheader.y-needed_offset;
            this.antwort1.y= this.antwort1.y-needed_offset;
            this.antwort2.y= this.antwort2.y-needed_offset;
            this.antwort3.y= this.antwort3.y-needed_offset;
            this.antwort4.y= this.antwort4.y-needed_offset;
        }

        



        this.antwortarray=[this.antwort1,this.antwort2,this.antwort3,this.antwort4];

        this.isActive=true;






    }




    hide(id){
      this.isActive=false;

        this.dialogheader.setText("");
        this.antwort1.setText("");
        this.antwort2.setText("");
        this.antwort3.setText("");
        this.antwort4.setText("");

        if(id==1){

                this.width=120;
                this.height=80;
          
               this.dialogheader = game.add.text(this.x+this.width/3,
                                              this.y+this.height/2,
                                             "Richtige Antwort",this.headerstyle);

         
        }
        if(id==0){
                this.dialogheader = game.add.text(this.x+this.width/3,
                                              this.y+this.height/2,
                                             "Das war leider Falsch!\nDu kommst hier nicht vorbei.\nVerzieh dich!",this.headerstyle);
            this.dialogheader.setText("Das war leider Falsch!\nDu kommst hier nicht vorbei.\nVerzieh dich!");
        }


    }


    destroy(){
        this.dialogheader.setText("");
    }

    colorAnswer(id){
        for(var i=0;i<this.antwortarray.length;i++){
            if(i==id-1){
                this.antwortarray[i].addColor("#ffcb0f", 0);
            }
            else{
                this.antwortarray[i].addColor("white",0);
            }
        }
    }



}
