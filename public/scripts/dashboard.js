class Dashboard{

    	constructor() {


              Phaser.Device.whenReady(function () {
            game.plugins.add(PhaserInput.Plugin);
            game.plugins.add(PhaserNineSlice.Plugin);
        });

            this.highscorevisible=false;
            this.isFinished=false;
            this.game=game;
		    this.global_points=0;
            this.global_text="";
            this.local_points=0;
            this.local_text="";
              this.proccess_text
            this.proccess;
            this.ghost_entry;
            this.anzahlRäume;
            this.user_copy;
            this.current_name;

            this.thema;
             this.runs_made_text;

              this.points_array={"OOP1":{local:0,global:0,multiplier:1,rooms_solved:0},
                           "OOP2":{local:0,global:0,multiplier:1,rooms_solved:0},
                           "MCI":{local:0,global:0,multiplier:1,rooms_solved:0},
                           "ISY":{local:0,global:0,multiplier:1,rooms_solved:0}};

            this.last_room="";
            this.old_topic;

            this.current_room="";
            this.current_dungeon="";

            this.dashflag=0;
            this.points_made;

            this.flag=0;

            //optional
            this.timer;
            this.timeLeft=0;
            this.tries_left;
            this.hearts_enabled=false;
            this.tries_left_text="";
            this.heartgroup;

            this.timerStyle = {
                font: "25px Verdana",
                fill: "black",
            }




                this.platz1;
                this.platz2;
                this.platz3;
                this.platz4;
                this.platz5;
                this.header;
                this.highscorearray;


        this.highscorearray=new Array(5);

              this.scorearray=[{name:'',score:0},{name:'',score:0},{name:'',score:0},{name:'',score:0},{name:'',score:0}];

            if(!localStorage.length>0){
                this.establishHighscore();
            }




    this.prevRoom = "none";
    this.textUpdated = false;

       }

      clearText(){
        this.global_text="";
        this.local_text="";
          this.current_dungeon="";
          this.current_room="";
    }


    setMultiplier(thema,m,isGlobal){


         var mp=(100+m)/100;

        if(isGlobal==="isGlobal"){
            var bonus_points=this.getglobalPoints(this.thema)*mp;

            var multi=mp-1;
            var old=this.getglobalPoints();
            var new_bonus=old*multi;



            this.updatePoints(new_bonus);

        }
        else{

            //Multiplikator für die nächsten Spiele


                if(this.thema==="OOP1"){
                    this.points_array.OOP1.multiplier=mp;
                }
                else if(this.thema==="OOP2"){
                    this.points_array.OOP2.multiplier=mp;
                }
                else if(this.thema==="MCI"){
                    this.points_array.MCI.multiplier=mp;
                }
                else if(this.thema==="ISY"){
                    this.points_array.ISY.multiplier=mp;
                }

                this.getMultiplier();

                 game.add.text(1064, 50,(""+mp).substring(2,4)+"0 % Bonus", { fontSize: '16px', fill: 'red' , font:'Georgia' });

        }



    }

    getMultiplier(){



         if(this.thema==="OOP1"){
            return(this.points_array.OOP1.multiplier);
        }
        else if(this.thema==="OOP2"){
            return(this.points_array.OOP2.multiplier);
        }
        else if(this.thema==="MCI"){
            return(this.points_array.MCI.multiplier);
        }
        else{
            return(this.points_array.ISY.multiplier);
        }
    }


    setLocalPoints(thema,score){



        var score_new;
        var isZero=false;

    if(score==0){
        score_new=0;
    }
        else{
            score_new=this.getLocalPoints(this.thema)+score;
            if(score_new<0){
                score_new=0;
                isZero=true;
            }
        }

        if(this.thema==="OOP1"){
            this.points_array.OOP1.local=score_new;
        }
        else if(this.thema==="OOP2"){
            this.points_array.OOP2.local=score_new;
        }
        else if(this.thema==="MCI"){
            this.points_array.MCI.local=score_new;
        }
        else if(this.thema==="ISY"){
            this.points_array.ISY.local=score_new;
        }



        return isZero;

    }



     setRoomsSolved(){
          if(this.thema==="OOP1"){

            this.points_array.OOP1.rooms_solved++;

        }
        else if(this.thema==="OOP2"){

            this.points_array.OOP2.rooms_solved++;

        }
        else if(this.thema==="MCI"){

            this.points_array.MCI.rooms_solved++;

        }
        else if(this.thema==="ISY"){

            this.points_array.ISY.rooms_solved++;

        }
     }

    getRoomsSolved(){
          if(this.thema==="OOP1"){
            return(this.points_array.OOP1.rooms_solved);
        }
        else if(this.thema==="OOP2"){
             return(this.points_array.OOP2.rooms_solved);
        }
        else if(this.thema==="MCI"){
             return(this.points_array.MCI.rooms_solved);
        }
        else{
             return(this.points_array.ISY.rooms_solved);
        }
     }

     setglobalPoints(thema,score){




        if(this.thema==="OOP1"){
            this.points_array.OOP1.global+=score;
        }
        else if(this.thema==="OOP2"){
            this.points_array.OOP2.global+=score;
        }
        else if(this.thema==="MCI"){
            this.points_array.MCI.global+=score;
        }
        else if(this.thema==="ISY"){
            this.points_array.ISY.global+=score;
        }
    }

    getglobalPoints(thema){


var to_return="";

        if(this.thema==="OOP1"){
            to_return=this.points_array.OOP1.global;
        }
        else if(this.thema==="OOP2"){
            to_return=this.points_array.OOP2.global;
        }
        else if(this.thema==="MCI"){
          to_return=this.points_array.MCI.global;
        }
        else {
            to_return=this.points_array.ISY.global;
        }


        return to_return;
    }


    getLocalPoints(thema){



        if(this.thema==="OOP1"){
            return this.points_array.OOP1.local;
        }
        else if(this.thema==="OOP2"){
            return this.points_array.OOP2.local;
        }
        else if(this.thema==="MCI"){
           return this.points_array.MCI.local;
        }
        else {
            return this.points_array.ISY.local;
        }
    }

     getnextScore(current_score){

       var i=0;
       var j=0;
         var temparray=[{name:'',score:0},{name:'',score:0},{name:'',score:0},{name:'',score:0},{name:'',score:0}];
       for (var a in localStorage) {
           if(j<5 && a.split("|")[0]===this.thema){
               var value_split = a.split("|");
               temparray[j].name=value_split[1];
               temparray[j].score=localStorage[a];
                j+=1;
          }
       }
           temparray= temparray.sort((a, b) => {
                return a.score-b.score
            });


        var index=-1;
         var returned=false;
        for(var i=0;i<5;i++){
            if(temparray[i].score!=0){
                index++;

                if( (temparray[i].score>current_score && index==0) || (temparray[i].score>current_score&&temparray[i-1].score<=current_score )){
                   returned=true;
                    return(temparray[i].score);
                }



        }

        }
    if(!returned){
            return "1.Platz :)";
            }


     }



    //wird zu Beginn jedes Spiels von der Aufgabe aufgerufen
    setPoints(thema,room,isHub){



        if(thema==="OOP1"||thema==="OOP2"||thema==="ISY"||thema==="MCI"){
             this.thema=thema;
        }
        else{

            this.thema=this.old_topic;
        }

        this.old_topic=this.thema;

        this.flag=0;
        this.hearts_enabled=false;

        if(!(room===this.last_room)){

            this.setLocalPoints(this.thema,0);


    }
        else{
            // console.log("Bonusrun raum wiederholung" + this.getLocalPoints(this.thema));
            }

        this.last_room=room;


        this.local_points=0;

        this.clearText();



        	var dashboardgroup = game.add.group();
		  dashboardgroup.create(1024,0, 'dashboard_background');





       if(isHub==true){

       }
        else{
             this.current_dungeon=game.add.text(1044, 165,this.thema+"-Dungeon", { fontSize: '18px', fill: '#000' , font:'Georgia',fontWeight:'bold'});

            this.current_room=game.add.text(1044, 35, room, { fontSize: '18px', fill: '#000' , font:'Georgia',fontWeight:'bold'});


            this.proccess_text=game.add.text(1044,255, "Du hast schon "+ this.getRoomsSolved() + " von "+this.anzahlRäume +"\n Räumen geschafft!", { fontSize: '16px', fill: '#000', font:'Georgia'  } );
            this.runs_made_text=game.add.text(1044,495,"Bisher geschaffte\n Durchläufe:"+this.getNumberOfRuns(), { fontSize: '16px', fill: '#000' , font:'Georgia' } );

            this.local_text=game.add.text(1064, 65, 'Levelpunkte:'+this.getLocalPoints(this.thema), { fontSize: '16px', fill: '#000' , font:'Georgia' });


                if(this.getMultiplier()!=1){


                    game.add.text(1064, 50,(""+this.getMultiplier()).substring(2,4)+"0 % Bonus", { fontSize: '16px', fill: 'red' , font:'Georgia' });


                }


                    this.global_text=game.add.text(1064, 195, 'Gesamtpunkte:'+this.getglobalPoints(this.thema), { fontSize: '16px', fill: '#000' , font:'Georgia' });
                    this.ghost_entry=game.add.text(1064, 225, 'nächster Rang:'+this.getnextScore(this.getglobalPoints(this.thema)), { fontSize: '16px', fill: '#000' , font:'Georgia' });


                }


    }


    updateRoom(){
        this.setRoomsSolved();
        this.proccess_text.setText("");
         this.proccess_text.setText("Du hast schon "+ this.getRoomsSolved() + " von "+this.anzahlRäume +"\n Räumen geschafft!");

    }





       updatePoints(localpoints){

        if(localpoints>0){

            let isZero=this.setLocalPoints(this.thema,Phaser.Math.roundTo(localpoints*this.getMultiplier(),0));
            if(!isZero){this.setglobalPoints(this.thema,Phaser.Math.roundTo(localpoints*this.getMultiplier(),0));}
            this.points_made=Phaser.Math.roundTo(localpoints*this.getMultiplier(),0);
            this.local_points+=Phaser.Math.roundTo(localpoints*this.getMultiplier(),0);
            this.global_points+=Phaser.Math.roundTo(localpoints*this.getMultiplier(),0);
        }
        else{
            let isZero=this.setLocalPoints(this.thema,localpoints);
            this.points_made=localpoints;
            if(!isZero){
                this.setglobalPoints(this.thema,localpoints);
            }

           this.local_points+=localpoints;
            this.global_points+=localpoints;
        }


        this.ghost_entry.setText('nächster Rang:'+this.getnextScore(this.getglobalPoints(this.thema)));
        this.local_text.setText('Levelpunkte:'+this.getLocalPoints(this.thema));
        this.global_text.setText('Gesamtpunkte:'+this.getglobalPoints(this.thema));

        if(localpoints<0 && this.hearts_enabled==true){

            this.tries_left=this.tries_left-1;
            this.heartgroup.removeChildAt(this.tries_left);
        }





        }

    //erstellt lifetimer oder bonusrun timer
    enable(id, timeLeft, bonusrun){
        if(id==1){
                    this.tries_left=3;
                    this.heartgroup = game.add.group();
                    game.add.text(1064,95,'Leben: ',{ fontSize: '16px', fill: '#000', font:'Georgia'  });
                    this.heartgroup.create(1124 ,  95, 'heart');
                    this.heartgroup.create(1164,  95, 'heart');
                    this.heartgroup.create(1204,  95, 'heart');

            this.hearts_enabled=true;

        }
        if(id==2){
            if(this.timeLeft==0){
                 this.timeLeft=timeLeft;
            //timer für bonusrun
            game.time.events.add(this.timeLeft, bonusrun.outOfTime, bonusrun);
            }



        }

    }


    updateTimer(bonusrun){
      this.timeLeft = game.time.events.duration;



      if(this.timer == null || bonusrun.raum.key != this.prevRoom){
        this.prevRoom = bonusrun.raum.key;
        this.timer = game.add.text(1064, 95, this.formatTime(Math.round((this.timeLeft)/1000)), this.timerStyle);
        this.timer.anchor.set(0.5);
        this.timer.position.x = this.timer.position.x + this.timer.width;
        this.timer.position.y = this.timer.position.y + this.timer.height;
        this.timerTween = game.add.tween(this.timer.scale).to({x: 1.07, y: 1.07}, 190, Phaser.Easing.Quadratic.InOut)
                                                         .to({x: 1.0, y: 1.0}, 190, Phaser.Easing.Quadratic.InOut)
                                                         .loop(true);
        this.timerTween.start();
      }
      else
        this.timer.setText(this.formatTime(Math.round((this.timeLeft)/1000)));



      if(this.timeLeft < 10000 && (!this.textUpdated || this.timer.colors[0] != "red")){
        this.timer.addColor("red", 0);
        this.timer.fontWeight = "bold";
        this.timerTween = game.add.tween(this.timer.scale).to({x: 1.07, y: 1.07}, 105, Phaser.Easing.Quadratic.InOut)
                                                         .to({x: 1.0, y: 1.0}, 105, Phaser.Easing.Quadratic.InOut)
                                                         .loop(true);
        this.timerTween.start();
        this.textUpdated = true;
      }

      if(this.flag==0){
        this.flag=1;
        game.time.events.add(this.timeLeft, bonusrun.outOfTime, bonusrun);
      }
    }

      formatTime(s) {
      var minutes = "0" + Math.floor(s / 60);
      var seconds = "0" + (s - minutes * 60);
      return minutes.substr(-2) + ":" + seconds.substr(-2);
  }


    establishHighscore(){


        localStorage.setItem("number_of_runs",0);

      localStorage.setItem("OOP1|Mark","100");
        localStorage.setItem("OOP1|Sarah","300");
        localStorage.setItem("OOP1|Florian","400");

        localStorage.setItem("OOP2|Laura","100");
        localStorage.setItem("OOP2|Felix","300");
        localStorage.setItem("OOP2|Sven","400");

         localStorage.setItem("MCI|Leo","100");
        localStorage.setItem("MCI|Julian","300");
         localStorage.setItem("MCI|Alex","400");

          localStorage.setItem("ISY|Peter","100");
         localStorage.setItem("ISY|Rick","300");
         localStorage.setItem("ISY|Steve","400");




    }


    clearHighscoreText(){

          if(this.highscorearray[0]!=null&&this.highscorearray[1]!=null&&this.highscorearray[2]!=null&&this.highscorearray[3]!=null&&this.highscorearray[4]!=null){
               this.highscorearray[0].destroy();
             this.highscorearray[1].destroy();
             this.highscorearray[2].destroy();
             this.highscorearray[3].destroy();
             this.highscorearray[4].destroy();
              this.header.destroy();
          }


    }

    showHighscore(thema){

        this.header=game.add.text(1064, 315,'Highscore', { fontSize: '18px', fill: '#000', font:'Georgia',fontWeight:'bold' });

        let underline = game.add.graphics(this.header.left, this.header.bottom - 5);
        let underline2= game.add.graphics(this.header.left, this.header.top + 22);


    underline.lineStyle(2, 0x000000);
    underline2.lineStyle(2, 0x000000);

    underline.moveTo(0, 0);
    underline2.moveTo(0, 0);

    underline.lineTo(this.header.width, 0);
     underline2.lineTo(this.header.width, 0);

         this.highscorearray[3]=game.add.text(1064, 345+i,'', { fontSize: '16px', fill: '#000' , font:'Georgia' });
         this.highscorearray[4]=game.add.text(1064, 345+i,'', { fontSize: '16px', fill: '#000' , font:'Georgia' });




       var i=0;
        var j=0;
       for (var a in localStorage) {

           if(j<5 && a.split("|")[0]===this.thema){

               var value_split = a.split("|");


               this.scorearray[j].name=value_split[1];
                this.scorearray[j].score=localStorage[a];



           j+=1;
          }
          }





            this.scorearray = this.scorearray.sort((a, b) => {
                return a.score-b.score
            });

        this.scorearray=this.scorearray.reverse();


        var m=0;

        for(var i=0;i<5;i++){
            if(this.scorearray[i].score!=0){

                if(this.scorearray[i].name==this.current_name){



                     this.highscorearray[i]=game.add.text(1064, 345+m,this.scorearray[i].name+' :  '+this.scorearray[i].score, { fontSize: '16px', fill: '#FF0000' , font:'Georgia' });

                    this.highscorearray[i].alpha = 0.1;

                    game.add.tween( this.highscorearray[i]).to( { alpha: 1 }, 2000, "Linear", true);
                }
                else{
                    this.highscorearray[i]=game.add.text(1064, 345+m,this.scorearray[i].name+' :  '+this.scorearray[i].score, { fontSize: '16px', fill: '#000' , font:'Georgia' });
                }






            }


             m+=30;
        }


    }

    exportHighscore(thema){



        var output="";

         output+=' \n Highscore\n';


         this.highscorearray[3]=game.add.text(1064, 335+i,'', { fontSize: '16px', fill: '#000' , font:'Georgia' });
         this.highscorearray[4]=game.add.text(1064, 335+i,'', { fontSize: '16px', fill: '#000' , font:'Georgia' });




       var i=0;
       var j=0;
       for (var a in localStorage) {
           if(j<5 && a.split("|")[0]===thema){
               var value_split = a.split("|");
               this.scorearray[j].name=value_split[1];
                this.scorearray[j].score=localStorage[a];
           j+=1;
          }
        }

            this.scorearray = this.scorearray.sort((a, b) => {
                return a.score-b.score
            });

        this.scorearray=this.scorearray.reverse();

        for(var i=0;i<5;i++){
            if(this.scorearray[i].score!=0)
                output+=this.scorearray[i].name+' :  '+this.scorearray[i].score+" \n";
        }

        return output;
    }




    removefromLocalStorage2(){


        if(this.scorearray[4].score!=0){
            for(var a in localStorage){

                if(localStorage[a]==this.scorearray[4].score){
                    localStorage.removeItem(a);

                }
            }
        }


    }

      removefromLocalStorage(){


          var old_value=1000;
          var key="";

          for (var a in localStorage){

              if(a.split("|")[0]===this.thema){

              if(localStorage[a]<old_value){
                  old_value=localStorage[a];
                    key=a;
              }
          }
          }

          window.localStorage.removeItem(key);


    }

    submitInput(name,spieler){


        this.highscorevisible=false;


                    spieler.isMovable=true;

                      localStorage.setItem(dashboard.thema+"|"+name,dashboard.getglobalPoints(dashboard.thema));
                    this.current_name=name;


             for (var a in localStorage) {
                 //-----------------------Dungeonlauf in localStorage erhöhen----------------

                    if(a==="number_of_runs"){
                        let i=localStorage[a];

                        i++;

                       localStorage.setItem(a,i);

                         this.runs_made_text.setText("Bisher geschaffte\n Durchläufe:"+i);


                       }

                    //---------------------------------------------------------------------------

       }







        this.clearHighscoreText();
       this.showHighscore(this.thema);





    }

    printOutName(name){
        // console.log(name);
    }


    checkHighscore(thema,spieler){



       this.thema=thema;
        var counter =0;
        var user;
        var item_above;
        var entry="";
        var number_of_runs_added=false;


            this.isFinished=true;
             for (var a in localStorage) {

                 if(a.split("|")[0]===this.thema){
                  counter++;
                 }



                if(a.split("|")[0]===this.thema&&this.getglobalPoints(this.thema)>localStorage[a]&&!this.hasBeenAdded){



             var login = game.add.text((game.width+offset) / 2, 100, 'Glückwunsch! Drücke Space um deinen\n Namen für den Highscore einzutragen:', {
                font: '30px Arial',
                fill: '#ffffff'
            });
            login.anchor.set(0.5);

            var userBg = game.add.nineSlice((game.width+offset) / 2+ 5, 180, 'input', null, 200, 50);
            userBg.anchor.set(0.5);

            user = game.add.inputField((game.width + offset) / 2 - 85, 180 - 17, {
                font: '18px Arial',
                fill: '#212121',
                fillAlpha: 0,
                fontWeight: 'bold',
                width: 150,
                max: 20,
                padding: 8,
                borderWidth: 1,
                borderColor: '#000',
                borderRadius: 6,
                placeHolder: 'Username',
                textAlign: 'center',
                zoom: true
            });



            user.blockInput = false;
            this.highscorevisible=true;

                     dashboard.hasBeenAdded=true;

                     spieler.isMovable=false;

                    game.input.keyboard.addKey(Phaser.Keyboard.ENTER).onDown.add(function() {

                        let name=user.value;



                        this.game=game;
                        // console.log("THIS");
                        // console.log(this);
                        user.endFocus();
                         user.destroy();
                        userBg.destroy();
                        login.destroy();


                    if(counter>=5){

                        dashboard.removefromLocalStorage();
                    }




                        dashboard.submitInput(name,spieler);




              }.bind(this));











                    user.startFocus();
                     this.user_copy=user;
                    this.user_copy.setText(this.user_copy.value+"");
                     this.user_copy.startFocus();


                }


       }




    }


    update(){

        if(this.highscorevisible){


        if(game.input.keyboard.addKey(Phaser.Keyboard.E).justDown){


            if(this.user_copy!=null){
            this.user_copy.setText(this.user_copy.value+"e");
            this.user_copy.startFocus();
        }
            else{
             //   (game.input.keyboard.addKey(Phaser.Keyboard.E).justDown).dispatch();
            }
        }
        else if(game.input.keyboard.addKey(Phaser.Keyboard.W).justDown){

             if(this.user_copy!=null){
            this.user_copy.setText(this.user_copy.value+"w");
            this.user_copy.startFocus();
        }
        else{
           // (game.input.keyboard.addKey(Phaser.Keyboard.W).justDown).dispatch();
        }}
             else if(game.input.keyboard.addKey(Phaser.Keyboard.A).justDown){

             if(this.user_copy!=null){
            this.user_copy.setText(this.user_copy.value+"a");
            this.user_copy.startFocus();
        }}
                  else if(game.input.keyboard.addKey(Phaser.Keyboard.S).justDown){

             if(this.user_copy!=null){
            this.user_copy.setText(this.user_copy.value+"s");
            this.user_copy.startFocus();
        } }
             else if(game.input.keyboard.addKey(Phaser.Keyboard.N).justDown){

             if(this.user_copy!=null){
            this.user_copy.setText(this.user_copy.value+"n");
            this.user_copy.startFocus();
        } }

         else if(game.input.keyboard.addKey(Phaser.Keyboard.D).justDown){

             if(this.user_copy!=null){
            this.user_copy.setText(this.user_copy.value+"d");
            this.user_copy.startFocus();
        }   }
            else if(game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR).justDown){

             if(this.user_copy!=null){
            this.user_copy.setText(this.user_copy.value+" ");
            this.user_copy.startFocus();
        }   }
    }
}

    getNumberOfRuns(){
        return localStorage["number_of_runs"];
    }



    }
