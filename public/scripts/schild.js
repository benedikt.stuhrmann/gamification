class Sign {

	constructor(x, y, rotation, sp, room, key) {
		this.x = x;
		this.y = y;
		this.rot = rotation;
		this.spieler = sp;
		this.raum = room;

		this.width = 0;
		this.height = 0;

		this.style = {
			font: "16px Verdana",
			fill: "#ffffff",
			backgroundColor: "rgba(67, 72, 79, 0.5)",
			align: "left",
			wordWrap: true,
			wordWrapWidth: 250
		}

		if (game.cache.checkImageKey(key)) {
			this.width = game.cache.getImage(key).width;
			this.height = game.cache.getImage(key).height;
			this.sign = game.add.sprite(this.x, this.y, key);
			this.sign.anchor.set(0.5);
			this.sign.angle = this.rot;
			this.raum.aufgabenLayer.add(this.sign);
		}

	}

	update() {
		//Schildinhalt vergrößern

		this.text.visible = (game.physics.arcade.distanceBetween(this.spieler, this) < 100 && this.spieler.isMovable);

	}

	setText(text) {
		this.text = game.add.text(this.x, this.y, text, this.style);
		this.positionText();
	}

	getText() {
		return this.text;
	}

	positionText() {
		this.text.setTextBounds(0 + (this.width / 2) + 5, 0 - (this.text.height / 2), 300, game.height - 50);

		//wenn nach rechts nicht genug Platz zum Anzeigen ist, auf der linken Seite anzeigen
		if (this.text.width > (game.width + this.raum.width_offset) - this.text.x)
			this.text.setTextBounds(-this.text.width - (this.width / 2) - 5, 0 - (this.text.height / 2), this.text.getBounds().width, this.text.getBounds().height);

		//wenn nach oben nicht genug Platz zum Anzeigen ist, nach unten verschieben
		if (this.text.y - (this.text.height / 2) < 0) {
			let overflow = 5 - (this.y - (this.text.height / 2));
			this.text.setTextBounds(0 + (this.width / 2) + 5, 0 - (this.text.height / 2) + overflow, this.text.getBounds().width, this.text.getBounds().height);
		}

		//wenn nach unten nicht genug Platz zum Anzeigen ist, nach oben verschieben
		if (this.text.y + (this.text.height / 2) > game.height) {
			let overflow = (this.text.y + (this.text.height / 2)) - game.height + 5;
			this.text.setTextBounds(0 + (this.width / 2) + 5, 0 - (this.text.height / 2) - overflow, this.text.getBounds().width, this.text.getBounds().height);
		}

		//wenn nach rechts und nach unten nicht genug Platz zum Anzeigen ist, nach links und oben verschieben
		if ((this.text.width > (game.width + this.raum.width_offset) - this.text.x) && (this.text.y + (this.text.height / 2) > game.height)) {
			let overflow = (this.text.y + (this.text.height / 2)) - game.height + 5;
			this.text.setTextBounds(-this.text.width - (this.width / 2) - 5, 0 - (this.text.height / 2) - overflow, this.text.getBounds().width, this.text.getBounds().height);
		}

		//wenn nach rechts und nach oben nicht genug Platz zum Anzeigen ist, nach links und unten verschieben
		if ((this.text.width > (game.width + this.raum.width_offset) - this.text.x) && (this.text.y - (this.text.height / 2) < 0)) {
			let overflow = 5 - (this.y - (this.text.height / 2));
			this.text.setTextBounds(-this.text.width - (this.width / 2) - 5, 0 - (this.text.height / 2) + overflow, this.text.getBounds().width, this.text.getBounds().height);
		}
	}

	destroy() {
		this.sign.destroy();
	}

}
