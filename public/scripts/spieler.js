class Spieler extends Phaser.Sprite {

	constructor(raum) {
		super(game, 0, 0, 'miner');

		//iv
		this.range = 32;

		this.anchor.set(0.5, 1);
		this.animations.add('left', [0, 1, 2, 3], 10, true);
		this.animations.add('right', [16, 17, 18, 19], 10, true);
		this.animations.add('up', [10, 11, 12, 13], 10, true);
		this.animations.add('down', [6, 7, 8, 9], 10, true);

		// sound
		this.steps_audio = game.add.audio('steps');

		game.add.existing(this);
		raum.spielerLayer.add(this);

		this.grabArea = game.add.sprite(0, 0);
		game.physics.arcade.enable(this.grabArea);
		this.grabRange = this.range;
		this.addChild(this.grabArea);

		this.body.collideWorldBounds = true;
		this.body.setSize(22, 10, 36, 86);

		this.raum = raum;

		this.speed = 250;

		// platziere den spieler
		this.spawn();


		// Objekt für aktions Keys
		this.actionKeys = game.input.keyboard.addKeys({
			'space': Phaser.KeyCode.SPACEBAR,
			'enter': Phaser.KeyCode.ENTER,
			'e': Phaser.KeyCode.E
		});

		//Variable für CTR um Spieler bei bestimmter Position zu stoppen
		//wird in der Bewegungssteuerung abgefragt
		this.isMovable = true;


	}

	update(){
        //console.log(this.isMovable);
        
		// steuerung aufsetzten
		let up = this.raum.cursors.up.isDown || this.raum.keyboard.isDown(Phaser.KeyCode.W);
		let down = this.raum.cursors.down.isDown || this.raum.keyboard.isDown(Phaser.KeyCode.S);
		let left = this.raum.cursors.left.isDown || this.raum.keyboard.isDown(Phaser.KeyCode.A);
		let right = this.raum.cursors.right.isDown || this.raum.keyboard.isDown(Phaser.KeyCode.D);

		// spieler bewegung stoppen
		// die Division ermöglicht das rutschen aus Eis.
		if (!this.isPushed) {
			this.body.velocity.x = this.body.velocity.x * 0.5;
			this.body.velocity.y = this.body.velocity.y * 0.5;

		}


		if((up||down||left||right)&&!this.steps_audio.isPlaying && this.isMovable){
				this.steps_audio.play();
		}


		// bewegungssteuerungs logik
		if (left && this.isMovable) {
			//  Move to the left
			this.body.velocity.x = -this.speed;

			if (!up && !down) this.animations.play('left');
		}
		if (right && this.isMovable) {
			//  Move to the right
			this.body.velocity.x = this.speed;

			if (!up && !down) this.animations.play('right');
		}
		if (up && this.isMovable) {

			this.body.velocity.y = -this.speed;

			this.animations.play('up');

		}
		if (down && this.isMovable) {

			this.body.velocity.y = this.speed;

			this.animations.play('down');

		}

		if (!(down || up || right || left)) {
			//  Stand still
			this.animations.stop();
			this.steps_audio.pause();
			this.frame = 8;
		}
	}

	// Fügt jeder aktions Taste die übergebene Funktion bei onDown hinzu.
	addAction(action, listenerContext) {
		Object.keys(this.actionKeys).forEach((key) => {
			this.actionKeys[key].onDown.add(action, listenerContext);
		});
	}


	// platziert den spieler an seiner Start-Postition
	// relativ zu seinem letzten Raum
	spawn() {

		// verbindungs namen
		let keys = Object.keys(this.raum.connections);

		let playerPos;

		// gibt es einen letzten Raum?
		if (game.cache.checkTextKey("lastRoom")) {
			// finde die position des letzten Raums
			keys.forEach((key) => {
				if (this.raum.connections[key] == game.cache.getText("lastRoom")) {
					this.spawnPos = key;
				}
			});
		}



		// setzte koordinaten zum letzten raum
		switch (this.spawnPos) {
			case "up":
				this.x = (game.width + this.raum.width_offset) / 2;
				this.y = 30;

				break;
			case "right":
				this.x = (game.width + this.raum.width_offset) - 30;
				this.y = game.height / 2;

				break;
			case "down":
				this.x = (game.width + this.raum.width_offset) / 2;
				this.y = game.height - 40;

				break;
			case "left":
				this.x = 30;
				this.y = game.height / 2;

				break;
			default:
				this.x = (game.width + this.raum.width_offset) / 2;
				this.y = game.height / 2;
		}
	}

	isInRange(sprite) {
		return game.physics.arcade.overlap(this.grabArea, sprite);
	}

	set grabRange(range) {
		this.range = range;
		this.grabArea.body.setCircle(range);
		this.grabArea.body.offset.set(-range);
	}

}
