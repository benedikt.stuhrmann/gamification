class GUI_Chest extends Chest{
  constructor(x, y, unlocked, raum, guiElementsInChest, guiElementsOnField, assignment){
    super(x, y, unlocked, raum);

    this.guiElementsInChest = guiElementsInChest;
    this.guiElementsInUse = guiElementsOnField;
    this.allElements = this.guiElementsInChest.slice();
    for(var i=0; i<this.guiElementsInUse.length; i++)
      this.allElements.push(this.guiElementsInUse[i]);

    this.slots = [];
    this.preview = {
      bg : null,
      icon : null,
      title : null,
      subtitle : null
    };
    this.assignment = assignment;
    this.menu = game.add.group();

    this.columns = 6;
    this.rows = 8;
    this.slotCount = this.columns * this.rows;
    this.inFocus = 0;

    this.style = {
      font: "25px Verdana",
      fill: "#ffffff",
      align: "left",
      wordWrap: true,
      wordWrapWidth: 325,
    };

    this.selected = null;
    this.firstTime = true;
    this.chest.animations.add('open', [5, 6, 7], 10, false);

    //Icons laden
    let loader = new Phaser.Loader(game);
    for(var i=0; i<this.allElements.length; i++){
      if(this.allElements[i].icon != null && this.allElements[i].icon_path != null)
        loader.image(this.allElements[i].icon, this.allElements[i].icon_path);
    }
    loader.onLoadComplete.addOnce(this.loaded, this);
    loader.start();
  }

  loaded(){
    this.unlocked = true;
    this.generateChestOverlay();
  }

  open(){
    this.chest.animations.play('open');
    this.slots[this.inFocus].focus.destroy();
    this.slots[this.inFocus].focus = this.selected = null
    this.updateSelection(0);
    this.opened = this.menu.visible = true;
  }

  close(){
    this.chest.animations.play('close');
    this.opened = this.menu.visible = false;
    if(this.slots[this.inFocus].item != null)
      this.selected = this.slots[this.inFocus].item;
  }

  generateChestOverlay(){
    //Hintergrund
    let bg = game.add.sprite((game.width/2)+(this.raum.width_offset/2), game.height/2, 'guiOverlay_bg');
    bg.anchor.set(0.5);
    this.menu.add(bg);

    //Überschrift
    let txt_truhe = game.add.text(200, 57, "Truhe", this.style);
    this.menu.add(txt_truhe);

    //slots
    for(var y=0; y<8; y++){
      for(var x=0; x<6; x++){

        let graphic = game.add.sprite(225+(49*x), 125+(49*y), 'slot');
        graphic.anchor.set(0.5);

        let slot = {
          bg : graphic,
          icon : null,
          focus : null,
          item : null,
        };

        this.menu.add(slot.bg);
        this.slots.push(slot);
      }//ende über Spalten
    }//ende über Reihen

    //Ausgewähltes Element hervorheben (Standard = 0 = erstes Element)
    let highlight = game.add.sprite(this.slots[0].bg.x, this.slots[0].bg.y, 'focus');
    highlight.anchor.set(0.5);
    this.slots[0].focus = highlight;
    this.menu.add(this.slots[0].focus);

    //Ausgewähltes GUI-Element Detail-Ansicht
    this.preview.bg = game.add.sprite(515, 100, 'preview');
    this.menu.add(this.preview.bg);

    //Aufgabentext
    let header = game.add.text(515, 220, "Aufgabenstellung:", this.style);
    header.fontSize = 19;
    this.menu.add(header);
    let assignment = game.add.text(515, 250, this.assignment, this.style);
    assignment.fontSize = 15;
    this.menu.add(assignment);

    this.fillWithElements();

    //Menü ausblenden
    this.menu.visible = false;
  }//end generateChestOverlay

  updateSelection(key){
    //löschen
    if(this.slots[this.inFocus].focus != null) this.slots[this.inFocus].focus.destroy(); this.slots[this.inFocus].focus = null;
    if(this.preview.icon != null) this.preview.icon.destroy();
    if(this.preview.title != null) this.preview.title.destroy();
    if(this.preview.subtitle != null) this.preview.subtitle.destroy();

    //aktualisieren
    if(key == "x+" && this.inFocus < this.slotCount-1) this.inFocus++;
    else if(key == "x-" && this.inFocus > 0) this.inFocus--;
    else if(key == "y+" && this.inFocus < this.slotCount-this.columns) this.inFocus += this.columns;
    else if(key == "y-" && this.inFocus > this.columns-1) this.inFocus -= this.columns;
    else if(key >= 0 && key <= this.slots.length) this.inFocus = key;

    //neu setzen
    let highlight = game.add.sprite(this.slots[this.inFocus].bg.x, this.slots[this.inFocus].bg.y, 'focus');
    highlight.anchor.set(0.5);
    this.slots[this.inFocus].focus = highlight;
    this.menu.add(this.slots[this.inFocus].focus);

    //preview anzeigen
    if(this.slots[this.inFocus].item != null){
      this.preview.icon = game.add.sprite(515, 100, this.slots[this.inFocus].item.icon);
      this.preview.title = game.add.text(625, 160, this.slots[this.inFocus].item.title, this.style); this.preview.title.fontSize = 19;
      this.preview.subtitle = game.add.text(625, 180, this.slots[this.inFocus].item.subtitle, this.style); this.preview.subtitle.fontSize = 15;
      this.menu.add(this.preview.icon);
      this.menu.add(this.preview.title);
      this.menu.add(this.preview.subtitle);
    }
  }//end updateSelection

  fillWithElements(){
    if(this.firstTime) this.guiElementsInChest.shuffle();
    this.firstTime = false;

    //GUI-Elemente in Truhen-Slots anzeigen
    for(var i=0; i<this.slots.length; i++){
      //Löschen
      if(this.slots[i].icon != null) this.slots[i].icon.destroy()
      if(this.slots[i].focus != null) this.slots[i].focus.destroy();
      this.slots[i].item = this.slots[i].icon = null;

      //Erzeugen
      if(this.guiElementsInChest.length > i){
        this.slots[i].item = this.guiElementsInChest[i];
        let ic = game.add.sprite(this.slots[i].bg.x, this.slots[i].bg.y, this.slots[i].item.icon);
        ic.anchor.set(0.5);
        ic.scale.setTo(0.5);
        this.slots[i].icon = ic;
        this.menu.add(this.slots[i].icon);
      }
    }
  }//end fillWithElements

  remove(item){
    //Item aus dem Inventar der Truhe löschen
    let index = this.guiElementsInChest.indexOf(item);
    this.guiElementsInUse.push(item);
    this.guiElementsInChest.splice(index, 1);
    this.firstTime = false;
    this.fillWithElements();
  }//end remove

    add(item){
    //Item in das Inventar der Truhe einfügen
    let index = this.guiElementsInUse.indexOf(item);
    this.guiElementsInChest.push(item);
    this.guiElementsInUse.splice(index, 1);
    this.firstTime = false;
    this.fillWithElements();
    }//end add

  resetChest(){
    this.guiElementsInChest = this.allElements.slice();
    this.guiElementsInUse = [];
    this.firstTime = true;
    this.fillWithElements();
  }//end resetChest
}//end class
