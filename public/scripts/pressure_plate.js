class Pressure_Plate extends Phaser.Sprite{

  constructor(x, y, graphicKey, raum){
    super(game, x, y, graphicKey);

    this.raum = raum;
		this.anchor.set(0.5);
		this.raum.aufgabenLayer.add(this);
    this.animations.add('activate', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12], 7, false);

    this.justSteppedOn = true;
    this.signalLeave = new Phaser.Signal();
    this.signalEnter = new Phaser.Signal();

    this.destination;
  }//end constructor

  //actionActivate wird getriggert, wenn der Spieler auf der Platte steht und die Animation abgeschlossen ist
  //actionDeactivate wird getriggert, wenn der Spieler von der Platte gesuchte
  //actionStepOn wird getriggert, wenn der Spieler die Platte berührt
	listen(actionActivate, actionDeactivate, actionStepOn, listenerContext){
    if(typeof actionActivate === 'string' || actionActivate instanceof String) { this.destination = actionActivate; actionActivate = this.teleportTo };
    if(actionDeactivate != null) this.signalLeave.addOnce(actionDeactivate, listenerContext);
    if(actionStepOn != null)     this.signalEnter.addOnce(actionStepOn, listenerContext);

    if(game.physics.arcade.overlap(this.raum.spieler, this)){
      if(this.justSteppedOn){
        if(actionStepOn != null) this.signalEnter.dispatch();
        this.anim = this.animations.play('activate');
        this.anim.onComplete.add(actionActivate, listenerContext);
        this.justSteppedOn = false;
      }
    }
    else if(this.anim != null){
      if(this.anim.currentFrame != null) this.anim.frame = 0;
      if(!this.justSteppedOn && actionDeactivate != null) this.signalLeave.dispatch();
      this.justSteppedOn = true;
    }
	}//end addAction

  teleportTo(){
    this.raum.spieler.isMovable = false;

		game.add.tween(this.raum.spieler).to({alpha: 0}, 1000, null, true)
		.chain(game.add.tween(this.raum.spieler).to({alpha: 1}, 500, null)
		.chain(game.add.tween(this.raum.spieler).to({alpha: 0}, 700, null)
		.chain(game.add.tween(this.raum.spieler).to({alpha: 0.7}, 350, null)
		.chain(game.add.tween(this.raum.spieler).to({alpha: 0}, 100, null))
		.chain(game.add.tween(this.raum.spieler).to({alpha: 0.5}, 100, null)
		.chain(game.add.tween(this.raum.spieler).to({alpha: 0}, 100, null)
		.chain(game.add.tween(this.raum.spieler).to({alpha: 0.5}, 100, null)
		.chain(game.add.tween(this.raum.spieler).to({alpha: 0}, 100, null)
		.chain(game.add.tween(this.raum.spieler).to({alpha: 0.5}, 100, null)
		.chain(game.add.tween(this.raum.spieler).to({alpha: 0}, 100, null))))))))));

		this.raum.spieler.body.enable = false;
		game.time.events.add(2600, function() {
			game.add.tween(this.raum.spieler.scale).to({y: 20}, 500, Phaser.Easing.Quadratic.In, true);
		}, this);

		game.time.events.add(2950, function() {
			game.add.tween(this.raum.spieler.position).to({y: -200}, 200, Phaser.Easing.Quadratic.InOut, true);
			game.time.events.add(200, function() { this.raum.spieler.isMovable = true; game.state.start(this.destination, true, false, null); }, this);
		}, this);
	}//end teleportTo

}//end Pressure_Plate
