class NPC {
	constructor(x, y, raum) {
		this.x = x;

		this.y = y;
		this.width = 0;
		this.height = 0;
		this.raum = raum;
		this.graphic;
		this.displayedText;
		this.displayedAnswers = [];
		this.textStyle = {
			font: "16px Verdana",
			fill: "#ffffff",
			backgroundColor: "rgba(67, 72, 79, 0.5)",
			align: "left",
			wordWrap: true,
			wordWrapWidth: 250
		};

        //um überlappungen bei den antworten zu verhindern wird der word wrap hier kleiner gesetzt
        //und die bounds_x von der überschrift werden zwischengespeichert damit die antwort direkt
        //drunter steht
        this.textStyleSmall = {
			font: "16px Verdana",
			fill: "#ffffff",
			backgroundColor: "rgba(67, 72, 79, 0.5)",
			align: "left",
			wordWrap: true,
			wordWrapWidth: 100
		};
         this.bounds_x=0;
		this.conversation;
		this.convoAt;
		this.convoEnded = false;
		this.convoPaused = false;
		this.isFocussed = 0;
		this.playerSelected;
		this.skipSetNext = false;
	} //end constructor

	setGraphic(key) {
		//Grafik entsprechend dem Key auswählen
		//wenn leerer Key, wird eine zufällige Grafik verwendet

		this.graphic = game.add.sprite(this.x, this.y, key);
		this.graphic.anchor.set(0.5);
		this.graphic.scale.setTo(0.75);
		game.physics.enable(this.graphic, Phaser.Physics.ARCADE);
		this.graphic.body.immovable = true;

		if(key != null && key.indexOf('spritesheet') !== -1){
			this.graphic.animations.add('talk', [0, 1], 7, true);
			this.graphic.animations.add('wave', [2, 3, 4, 5, 6, 7, 8], 5, false);
			this.graphic.frame = 0;

			this.height = game.cache.getImage(key).height;
			this.width = this.height;
		}
		else{
			this.width = game.cache.getImage(key).width;
			this.height = game.cache.getImage(key).height;
		}

		this.raum.aufgabenLayer.add(this.graphic);
	} //end setGraphic

	set convo(convo) {
		this.conversation = convo;
	} //end set convo

	updateConvo() {
		if (this.convoEnded || this.convoPaused)
			return -1;

		//convoAt aktualisieren
		if (!this.skipSetNext)
			this.convoSetNext(this.conversation[this.convoAt]);
		else
			this.skipSetNext = false;
		//Alten Text löschen
		this.clearText();

		if (this.convoPaused)
			return -1;

		//Text anzeigen
		this.graphic.animations.play('talk');
		this.updateDisplayedText(this.conversation[this.convoAt]);
	} //end updateConvo

	updateDisplayedText(element) {
		if (this.convoAt.search("statement") != -1) {
			//das Element ist eine einfache Aussage
			this.displayedText = game.add.text(this.x, this.y, element.text, this.textStyle);
      this.positionText(this.displayedText);
		} else if (this.convoAt.search("question") != -1) {
			//das Element ist eine Frage
			this.displayedText = game.add.text(this.x, this.y, element.text, this.textStyle);
      this.positionText(this.displayedText);
			this.displayAnswers(element);
		}

		//this.positionText(this.displayedText);
	} //end updateDisplayedText

	clearText() {
		//alten Text löschen, wenn vorhanden
		if (this.displayedText != null)
			this.displayedText.destroy();
		//Antwortmöglichkeiten löschen, wenn vorhanden
		if (this.displayedAnswers != null) {
			for (var i = 0; i < this.displayedAnswers.length; i++)
				this.displayedAnswers[i].destroy();
			this.displayedAnswers = [];
		}
	} //end clearText

	convoSetNext(element) {
		if (element == null) {
			this.raum.spieler.isMovable = false;
			this.convoAt = Object.keys(this.conversation)[0];
		} else {
			this.raum.spieler.isMovable = false;

			if (this.convoAt.search("statement") != -1) { //das Element ist eine einfache Aussage
				this.convoAt = element.next;
				if (element.callback != null)
					this.raum.aufgabe.npcCallback(element);
			} else if (this.convoAt.search("question") != -1) { //das Element ist eine Frage
				//Prüfe, welche Antwort ausgewählt wurde
				let i = 0;
				Object.keys(element).forEach((key) => {
					if (key.search("answer") != -1) {
						if (i == this.isFocussed) {
							this.convoAt = element[key].next;
							this.playerSelected = element[key];
							if (this.playerSelected.callback != null)
								this.raum.aufgabe.npcCallback(element[key]);
						}
						i++;
					}
				});
			}
			if (this.convoAt == "") {
				this.convoEnded = true;
				this.raum.spieler.isMovable = true;

			}
		}
	} //end convoSetNext

	displayAnswers(element) {
		//die Subelemente (Antworten) des Elements (Frage) holen
		let elementKeys = Object.keys(element);
		elementKeys.forEach((key) => {
			if (key.search("answer") != -1) {
				let answer = element[key].text;
				this.displayedAnswers.push(answer);
			}
		});

		let xOffset = 0;
		let yOffset = 0;
		for (var i = 0; i < this.displayedAnswers.length; i++) {

			this.displayedAnswers[i] =  game.add.text(this.bounds_x+this.x + (xOffset * 120), this.y + 40 + yOffset, this.displayedAnswers[i], this.textStyleSmall);
            xOffset++;
			if (xOffset == 2)
				xOffset = 0;
			if (i % 2 != 0)
				yOffset += 40;
		}

		//Hervorheben der ausgewählten Antwort
		this.displayedAnswers[this.isFocussed].addColor("#ffcb0f", 0);
	} //end displayAnswers

	updateSelection(key) {
		if (this.displayedAnswers.length != 0) {

			if (key == "x+" && this.isFocussed < this.displayedAnswers.length - 1) {
				this.displayedAnswers[this.isFocussed].addColor("white", 0);
				this.isFocussed++;
				this.displayedAnswers[this.isFocussed].addColor("#ffcb0f", 0);
			} else if (key == "x-" && this.isFocussed > 0) {
				this.displayedAnswers[this.isFocussed].addColor("white", 0);
				this.isFocussed--;
				this.displayedAnswers[this.isFocussed].addColor("#ffcb0f", 0);
			} else if (key == "y+" && this.isFocussed < this.displayedAnswers.length - 2) {
				this.displayedAnswers[this.isFocussed].addColor("white", 0);
				this.isFocussed += 2;
				this.displayedAnswers[this.isFocussed].addColor("#ffcb0f", 0);
			} else if (key == "y-" && this.isFocussed > 1) {
				this.displayedAnswers[this.isFocussed].addColor("white", 0);
				this.isFocussed -= 2;
				this.displayedAnswers[this.isFocussed].addColor("#ffcb0f", 0);
			}

		}
	} //end updateSelection

	lastPlayerSelection() {
		return this.playerSelected;
	}

	convoJumpTo(enterKey) {
		if (enterKey == "end") {
			this.convoEnded = true;
			this.raum.spieler.isMovable = true;
		} else {
			let elementKeys = Object.keys(this.conversation);
			elementKeys.forEach((key) => {
				if (key.search("statement") != -1) {
					if (this.conversation[key].enter != null && this.conversation[key].enter == enterKey) {
						this.convoAt = key;
						this.skipSetNext = true;
					}
				}
				if (key.search("question") != -1) {
						if (this.conversation[key].enter != null && this.conversation[key].enter == enterKey) {
						this.convoAt = key;
						this.skipSetNext = true;
					}
				}
			});
		}
	}

	convoFinsihed() {
		return this.convoEnded;
	}

	togglePauseConvo(pause) {
		this.convoPaused = pause;
		if (pause) {
			this.skipSetNext = true;
			this.raum.spieler.isMovable = true;
		}
		else
			this.raum.spieler.isMovable = false;
	}

	positionText(text) {
		text.setTextBounds(0 + (this.width / 2) + 5, 0 - (text.height / 2), 300, game.height - 50);
         this.bounds_x=(0 + (this.width / 2) + 5);
		//wenn nach rechts nicht genug Platz zum Anzeigen ist, auf der linken Seite anzeigen
		if (text.width > (game.width + this.raum.width_offset) - text.x){
			text.setTextBounds(-text.width - (this.width / 2) - 5, 0 - (text.height / 2), text.getBounds().width, text.getBounds().height);
        this.bounds_x=(-text.width - (this.width / 2) - 5);
        }
		//wenn nach oben nicht genug Platz zum Anzeigen ist, nach unten verschieben
		if (text.y - (text.height / 2) < 0) {
			let overflow = 5 - (this.y - (text.height / 2));
			text.setTextBounds(0 + (this.width / 2) + 5, 0 - (text.height / 2) + overflow, text.getBounds().width, text.getBounds().height);
		this.bounds_x=(0 + (this.width / 2) + 5);
        }

		//wenn nach unten nicht genug Platz zum Anzeigen ist, nach oben verschieben
		if (text.y + (text.height / 2) > game.height) {
			let overflow = (text.y + (text.height / 2)) - game.height + 5;
			text.setTextBounds(0 + (this.width / 2) + 5, 0 - (text.height / 2) - overflow, text.getBounds().width, text.getBounds().height);
		 this.bounds_x=(0 + (this.width / 2) + 5);
        }

		//wenn nach rechts und nach unten nicht genug Platz zum Anzeigen ist, nach links und oben verschieben
		if ((text.width > (game.width + this.raum.width_offset) - text.x) && (text.y + (text.height / 2) > game.height)) {
			let overflow = (text.y + (text.height / 2)) - game.height + 5;
			text.setTextBounds(-text.width - (this.width / 2) - 5, 0 - (text.height / 2) - overflow, text.getBounds().width, text.getBounds().height);
	 this.bounds_x=(-text.width - (this.width / 2) - 5);
        }

		//wenn nach rechts und nach oben nicht genug Platz zum Anzeigen ist, nach links und unten verschieben
		if ((text.width > (game.width + this.raum.width_offset) - text.x) && (text.y - (text.height / 2) < 0)) {
			let overflow = 5 - (this.y - (text.height / 2));
			text.setTextBounds(-text.width - (this.width / 2) - 5, 0 - (text.height / 2) + overflow, text.getBounds().width, text.getBounds().height);
	        this.bounds_x=(-text.width - (this.width / 2) - 5);

        }
	} //end positionText

    changeText(statementName, newText){
        this.conversation[statementName].text = newText;
    }

} //end class
