class Raum extends Phaser.State {

	constructor(aufgabe, thema, schwierigkeit) {
		super();

		this.debugOverlay = false;

		//Parameter die dem nächsten State mitgegeben werden
		this.params = null;
		
		this.aufgaben = aufgaben.slice();
		
		// GUI Bau hinzufügen wenn es ein MCI Dungeon ist
		if(thema == 'MCI' || thema == 'ISY')
			this.aufgaben.push(GUI_Bau);

		this.thema = thema;
		this.aufgabe = aufgabe || new this.aufgaben[Math.floor(Math.random() * this.aufgaben.length)](this.thema, schwierigkeit);
		this.aufgabe.raum = this;

		// Spielibjekte in der Scene
        this.game=game;
		//Layer
		this.wallLayer;
		this.doorbowLayer;
		this.doorbgLayer;
		this.doorLayer;
		this.bodenLayer;
		this.aufgabenLayer;
		this.spielerLayer;

		this.dungeonCompleted = false;

		this.isSolved = false;

		this.keyboard;
		this.cursors;
		this.wasd;
		this.quake;
		// trigger objekte zum laden der anderen Räume
		this.roomTrigger;
		this.width_offset = offset;

		// Spieler referenz
		this.spieler;

		// angrenzende Rräume
		// die reihenfolge der Türen und Räume ist wichtig
		// im Uhrzeigersinn: oben, rechts, unten, links
		this.connections = {
			up: null,
			right: null,
			down: null,
			left: null
		}

		this.aufgabe.blockMe(this);

		this.doorTweens = {
			up: {
				open: null,
				close: null
			},
			right: {
				open: null,
				close: null
			},
			down: {
				open: null,
				close: null
			},
			left: {
				open: null,
				close: null
			}
		};

		this.blackFade;
		this.exitConnection;

		this.deco = new Decorations();
	}

	init(params) {
		this.params = params;
		if (this.aufgabe != null)
			this.aufgabe.init(params);
	}

	// lädr assets, wird vor dem Spiel Aufbau und Ablauf auferufen
	preload() {
		game.load.atlas('room', 'assets/room/room.png', 'assets/room/room.json');
		game.load.image('dashboard_background', 'assets/dashboard.png');
		game.load.spritesheet('miner', 'assets/spieler/miner.png', 96, 96);

		game.load.audio('steps', 'assets/audio/footsteps.wav');
		game.load.audio('gong', 'assets/audio/code_puzzle/Gong.wav');

		// aufgaben assets laden
		if (this.aufgabe != null)
			this.aufgabe.preload();

		this.deco.preload();
	}

	// erstellt das Level, wird vor dem Spiel Ablauf aufgerufen
	create() {

		this.completeSFX = game.add.audio('gong');

		// setzte raum wecchsel Trigger.
		this.setRoomTrigger();

		// we need to add margin to the world, so the camera can move
		let margin = 50;
		// and set the world's bounds according to the given margin
		let x = -margin;
		let y = -margin;
		let w = game.world.width + margin * 2;
		let h = game.world.height + margin * 2;
		// it's not necessary to increase height, we do it to keep uniformity
		game.world.setBounds(x, y, w, h);

		// we make sure camera is at position (0,0)
		game.world.camera.position.set(0);

		this.addQuake();

		// input objekte initilaisieren
		this.keyboard = game.input.keyboard;
		this.cursors = this.keyboard.createCursorKeys();
		this.wasd = this.keyboard.addKeys({
			'w': Phaser.KeyCode.W,
			'a': Phaser.KeyCode.A,
			's': Phaser.KeyCode.S,
			'd': Phaser.KeyCode.D
		});

		// debug toggle
		this.keyboard.addKey(Phaser.KeyCode.Q)
			.onDown.add(this.toggleDebug, this);


		// Spiel Physik starten
		game.physics.startSystem(Phaser.Physics.ARCADE);


		// hintergrund settzen
		//game.add.sprite(0, 0, 'back');
		game.add.sprite(0, 0, 'room', 'bg');

		// Wände aufbauen
		this.wallLayer = game.add.group();
		this.wallLayer.enableBody = true;

		// Türen Hintergrund
		this.doorbgLayer = game.add.group();

		// Türen einsetzten
		this.doorLayer = game.add.group();
		this.doorLayer.enableBody = true;

		// Türrahmen platzieren
		this.doorbowLayer = game.add.group();

		// Boden layer platzieren
		this.bodenLayer = game.add.group();
		this.bodenLayer.enableBody = true;

		// Aufgaben Layer setzten
		this.aufgabenLayer = game.add.group();
		this.aufgabenLayer.enableBody = true;

		// Spieler ebene
		this.spielerLayer = game.add.group();
		this.spielerLayer.enableBody = true;

		// spieler erstellen
		this.spieler = new Spieler(this);

		this.deco.create(this);

		// aufgaben vorbereiten
		if (this.aufgabe != null)
			this.aufgabe.create();

		// wände Türen Platzieren
		this.setWallsDoors();
		// Wände und Türen unbeweglich machen
		this.wallLayer.setAll('body.immovable', true);
		this.doorLayer.setAll('body.immovable', true);




		this.blackFade = game.add.graphics();
		this.blackFade.beginFill(0x000000, 1);
		this.blackFade.drawRect(0, 0, 1024, 576);
		this.blackFade.endFill();

		var fadeDuration = 200;
		game.add.tween(this.blackFade).to({
			alpha: 0
		}, fadeDuration).start();
		this.blackFade.fadeOut = game.add.tween(this.blackFade).to({
			alpha: 1
		}, fadeDuration);
		this.blackFade.fadeOut.onComplete.add(this.exitRoom, this);

		// Objekt für aktions Keys
		game.input.onDown.add(this.gofull);;

		game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;
	}

	update() {


        dashboard.update();
		// collsitions abfragen
		// Wände Türen
		game.physics.arcade.collide(this.spieler, [this.wallLayer, this.doorLayer]);

		// raum wechsel trigger
		game.physics.arcade.overlap(this.spieler, this.roomTrigger);

		this.spieler.update();

		if (this.aufgabe != null)
			this.aufgabe.update();

		this.deco.update();
	}

	render() {

		//Testlinie für dashboard
		game.debug.geom(dashboard.line1, "black");


		if (this.debugOverlay && this.roomTrigger) {
			game.debug.body(this.roomTrigger.next(), 'rgba(0,255,0,0.5)');
			game.debug.body(this.roomTrigger.next(), 'rgba(0,255,0,0.5)');
			game.debug.body(this.roomTrigger.next(), 'rgba(0,255,0,0.5)');
			game.debug.body(this.roomTrigger.next(), 'rgba(0,255,0,0.5)');

			game.debug.body(this.doorLayer.next(), 'rgba(0,255,0,0.5)');
			game.debug.body(this.doorLayer.next(), 'rgba(0,255,0,0.5)');
			game.debug.body(this.doorLayer.next(), 'rgba(0,255,0,0.5)');
			game.debug.body(this.doorLayer.next(), 'rgba(0,255,0,0.5)');

			game.debug.body(this.spieler, 'rgba(0,255,0,0.5)');
            
			game.debug.text("Raum " + this.key, 120, 120, 'rgba(255,0,0,0.5)');

			if (this.aufgabe != null)
				this.aufgabe.render();
		}
	}

	shutdown() {

		if (this.aufgabe && this.aufgabe.shutdown)
			this.aufgabe.shutdown();
	}

	// debug ein/aus
	toggleDebug() {
		if (this.debugOverlay)
			game.debug.reset();

		this.debugOverlay = !this.debugOverlay;
	}

	addQuake() {

		// define the camera offset for the quake
		let rumbleOffset = 15;

		// we need to move according to the camera's current position
		let properties = {
			x: game.camera.x - rumbleOffset
		};

		// we make it a relly fast movement
		let duration = 100;
		// because it will repeat
		let repeat = 3;
		// we use bounce in-out to soften it a little bit
		let ease = Phaser.Easing.Bounce.InOut;
		let autoStart = false;
		// we want to go back to the original position
		let yoyo = true;

		this.quake = game.add.tween(game.camera)
			.to(properties, duration, ease, autoStart, 0, repeat, yoyo);

		// das erdbeben wird nur ein mal ausgelöst
		this.quake.onComplete.add(() => {
			game.tweens.remove(this.quake);
		});
	}

	// öffnet alle türen im Raum.
	// die reihenfolge der Türen und Räume ist wichtig
	// im Uhrzeigersinn: oben, rechts, unten, links

    checkHighscore(){
         dashboard.checkHighscore(this.thema,this.spieler);
    }

	openDoors() {
      //  this.checkHighscore();



		// verbindungs namen in array speichern
		let keys = Object.keys(this.connections);

		// markiere diesen Raum als gelöst
		this.isSolved = true;
		this.completeSFX.play();

		// Jeder Tür Prüfen
		keys.forEach((key) => {

			// prüfen ob hinter der Tür ein Raum ist
			if (this.connections[key] != null &&
				this.connections[key] != 'blocked' &&
				this.doorTweens[key].open != null) {
				this.doorTweens[key].open.updateTweenData("duration", 300);
				this.doorTweens[key].open.start();
			}

		});

		if (this.thema && dungeons[this.thema].completed()) {
			//console.log(this.thema + ' Dungeon abgeschlossen');
			this.dungeonCompleted = true;
			//this.exitConnection = this.thema + "BonusRun";
			//this.blackFade.fadeOut.start();

			//dashboard.checkHighscore(this.thema,this.spieler);
		}

	}

	openDoor(key, solve) {
		// markiere diesen Raum als gelöst
		if (solve != null) this.isSolved = solve;

		if (this.connections[key] != null &&
			this.connections[key] != 'blocked' &&
			this.doorTweens[key].open != null) {
			this.doorTweens[key].open.start();
		}

	}

	// öffnet alle türen im Raum.
	// die reihenfolge der Türen und Räume ist wichtig
	// im Uhrzeigersinn: oben, rechts, unten, links
	closeDoors() {

		// verbindungs namen in array speichern
		let keys = Object.keys(this.connections);

		// Jeder Tür Prüfen
		keys.forEach((key) => {

			// prüfen ob hinter der Tür ein Raum ist
			if (this.connections[key] != null &&
				this.connections[key] != 'blocked' &&
				this.doorTweens[key].close != null)
				this.doorTweens[key].close.start();

		});
	}

	closeDoor(key) {
		// prüfen ob hinter der Tür ein Raum ist
		if (this.connections[key] != null &&
			this.connections[key] != 'blocked' &&
			this.doorTweens[key].close != null)
			this.doorTweens[key].close.start();

	}

	// setzt die Trigger um angrenzende Räume zu laden
	// die reihenfolge der Türen und Räume ist wichtig
	// im Uhrzeigersinn: oben, rechts, unten, links
	setRoomTrigger() {
		this.roomTrigger = game.add.group();
		this.roomTrigger.enableBody = true;

		// top
		let trigger = this.roomTrigger.create(482, 0);
		trigger.body.setSize(60, 10);
		trigger.body.onOverlap = new Phaser.Signal();
		trigger.body.onOverlap.add(() => {
			if (this.connections.up != null) {
				this.setLastRoom();
				//game.state.start(this.connections.up, true, false, this.params);
				this.exitConnection = this.connections.up;
				this.blackFade.fadeOut.start();
			}
		});

		// r
		trigger = this.roomTrigger.create(1014, 258);
		trigger.body.setSize(10, 60);
		trigger.body.onOverlap = new Phaser.Signal();
		trigger.body.onOverlap.add(() => {
			if (this.connections.right != null) {
				this.setLastRoom();
				//game.state.start(this.connections.right, true, false, this.params);
				this.exitConnection = this.connections.right;
				this.blackFade.fadeOut.start();
			}
		});

		// bot
		trigger = this.roomTrigger.create(482, 566);
		trigger.body.setSize(60, 10);
		trigger.body.onOverlap = new Phaser.Signal();
		trigger.body.onOverlap.add(() => {
			if (this.connections.down != null) {
				this.setLastRoom();
				//game.state.start(this.connections.down, true, false, this.params);
				this.exitConnection = this.connections.down;
				this.blackFade.fadeOut.start();

			}
		});

		// l
		trigger = this.roomTrigger.create(0, 258);
		trigger.body.setSize(10, 60);
		trigger.body.onOverlap = new Phaser.Signal();
		trigger.body.onOverlap.add(() => {
			if (this.connections.left != null) {
				this.setLastRoom();
				//game.state.start(this.connections.left, true, false, this.params);
				this.exitConnection = this.connections.left;
				this.blackFade.fadeOut.start();
			}
		});

	}

	exitRoom() {
		game.state.start(this.exitConnection, true, false, this.params);
	}

	// ermittelt ob eine Tür gesetzt werden soll
	// tür soll gesetzt werden wenn:
	//		sich kein Raum dahinter befindet
	//		der Raum noch nicht gelöst wurde
	//		der Spieler sich nicht an der Tür Position befindet
	doorClosed(doorPosition) {

		// tür darf nicht gesetzt werden wenn Raum bereits gelöst wurde
		if (this.isSolved)
			return false;

		// tür darf nicht gesetzt werden wenn der Raum hinter der Tür bereits gelöst wurde
		if (game.state.states[this.connections[doorPosition]].isSolved)
			return false;

		// // tür darf nicht gesetzt werden wenn der Speiler in ihr steht
		if (this.spieler.spawnPos == doorPosition)
			return false;

		return true;


	}

	// setzt Wände, Türen und Tür Rahmen
	setWallsDoors() {

		let door;
		let doorTween;
		let doorbow;

		let duration = 300;

		// die reihenfolge der Türen und Räume ist wichtig
		// im Uhrzeigersinn: oben, rechts, unten, links


		// up
		if (this.connections.up == null || this.connections.up == 'blocked')
			this.wallLayer.create(0, 0, 'room', 'wall_top');
		else {
			this.wallLayer.create(0, 0, 'room', 'wall_top_left');
			this.wallLayer.create(540, 0, 'room', 'wall_top_right');
			this.doorbgLayer.create(512, 0, 'room', 'door_bg').anchor.set(0.5, 0);
			this.doorbowLayer.create(512, 0, 'room', 'door_bow').anchor.set(0.5, 0);

			door = this.doorLayer.create(512, 0, 'room', 'door');
			door.anchor.set(0.5, 0);
			doorTween = game.add.tween(door).to({
				y: '-98'
			}, duration);
			this.doorTweens.up.open = doorTween;

			doorTween = game.add.tween(door).to({
				y: 0
			}, duration);
			this.doorTweens.up.close = doorTween;

			if (this.doorClosed('up')) {
				this.doorTweens.up.close.start();
			} else {
				this.doorTweens.up.open.updateTweenData("duration", 1);
				this.doorTweens.up.open.start();
			}


		}





		// right
		if (this.connections.right == null || this.connections.right == 'blocked')
			this.wallLayer.create(936, 0, 'room', 'wall_right');
		else {
			this.wallLayer.create(936, 0, 'room', 'wall_right_up');
			this.wallLayer.create(936, 316, 'room', 'wall_right_down');
			var doorBg = this.doorbgLayer.create(1024, 288, 'room', 'door_bg');
			doorBg.anchor.set(0.5, 0);
			doorBg.angle = 90;
			var doorBow = this.doorbowLayer.create(1024, 288, 'room', 'door_bow');
			doorBow.anchor.set(0.5, 0);
			doorBow.angle = 90;

			door = this.doorLayer.create(1024, 288, 'room', 'door');
			door.anchor.set(0.5, 0);
			door.angle = 90;
			door.body.setSize(door.height, door.width);
			door.body.offset.set(-58, -43);
			doorTween = game.add.tween(door).to({
				x: '+98'
			}, duration);
			this.doorTweens.right.open = doorTween;

			doorTween = game.add.tween(door).to({
				x: 1024
			}, duration);
			this.doorTweens.right.close = doorTween;

			if (this.doorClosed('right')) {
				this.doorTweens.right.close.start();
			} else {
				this.doorTweens.right.open.updateTweenData("duration", 1);
				this.doorTweens.right.open.start();
			}


		}





		// down
		if (this.connections.down == null || this.connections.down == 'blocked')
			this.wallLayer.create(0, 490, 'room', 'wall_bottom');
		else {
			this.wallLayer.create(0, 490, 'room', 'wall_bottom_left');
			this.wallLayer.create(540, 490, 'room', 'wall_bottom_right');
			var doorBg = this.doorbgLayer.create(512, 576, 'room', 'door_bg');
			doorBg.anchor.set(0.5, 0);
			doorBg.angle = 180;
			var doorBow = this.doorbowLayer.create(512, 576, 'room', 'door_bow');
			doorBow.anchor.set(0.5, 0);
			doorBow.angle = 180;

			door = this.doorLayer.create(512, 576, 'room', 'door');
			door.anchor.set(0.5, 0);
			door.angle = 180;
			door.body.offset.set(0, -100);
			doorTween = game.add.tween(door).to({
				y: '+98'
			}, duration);
			this.doorTweens.down.open = doorTween;

			doorTween = game.add.tween(door).to({
				y: 576
			}, duration);
			this.doorTweens.down.close = doorTween;


			if (this.doorClosed('down')) {
				this.doorTweens.down.close.start();
			} else {
				this.doorTweens.down.open.updateTweenData("duration", 1);
				this.doorTweens.down.open.start();
			}


		}





		// left
		if (this.connections.left == null || this.connections.left == 'blocked')
			this.wallLayer.create(0, 0, 'room', 'wall_left');
		else {
			this.wallLayer.create(0, 0, 'room', 'wall_left_up');
			this.wallLayer.create(0, 316, 'room', 'wall_left_down');
			var doorBg = this.doorbgLayer.create(0, 288, 'room', 'door_bg');
			doorBg.anchor.set(0.5, 0);
			doorBg.angle = -90;
			var doorBow = this.doorbowLayer.create(0, 288, 'room', 'door_bow');
			doorBow.anchor.set(0.5, 0);
			doorBow.angle = -90;

			door = this.doorLayer.create(0, 288, 'room', 'door');
			door.anchor.set(0.5, 0);
			door.angle = -90;
			door.body.setSize(door.height, door.width);
			door.body.offset.set(45, -43);
			doorTween = game.add.tween(door).to({
				x: '-98'
			}, duration);
			this.doorTweens.left.open = doorTween;

			doorTween = game.add.tween(door).to({
				x: 0
			}, duration);
			this.doorTweens.left.close = doorTween;

			if (this.doorClosed('left')) {
				this.doorTweens.left.close.start();
			} else {
				this.doorTweens.left.open.updateTweenData("duration", 1);
				this.doorTweens.left.open.start();
			}


		}
		/*

		var dashboardgroup = game.add.group();
		  dashboardgroup.create(1024,0, 'dashboard_background');
	*/

	}

	// speichert den aktuellen Raum in einem globalen wert.
	setLastRoom() {
		this.cache.addText("lastRoom", null, this.key);
	}


	// toggle funktion für Fullscreen
	gofull() {
		if (game.scale.isFullScreen) {
			game.scale.stopFullScreen();
		} else {
			game.scale.startFullScreen(true);
		}
	}

	// setzte Raumverbindungen
	set up(stateKey) {
		this.connections.up = stateKey;
	}

	set down(stateKey) {
		this.connections.down = stateKey;
	}

	set left(stateKey) {
		this.connections.left = stateKey;
	}

	set right(stateKey) {
		this.connections.right = stateKey;
	}

}
