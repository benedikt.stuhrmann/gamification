class Dungeon {

	constructor(game, thema) {

		this.game = game;
		this.thema = thema;
		this.schwierigkeit;
		this.anzahl;

		this.räume = new Array();

		this.connections = 0;

		//zufälligen dungeon bauen
		this.createDungeon();

		//console.log(thema + ': ' + this.connections + ' verbindungen');

	}

	// erstellt einen neuen zufälligen Dungeon
	createDungeon(schwierigkeit, anzahlRaeume) {

		this.reset();

		this.schwierigkeit = schwierigkeit;
		this.anzahl = anzahlRaeume;
        dashboard.anzahlRäume=this.anzahl;


		// der erste start raum ist der Hub
		let aktuellerRaum = game.state.states['Hub'];

		let richtung;

		// ermittle die richtung in die der erste Raum führen wird
		switch (this.thema) {
			case 'OOP1':
				richtung = 'up';
				break;
			case 'OOP2':
				richtung = 'right';
				break;
			case 'MCI':
				richtung = 'down';
				break;
			case 'ISY':
				richtung = 'left';
				break;
		}

		this.neuenRaumAusRichtung(aktuellerRaum, richtung);

		aktuellerRaum = this.räume[0];

		// erstelle so viele Räume wie angefordert
		while (this.räume.length < this.anzahl) {

			// leere verbindungs namen speichern
			let freieConnections = Object.keys(aktuellerRaum.connections).filter(key => {
				return aktuellerRaum.connections[key] == null
			});


			for (let i = 0, cons = 0; i < freieConnections.length && this.räume.length < this.anzahl; i++) {

				richtung = freieConnections[i];

				let rnd = Math.random() > 0.5;
				let istLetzteVerbindung = i == freieConnections.length - 1;
				let hatNochKeineVerbindungen = cons == 0;

				if (rnd || (istLetzteVerbindung && hatNochKeineVerbindungen)) {
					this.neuenRaumAusRichtung(aktuellerRaum, richtung);
					cons++;
				}



			}

			let index = this.räume.indexOf(aktuellerRaum)

			aktuellerRaum = this.räume[index + 1];

		}

		//BonusRun dieses Dungeons hinzufügen
		let bonusRunAufgabe = new Bonus_Run(this.thema, this.schwierigkeit);
		let bonusRunRaum = new Raum(bonusRunAufgabe, this.thema, this.schwierigkeit);
		bonusRunRaum.key = this.thema + "BonusRun";

		game.state.add(bonusRunRaum.key, bonusRunRaum, false);

	}

	verbindeRäume(startRaum, richtung, zielRaum) {

		switch (richtung) {
			case 'up':
				startRaum.up = zielRaum.key;
				zielRaum.down = startRaum.key;
				break;
			case 'right':
				startRaum.right = zielRaum.key;
				zielRaum.left = startRaum.key;
				break;
			case 'down':
				startRaum.down = zielRaum.key;
				zielRaum.up = startRaum.key;
				break;
			case 'left':
				startRaum.left = zielRaum.key;
				zielRaum.right = startRaum.key;
				break;
		}

		this.connections++;

		//console.log(startRaum.key + ' über ' + richtung + ' mit ' + zielRaum.key);

	}

	// ist wahr wenn der Zielraum in richtung Startraum geblockt ist.
	istErreichbar(richtung, zielRaum) {

		switch (richtung) {
			case 'up':
				return zielRaum.connections['down'] != 'blocked';
				break;
			case 'right':
				return zielRaum.connections['left'] != 'blocked';
				break;
			case 'down':
				return zielRaum.connections['up'] != 'blocked';
				break;
			case 'left':
				return zielRaum.connections['right'] != 'blocked';
				break;
		}
	}

	// liefert einen neuen Raum zurück der aus
	// Richtung des Start Raums erreichbar ist
	neuenRaumAusRichtung(startRaum, richtung) {

		// erstelle den neuen Raum
		let neuerRaum = new Raum(null, this.thema, this.schwierigkeit);

		neuerRaum.key = this.thema + this.räume.length;

		let istNichtGleicheAufgabe = startRaum.aufgabe.constructor.name != neuerRaum.aufgabe.constructor.name;
		let neuerRaumIstErreichbar = this.istErreichbar(richtung, neuerRaum);

		// ist der neue Raum aus richtung des start Raums erreichbar
		// gebe ihn zurück, wenn nicht erstelle einen neuen Raum.
		if (neuerRaumIstErreichbar && istNichtGleicheAufgabe) {

			this.verbindeRäume(startRaum, richtung, neuerRaum);

			game.state.add(this.thema + this.räume.length, neuerRaum, false);

			this.räume.push(neuerRaum);

		} else {

			this.neuenRaumAusRichtung(startRaum, richtung)

		}

	}

	reset() {
		this.räume = [];
	}

	// gibt an ob ein Dungeon gelöst ist.
	// liefert true zurück wenn alle Räume isSolved == true sind
	completed() {
		return this.räume.every(raum => raum.isSolved);
	}


}
