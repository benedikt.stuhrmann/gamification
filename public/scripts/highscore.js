class Highscore extends Aufgabe {

	constructor(raum) {
		super(raum)

		this.red_btn;
        this.phaserJSON;
	}

	preload() {
		game.load.image('red_btn', 'assets/aufgaben/red_btn/red_button.png');
          game.load.json('testfile', 'assets/testfile.json');
	}

	create() {
		// red button setzten
		this.red_btn = game.add.sprite((game.width+this.raum.width_offset)/ 2, game.height / 4, 'red_btn');
		this.red_btn.anchor.set(0.5);
		this.raum.aufgabenLayer.add(this.red_btn);

		this.spieler = this.raum.spieler;
        this.raum.dashboard.setPoints();
        
        this.phaserJSON = game.cache.getJSON('testfile').Highscore;

	}

	update() {
        
// Wie machen wir das ? Lokaler persönlcher Highscore über Local Storage
        //oder globalen Highscore über den Server?? (weil phaser kann nicht in files schreiben? )
        
        
        
		// red button trigger
		game.physics.arcade.overlap(this.spieler, this.red_btn, () => {
            

			if (!this.raum.isSolved) {
				// lets the earthquake begin
			console.log(this.phaserJSON[0][0]);
                console.log(this.phaserJSON[1][0]);
                console.log(this.phaserJSON[2][0]);
                
                
                this.raum.quake.start();
                this.raum.dashboard.updatePoints(20);
				this.raum.openDoors();
			}


		});
	}

}
