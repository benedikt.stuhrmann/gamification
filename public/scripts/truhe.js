class Chest{
  constructor(x, y, unlocked, raum){
    this.x = x;
    this.y = y;
    this.chest;
    this.raum = raum;
    this.unlocked = unlocked;
    this.opened = false;

    this.chest = game.add.sprite(this.x, this.y, 'chest');
    this.chest.anchor.set(0.5);
    this.chest.scale.setTo(0.55);
    game.physics.enable(this.chest, Phaser.Physics.ARCADE);
    this.chest.body.immovable = true;
    this.raum.aufgabenLayer.add(this.chest);

    this.chest.animations.add('open', [0, 1, 2, 3, 4], 10, false);
    this.chest.animations.add('close', [7, 6, 5], 10, false);
  }

  open(){
    this.chest.animations.play('open');
    this.opened = true;
  }

  close(){
    this.chest.animations.play('close');
    this.opened = false;
  }

  toggleUnlock(unlock){
    this.unlocked = unlock;
  }
}
