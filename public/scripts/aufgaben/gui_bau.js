class GUI_Bau extends Aufgabe{
  constructor(thema, schwierigkeit){
    super();
    this.guiChest;
    this.inMenu = false;
    this.guiElements;
    this.additionalElements;
    this.resetBtn; this.resetAnim = null;
    this.confirmBtn; this.confirmAnim = null;

    this.SCALE_FACTOR = 1.08;
    this.env;
		this.pieces;
    this.eventOverlay;
		this.targetOverlay;

    //sfx
    this.pickup_sfx;
		this.drop_sfx;
		this.error_sfx;
		this.grind_sfx;
		this.gong_sfx;

    this.justSteppedOnRed = true;
    this.justSteppedOnGreen = true;

    this.baseElements;

    this.thema = thema;
    this.schwierigkeit = schwierigkeit;

    this.lvlPoints = 0;
    this.tries = 0;

    this.elementsOnField;
    this.elementsInChest;
  }

  preload(){
    game.load.json('guiElements', 'fragen/guiBau/gui_bau_elements.json');
    game.load.json('aufgaben', 'fragen/guiBau/' + this.thema + '/' + this.schwierigkeit + '.json');

    game.load.image('guiZone', 'assets/aufgaben/gui_bau/guiZone.png');
    game.load.image('guiZoneWhite', 'assets/aufgaben/gui_bau/guiZoneWhite.png');
    game.load.image('guiOverlay_bg', 'assets/aufgaben/gui_bau/guiOverlay_bg.png');
    game.load.image('slot', 'assets/aufgaben/gui_bau/guiOverlay_slot.png');
    game.load.image('focus', 'assets/aufgaben/gui_bau/guiOverlay_focus.png');
    game.load.image('preview', 'assets/aufgaben/gui_bau/guiOverlay_preview.png');
    game.load.image('red_btn', 'assets/aufgaben/red_btn/red_button.png');

    game.load.image('transparent_tile', 'assets/aufgaben/gui_bau/transparent_tile.png');
		game.load.audio('pickup', 'assets/audio/code_puzzle/Pickup.mp3');
		game.load.audio('drop', 'assets/audio/code_puzzle/Drop.wav');
		game.load.audio('error', 'assets/audio/code_puzzle/Error.wav');
		game.load.audio('grind', 'assets/audio/code_puzzle/Grind.wav');
		game.load.audio('gong', 'assets/audio/code_puzzle/Gong.wav');

    game.load.spritesheet('chest', 'assets/truhe.png', 128, 128);
    game.load.spritesheet('reset_plate', 'assets/aufgaben/gui_bau/pressure_plate_red.png', 70, 70);
    game.load.spritesheet('confirm_plate', 'assets/aufgaben/gui_bau/pressure_plate_green.png', 70, 70);
    game.load.spritesheet('teleporter', 'assets/room/pressure_plate_bonus.png', 70, 70);
  }

  create(){
    //GUI-Elemente Grafiken laden
    this.baseElements = game.cache.getJSON('guiElements');
    let loader = new Phaser.Loader(game);
    for(var i=0; i<this.baseElements.length; i++){
      if(this.baseElements[i].graphic != null && this.baseElements[i].graphic_path != null)
        loader.image(this.baseElements[i].graphic, this.baseElements[i].graphic_path);
    }
    loader.start();

    if(this.task == null){
      let tasks = game.cache.getJSON('aufgaben');
      this.task = game.rnd.pick(tasks);

      //GUI-Elemente der Aufgabe erstellen
      this.guiElements = [];
      this.groups = [];
      let module = this.task;
      this.createElements(module);
      //Zusätzliche GUI-Elemente erstellen
      this.additionalElements = [];
      this.addAdditionalElements();

      this.elementsInChest = this.guiElements.slice();
      for(var i=0; i<this.additionalElements.length; i++)
        this.elementsInChest.push(this.additionalElements[i]);
    }

    //Baufläche
    this.guiZoneGraphic = game.add.sprite((game.width/2)+(this.raum.width_offset/2), game.height/2, 'guiZone');
    this.guiZoneGraphic.anchor.set(0.5);
    this.raum.aufgabenLayer.add(this.guiZoneGraphic);

    this.guiZoneBG = game.add.sprite((game.width/2)+(this.raum.width_offset/2), game.height/2, 'guiZoneWhite');
    this.guiZoneBG.anchor.set(0.5);
    this.guiZoneBG.alpha = 0;
    this.raum.aufgabenLayer.add(this.guiZoneBG);

    //Zurücksetzen Schalter
    this.resetBtn = new Pressure_Plate(140, (game.height/2)+80, 'reset_plate', this.raum);
    //Bestätigen Schalter
    this.confirmBtn = new Pressure_Plate(game.width - 140 + this.raum.width_offset, (game.height/2)+80, 'confirm_plate', this.raum);

    //Audio
    this.pickup_sfx = game.add.audio('pickup');
    this.drop_sfx = game.add.audio('drop');
    this.error_sfx = game.add.audio('error');
    this.gong_sfx = game.add.audio('gong');
    this.grind_sfx = game.add.audio('grind');
    this.grind_sfx.loop = true;

    this.eventOverlay = game.add.graphics();
    this.targetOverlay = game.add.graphics();
    this.env = [this.raum.wallLayer, this.raum.doorLayer, this.raum.doorbowLayer];
    this.pieces = game.add.physicsGroup();
    this.raum.aufgabenLayer.add(this.pieces);

    this.placeExisting();

    this.guiChest = new GUI_Chest(140, (game.height/2)-80, false, this.raum, this.elementsInChest, this.elementsOnField, this.task.assignment);
    this.raum.spieler.addAction(this.action, this);

    dashboard.setPoints(this.raum.thema,"GuiBau");
  }

  render() {
    this.pieces.forEach(piece => {
        game.debug.body(piece, 'rgba(0,255,0,0.5)');
    });
  }

  update(){
    if(this.teleporter != null) this.teleporter.listen((this.thema+'BonusRun'), null, null, this.teleporter);
		else if(this.raum.dungeonCompleted) this.teleporter = new Pressure_Plate(game.width - 140 + this.raum.width_offset, (game.height/2)-80, 'teleporter', this.raum);

    game.physics.arcade.collide(this.raum.spieler, this.guiChest);

    this.resetBtn.listen(this.resetPieces, null, null, this);
    this.confirmBtn.listen(this.evaluate, this.hideText, this.showBackground, this);

    if(this.inMenu){

		// steuerung aufsetzten
		let up = this.raum.cursors.up.justDown || this.raum.wasd.w.justDown;
		let down = this.raum.cursors.down.justDown || this.raum.wasd.s.justDown;
		let left = this.raum.cursors.left.justDown || this.raum.wasd.a.justDown;
		let right = this.raum.cursors.right.justDown || this.raum.wasd.d.justDown;

      if(up)
  			this.guiChest.updateSelection("y-");
  		if(left)
  			this.guiChest.updateSelection("x-");
  		if(down)
  			this.guiChest.updateSelection("y+");
  		if(right)
  			this.guiChest.updateSelection("x+");
    }

    if(this.targetOverlay != null)
      this.targetOverlay.clear();

		game.physics.arcade.collide(this.pieces, this.env.concat([this.raum.spielerLayer]));
		game.physics.arcade.collide(this.pieces);

		var piecesMoving = false;
		// Slow down pieces
		this.pieces.forEach((piece) => {
			piecesMoving |= piece.body.velocity.getMagnitude() > 10;
			if (piece != this.hold) piece.body.velocity.multiply(.8, .8);

      if(Math.round(piece.body.velocity.x) == 0 && Math.round(piece.body.velocity.y) == 0 && piece != this.hold)
        piece = this.snap(piece);
		});

		// Play sounds
		if (this.grind_sfx.isDecoded) {
			if (piecesMoving) {
				this.grind_sfx.isFadingOut = false;
				if (!this.grind_sfx.isPlaying) this.grind_sfx.fadeIn(200);
			}
			else if (!this.grind_sfx.isFadingOut) {
				this.grind_sfx.fadeOut(400);
				this.grind_sfx.isFadingOut = true;
			}
		}

		// Draw target overlay
		let firstFound = false;
		if (!this.hold) game.physics.arcade.overlap(this.raum.spieler.grabArea, this.pieces, (spieler, piece) => {
			if (firstFound) return;
			this.targetOverlay.lineStyle(3, 0xFFFFFF, .2);
			this.targetOverlay.drawRect(piece.left, piece.top, piece.width, piece.height);
			firstFound = true;
		});

    //Level skippen
    if(this.tries >= 3 && !this.raum.isSolved && game.input.keyboard.addKey(Phaser.Keyboard.N).justDown)
      this.raum.openDoors();

  }//end update

  createPiece(element, x, y) {
    this.standingOn = false;

		var padding = 20;

		var captionTxt = game.add.text(0, 2, element.text);
		captionTxt.anchor.set(0.5);
		captionTxt.font = 'Verdana';
    captionTxt.fontSize = 14;
    if(element.color != null)
      captionTxt.addColor(element.color, 0);
    else
      captionTxt.addColor("black", 0);

    var piece;

    if(element.graphic != null)
      piece = game.add.sprite(0, 0, element.graphic);
    else
      piece = game.add.tileSprite(0, 0, this.calculateWidth(captionTxt.width, padding), 40, 'transparent_tile');

		piece.anchor.set(.5);
		piece.caption = captionTxt;

		piece.addChild(captionTxt);

    if(x == null || y == null){
  		piece.x = this.raum.spieler.x;
  		piece.y = this.raum.spieler.x;
    }
    else{
      piece.x = x;
      piece.y = y;
    }

	  game.physics.arcade.enable(piece);

    if(element.graphic == null)
      piece.body.setSize(this.calculateWidth(captionTxt.width, padding), 40, 0, 0);

    piece.item = element;

    if(x == null || y == null){
      this.elevate(piece);
      this.hold = piece;
      this.pickup_sfx.play();
    }

		return piece;
	}

  calculateWidth(width, padding){
    if(width%20 == 0)
      return width;
    return Math.round((Math.round(width)+9)/20)*20;
  }

	elevate(piece) {
		piece.width *= this.SCALE_FACTOR;
		piece.height *= this.SCALE_FACTOR;
		piece.scale.multiply(this.SCALE_FACTOR, this.SCALE_FACTOR);
		piece.body.width *= this.SCALE_FACTOR;
		piece.body.height *= this.SCALE_FACTOR;
		piece.children.forEach((el) => {
			el.scale.multiply(this.SCALE_FACTOR, this.SCALE_FACTOR);
			el.position.multiply(this.SCALE_FACTOR, this.SCALE_FACTOR);
		});

		this.raum.spieler.addChild(piece);
		piece.x -= this.raum.spieler.x;
		piece.y -= this.raum.spieler.y;
		piece.body.velocity = new Phaser.Point(0,0);
		piece.body.immovable = true;

	}

	ground(piece) {
		piece.width /= this.SCALE_FACTOR;
		piece.height /= this.SCALE_FACTOR;
		piece.scale.divide(this.SCALE_FACTOR, this.SCALE_FACTOR);
		piece.body.width /= this.SCALE_FACTOR;
		piece.body.height /= this.SCALE_FACTOR;
		piece.children.forEach((el) => {
			el.scale.divide(this.SCALE_FACTOR, this.SCALE_FACTOR);
			el.position.divide(this.SCALE_FACTOR, this.SCALE_FACTOR);
		});
    piece.x += this.raum.spieler.x;
		piece.y += this.raum.spieler.y;
    piece = this.snap(piece);
    this.pieces.add(piece);
		piece.body.immovable = false;
	}

	action() {
		if(!this.hold) {
			game.physics.arcade.overlap(this.raum.spieler.grabArea, this.pieces, (spieler, piece) => {
				if (this.hold != null) return;
				this.elevate(piece);
				this.hold = piece;
				this.pickup_sfx.play();
			});
		}
		else {
			// Draw obstacle
			let overlapping = game.physics.arcade.overlap(
				this.hold, this.env.concat([this.pieces]), (hold, obs) => {
					this.eventOverlay.beginFill(0xFF0000, .1);
					this.eventOverlay.drawRect(obs.left, obs.top, obs.width, obs.height);
					this.eventOverlay.endFill();
					setTimeout(() => { this.eventOverlay.clear(); }, 200);
				}
			);
			if (!overlapping) {
				this.ground(this.hold);
				this.hold = null;
				this.drop_sfx.play();
			}
			else this.error_sfx.play();
		}


    if(game.physics.arcade.distanceBetween(this.raum.spieler, this.guiChest.chest) < 80){
      //wenn in Nähe der Truhe
      if(!this.guiChest.opened && this.guiChest.unlocked){
        if(this.hold != null){
            this.guiChest.add(this.hold.item);
          this.hold.destroy();
          this.hold = null;
        }
        this.guiChest.open();
        this.inMenu = true;
        this.raum.spieler.isMovable = false;
      }
      else if(this.guiChest.opened){
        this.guiChest.close();
        this.inMenu = false;
        this.raum.spieler.isMovable = true;
        if(this.guiChest.selected != null){
          this.createPiece(this.guiChest.selected);
          this.guiChest.remove(this.guiChest.selected);
        }
      }
    }

	}//end action

  snap(piece){
    let evenTilesWidth  = (piece.width/20)%2==0;
    let evenTilesHeight = (piece.height/20)%2==0;

    if(!evenTilesWidth && evenTilesHeight  && (piece.x-2)%20==0    && (piece.y-8)%20==0)    return;
    if( evenTilesWidth && evenTilesHeight  && (piece.x-2-10)%20==0 && (piece.y-8)%20==0)    return;
    if( evenTilesWidth && !evenTilesHeight && (piece.x-2-10)%20==0 && (piece.y-8-10)%20==0) return;
    if(!evenTilesWidth && !evenTilesHeight && (piece.x-2)%20==0    && (piece.y-8-10)%20==0) return;

    let x = Math.round(piece.x);
    let y = Math.round(piece.y);

    if(!evenTilesWidth)  x = Math.round((x)/20)*20;
    else                 x = Math.round((x-10)/20)*20;
    if(!evenTilesHeight) y = Math.round((y-10)/20)*20;
    else                 y = Math.round((y)/20)*20;

    if(evenTilesWidth)  piece.x = x+2+10;
    else                piece.x = x+2;
		if(evenTilesHeight) piece.y = y+8;
    else                piece.y = y+8+10;

    return piece;
  }

  createElements(module){
    if(module.elements == null)
      //Element ist ein GUI-Element, also erstellen
      this.guiElements.push(this.createElement(module));

    else{
      //Element hat Kind Elemente, also alle Elemente durchgehen (ist also eine Gruppe)
      for(var i=0; i<module.elements.length; i++)
        this.createElements(module.elements[i]);
    }
  }

  createElement(module){
    for(var i=0; i<this.baseElements.length; i++){
      if(this.baseElements[i].type == module.type){
        let guiElement = Object.assign({}, this.baseElements[i]);
        guiElement = Object.assign(guiElement, module);
        return guiElement;
      }
    }
  }

  addAdditionalElements(){
    //Vordefinierte zusätzliche Elemente erstellen
    let additionals = this.task.additionalElements;
    additionals.forEach((element) => {
      element.groups = ['additional'];
      this.additionalElements.push(this.createElement(element));
    });
  }//end addAdditionalElements

  resetPieces(){
    this.pieces.removeAll(true);
    this.pieces = game.add.group();
    this.raum.aufgabenLayer.add(this.pieces);
    if(this.hold != null){
      this.hold.destroy();
      this.hold = null;
    }
    this.guiChest.resetChest();
  }

  evaluate(){
    game.add.tween(this.guiZoneBG).to({alpha: (1)}, 1000, null, true, 0, 0, false);

    if(!this.raum.isSolved){
      this.tries++;

      if(this.lvlPoints != 0) dashboard.updatePoints(-this.lvlPoints); //Zuvor erreichte Punkte zurücksetzen

      let evaluator = new GUI_Evaluator(this, this.task, this.pieces, this.baseElements, this.guiElements, this.guiZoneGraphic);
      let points = evaluator.evaluate();
      // console.log("Punkte: " + points);

      this.showResult(evaluator);

      if(evaluator.result >= 70){
        this.lvlPoints = Math.round((points/10)/2);
        if(!this.raum.isSolved) dashboard.updateRoom();
        dashboard.updatePoints(this.lvlPoints);
        this.raum.openDoors();
      }
    }
  }//end evaluate

  showResult(evaluator){
    let unused = '';
    if(evaluator.unusedGuiElements.length != 0)
      unused = 'Sicher, dass du alle wichtigen Elemente benutzt hast?';

    let wronglyUsed = '';
    if(evaluator.wronglyUsedElements != 0)
        wronglyUsed = 'Überdenke am besten noch einmal\ndie Wahl der von dir benutzen Elemente';

    let congrats;
    if(evaluator.result < 70) congrats = 'Versuchs nochmal!';
    else                      congrats = 'Geschafft!';

    let info = '';
    if(evaluator.result < 70) info = '(Du benötigst mindestens 70% um das Level abzuschließen)';
    let skip = '';
    if(evaluator.result < 70 && !this.raum.isSolved && this.tries >= 3) skip += '\nDrücke \'N\' um das Level zu überpringen!';

    let textPercentage =  'Deine Bewertung: ' + Math.round(evaluator.result) + '%';

    let styles = {
      heading : {
        font: "30px Verdana",
        fontWeight: "bold",
        fill: "black",
        align: "center",
        wordWrapWidth: 300
      },
      info : {
        font: "14px Verdana",
        fill: "black",
        align: "center",
        wordWrapWidth: 300
      },
      normal : {
        font: "20px Verdana",
        fill: "black",
        align: "center",
        wordWrapWidth: 300
      }
    };

    this.hidePopup = false;

    this.popupBG = game.add.sprite((game.width/2)+(this.raum.width_offset/2), game.height/2, 'guiZoneWhite');
    this.popupBG.anchor.set(0.5);
    this.popupBG.alpha = 1;

    this.popupHeading = game.add.text((game.width+this.raum.width_offset)/2, (game.height/2), congrats, styles.heading);
    this.popupHeading.anchor.set(0.5);
    this.popupHeading.alpha = 0;
    if(!this.hidePopup) game.add.tween(this.popupHeading).to({alpha: 1}, 800, null, true, 0, 0, false);
    if(!this.hidePopup) game.add.tween(this.popupHeading.scale).to({x: 2, y: 2}, 800, Phaser.Easing.Quadratic.InOut);
    if(!this.hidePopup) game.time.events.add(700,
      function() {
        if(!this.hidePopup) game.add.tween(this.popupHeading).to({y: (game.height/2)-100}, 500, null, true, 0, 0, false);
        if(!this.hidePopup) game.time.events.add(700,
          function() {
            if(!this.hidePopup) this.popupPercent = this.enterText(textPercentage, (game.width+this.raum.width_offset)/2, (game.height/2)-50, styles.normal);
            if(!this.hidePopup) game.time.events.add(600,
              function() {
                if(!this.hidePopup) this.popupInfo = this.enterText(info, (game.width+this.raum.width_offset)/2, (game.height/2)-25, styles.info);
                if(!this.hidePopup) game.time.events.add(200,
                  function() {
                    if(!this.hidePopup) this.popupUnused = this.enterText(unused, (game.width+this.raum.width_offset)/2, (game.height/2)+10, styles.normal);
                    if(!this.hidePopup) game.time.events.add(200,
                      function() {
                        if(!this.hidePopup) this.popupWrongly = this.enterText(wronglyUsed, (game.width+this.raum.width_offset)/2, (game.height/2)+60, styles.normal);
                        if(!this.hidePopup) game.time.events.add(200,
                          function() {
                            if(!this.hidePopup) this.popupSkip = this.enterText(skip, (game.width+this.raum.width_offset)/2, (game.height/2)+73, styles.info);
                          },
                        this);
                      },
                    this);
                  },
                this);
              },
            this);
          },
        this);
      },
    this);

  }//end showResult

  enterText(text, x, y, style){
    let popupText = game.add.text(x, y-15, text, style);
    popupText.anchor.set(0.5);
    popupText.alpha = 0;
    game.add.tween(popupText).to({alpha: 1}, 300, null, true, 0, 0, false);
    game.add.tween(popupText).to({y: y}, 300, null, true, 0, 0, false);
    return popupText;
  }

  showBackground(){
    this.guiZoneBG.alpha = 1;
  }

  hideText(){
    this.hidePopup = true;
    if(this.popupBG != null)      game.add.tween(this.popupBG).to({alpha: (0)}, 500, null, true, 0, 0, false);
    if(this.popupHeading != null) game.add.tween(this.popupHeading).to({alpha: (0)}, 2000, null, true, 0, 0, false);
    if(this.popupInfo != null)    game.add.tween(this.popupInfo).to({alpha: (0)}, 2000, null, true, 0, 0, false);
    if(this.popupPercent != null) game.add.tween(this.popupPercent).to({alpha: (0)}, 2000, null, true, 0, 0, false);
    if(this.popupUnused != null)  game.add.tween(this.popupUnused).to({alpha: (0)}, 2000, null, true, 0, 0, false);
    if(this.popupWrongly != null) game.add.tween(this.popupWrongly).to({alpha: (0)}, 2000, null, true, 0, 0, false);
    if(this.popupSkip != null)    game.add.tween(this.popupSkip).to({alpha: (0)}, 2000, null, true, 0, 0, false);

    game.time.events.add(3500,
      function() {
        game.add.tween(this.guiZoneBG).to({alpha: (0)}, 1000, null, true, 0, 0, false);
      },
    this);;
  }

  shutdown(){
    this.elementsOnField = this.guiChest.guiElementsInUse;
    this.elementsInChest = this.guiChest.guiElementsInChest;

    if(this.elementsOnField != null){
      this.elementsOnField.forEach((element) => {
        //Positionen der bestehenden Elemente speichern
        var thePiece;
        this.pieces.forEach((piece) => {
          if(piece.item === element) thePiece = piece;
        })
        if(thePiece != null){
          element.xPos = thePiece.position.x;
          element.yPos = thePiece.position.y;
        }
      });
    }
  }//end shutdown

  placeExisting(){

    if(this.elementsOnField != null && this.elementsOnField.length != 0){
      this.elementsOnField.forEach((element) => {
        //die bestehenden Elemente an Ihrer alten Position wieder platzieren
        this.pieces.add(this.createPiece(element, element.xPos, element.yPos));
      });
    }
    else
      this.elementsOnField = [];
  }

}//end class
