// TODO: Check if rocks overlap, if yes, create no new rocks at this position
class Decorations {

    constructor() {
    }

    preload() {
        game.load.image('rock1', 'assets/room/decorations/rock1.png');
        game.load.image('rock2', 'assets/room/decorations/rock2.png');
        game.load.image('rock3', 'assets/room/decorations/rock3.png');
        game.load.image('rock4', 'assets/room/decorations/rock4.png');
        game.load.image('rock5', 'assets/room/decorations/rock5.png');
        game.load.image('crack1', 'assets/room/decorations/crack1.png');
        game.load.image('crack2', 'assets/room/decorations/crack2.png');
        game.load.image('crack3', 'assets/room/decorations/crack3.png');
        game.load.image('crack4', 'assets/room/decorations/crack4.png');
        game.load.image('plant1', 'assets/room/decorations/plant1.png');
        game.load.image('plant2', 'assets/room/decorations/plant2.png');
        game.load.image('plant3', 'assets/room/decorations/plant3.png');
        game.load.image('plant4', 'assets/room/decorations/plant4.png');
        game.load.image('plant5', 'assets/room/decorations/plant5.png');
        game.load.image('plant6', 'assets/room/decorations/plant6.png');
        game.load.image('plant7', 'assets/room/decorations/plant7.png');
        game.load.image('plant8', 'assets/room/decorations/plant8.png');
        game.load.spritesheet('rareRock1', 'assets/room/decorations/rock1Spritesheet.png', 128, 128, 7);
        game.load.spritesheet('rareRock2', 'assets/room/decorations/rock2Spritesheet.png', 128, 128, 7);
        game.load.spritesheet('rareRock4', 'assets/room/decorations/rock4Spritesheet.png', 128, 128, 7);
        game.load.spritesheet('rareRock5', 'assets/room/decorations/rock5Spritesheet.png', 128, 128, 7);
    }

    // Creates variables
    create(raum) {
        this.x = 0;
        this.y = 0;

        this.decoLayer = game.add.group();
        this.decoLayer.enableBody = true;

        this.raum = raum;
        this.raum.bodenLayer.add(this.decoLayer);

        this.alreadySet = false;
        this.placeExisting();

        if(this.alreadySet == false)
            this.allObjects = new Array(new Array());

        this.rockRect = new Array(new Array());
        this.plantRect = new Array(new Array());
        this.puddleRect = new Array(new Array());

        this.allRects = new Array(this.rockRect, this.plantRect, this.puddleRect);

        this.place = false;
        this.actionKeys = this.raum.spieler.actionKeys;
    }

    update() {
        for(var i = 1; i < this.allObjects.length; i++) {
            for(var a = 0; a < 4; a++) {
                if(this.allObjects[i][a] != null) {
                    if(this.allObjects[i][a].alpha == 0.99 && game.physics.arcade.distanceBetween(this.raum.spieler, this.allObjects[i][a]) < 100 && (this.actionJustUp())) {
                        dashboard.updatePoints(5);
                        if(this.allObjects[i][a].key == 'rareRock1') {
                            this.allObjects[i][a].loadTexture('rock1');
                            this.allObjects[i][a].alpha = 1;
                        } else if(this.allObjects[i][a].key == 'rareRock2') {
                            this.allObjects[i][a].loadTexture('rock2');
                            this.allObjects[i][a].alpha = 1;
                        } else if(this.allObjects[i][a].key == 'rareRock4') {
                            this.allObjects[i][a].loadTexture('rock4');
                            this.allObjects[i][a].alpha = 1;
                        } else if(this.allObjects[i][a].key == 'rareRock5') {
                            this.allObjects[i][a].loadTexture('rock5');
                            this.allObjects[i][a].alpha = 1;
                        }
                    } else if(this.allObjects[i].alpha == 0.99 && game.physics.arcade.distanceBetween(this.raum.spieler, this.allObjects[i]) < 100 && (this.actionJustUp())) {
                        dashboard.updatePoints(5);
                        if(this.allObjects[i].key == 'rareRock1') {
                            this.allObjects[i].loadTexture('rock1');
                            this.allObjects[i].alpha = 1;
                        } else if(this.allObjects[i].key == 'rareRock2') {
                            this.allObjects[i].loadTexture('rock2');
                            this.allObjects[i].alpha = 1;
                        } else if(this.allObjects[i].key == 'rareRock4') {
                            this.allObjects[i].loadTexture('rock4');
                            this.allObjects[i].alpha = 1;
                        } else if(this.allObjects[i].key == 'rareRock5') {
                            this.allObjects[i].loadTexture('rock5');
                            this.allObjects[i].alpha = 1;
                        }
                    }
                }
            }
        }
    }

    // Allows the User of the class to set infinite allowed areas, where rocks can be spawned
    setRockRect(rectCount, topLeftX, topLeftY, bottomRightX, bottomRightY) {
        this.rockRect[rectCount] = [topLeftX, topLeftY, bottomRightX-35, bottomRightY-35];
    }

    // Allows the User of the class to set infinite allowed areas, where rocks can be spawned
    setPlantRect(rectCount, topLeftX, topLeftY, bottomRightX, bottomRightY) {
        this.plantRect[rectCount] = [topLeftX, topLeftY, bottomRightX-35, bottomRightY-35];
    }

    // Allows the User of the class to set infinite allowed areas, where rocks can be spawned
    setPuddleRect(rectCount, topLeftX, topLeftY, bottomRightX, bottomRightY) {
        this.puddleRect[rectCount] = [topLeftX, topLeftY, bottomRightX-35, bottomRightY-35];
    }

    // Method which is called by User of class, genertes x and y value pairs for a single rock and checks them if they are in the allowed areas. If so: goes into addRocks(), if not: generates new values
    placeObjects(count, object) {
        if(!this.alreadySet) {
        this.count = count;
        switch(object) {
            case 'rocks': for(var i = 0; i < this.count; i++) {
                            this.calculateCoords(this.rockRect);
                            var rockGroup = Math.floor(Math.random() * 4) + 1;
                            this.addRocks(rockGroup);
                            this.place = false;
                          }
                // console.log(this.count + " Rockgroups Added.");
            break;
            case 'plant': for(var i = 0; i < this.count; i++) {
                            this.calculateCoords(this.plantRect);
                            this.addPlant();
                            this.place = false;
                          }
                // console.log(this.count + " Plants Added.");
            break;
            case 'puddle': for(var i = 0; i < this.count; i++) {
                            this.calculateCoords(this.puddleRect);
                            this.addPuddle();
                            this.place = false;
                          }
                // console.log(this.count + " Puddles Added.");
            break;
        }
        //console.log(this.allObjects);
        }

    }

    calculateCoords(rect) {
        this.x = Math.floor(Math.random() * game.width);
        this.y = Math.floor(Math.random() * game.height);
        var iterations = 0;

        while(this.place == false) {
            for(var rectCount = 0; rectCount < rect.length; rectCount++) {
                if((this.x > rect[rectCount][0] && this.x < rect[rectCount][2]) && (this.y > rect[rectCount][1] && this.y < rect[rectCount][3])) {
                    this.place = true;
                    for(var a = 0; a < this.allObjects.length; a++) {
                        if(this.allObjects[a] != null && Phaser.Math.distance(this.x, this.y, this.allObjects[a].x, this.allObjects[a].y) < 80) {
                            this.place = false;
                            this.x = Math.floor(Math.random() * game.width);
                            this.y = Math.floor(Math.random() * game.height);
                        }
                    }
                    break;
                } else {
                    this.x = Math.floor(Math.random() * game.width);
                    this.y = Math.floor(Math.random() * game.height);
                }
            }
            iterations++;
            if(iterations > 500) {
                rect = this.allRects[Math.floor(Math.random() * 2) + 0]
                // console.log("No space in this rect left, searching for space in other rect.");
                iterations = 0;
            }
        }
    }

    // Adds rocks in single-groups to groups of four. The first rock generated is saved into the first dimension of the array 'allObjects', every further rock is saved in the second dimension
    addRocks(rockCount) {
        var newRock;
        var whichRock = Math.floor(Math.random() * 6) + 1;
        var scaler;
        var rarePoss = false;

        if(whichRock == 2)
            scaler = 1.5;
        else if(whichRock == 3)
            scaler = 1.25;
        else if(whichRock == 5)
            scaler = 1.25;
        else
            scaler = 1;

        if(whichRock == 6)
            whichRock = 1;

        for(var i = 0; i < rockCount; i++) {
            switch(i) {
                case 0: newRock = game.add.sprite(this.x, this.y, 'rock' + whichRock); newRock.scale.setTo(0.5 * scaler, 0.5 * scaler); this.decoLayer.add(newRock);
                    this.allObjects[this.allObjects.length] = newRock; rarePoss = true;
                break;
                case 1: newRock = game.add.sprite(this.x + 25 * scaler, this.y, 'rock' + whichRock); newRock.scale.setTo(0.5 * scaler, 0.5 * scaler); this.decoLayer.add(newRock);
                    this.allObjects[this.allObjects.length-1][i-1] = newRock; rarePoss = true;
                break;
                case 2: newRock = game.add.sprite(this.x, this.y + 25 * scaler, 'rock' + whichRock); newRock.scale.setTo(0.5 * scaler, 0.5 * scaler); this.decoLayer.add(newRock);
                    this.allObjects[this.allObjects.length-1][i-1] = newRock; rarePoss = true;
                break;
                case 3: newRock = game.add.sprite(this.x + 25 * scaler, this.y + 25 * scaler, 'rock' + whichRock); newRock.scale.setTo(0.5 * scaler, 0.5 * scaler); this.decoLayer.add(newRock);
                    this.allObjects[this.allObjects.length-1][i-1] = newRock; rarePoss = true;
                break;
            }
            if((whichRock == 1 || whichRock == 5 || whichRock == 4 || whichRock == 2 || whichRock == 6) && rarePoss) {
                if(whichRock == 6)
                    whichRock = 1;
                this.createRareRocks(newRock, whichRock);
                rarePoss = false;
            }
        }
    }

    addPlant() {
        var whichPlant = Math.floor(Math.random() * 8) + 1;
        var newPlant = game.add.sprite(this.x, this.y, 'plant' + whichPlant);
        newPlant.scale.setTo(0.9, 0.9); this.decoLayer.add(newPlant);
        this.allObjects[this.allObjects.length] = newPlant;
    }

    addPuddle() {
        var whichCrack = Math.floor(Math.random() * 4) + 1;
        var newPuddle = game.add.sprite(this.x, this.y, 'crack' + whichCrack);
        newPuddle.scale.setTo(0.4, 0.4); this.decoLayer.add(newPuddle);
        this.allObjects[this.allObjects.length] = newPuddle;
    }

    // every rock has the probability of 1/16 the be drawn with lower alpha values
    createRareRocks(newRock, whichRock) {
        var rarity = Math.floor(Math.random() * 16) + 1;
        var scaler;

        if(whichRock == 2)
            scaler = 1.5;
        else
            scaler = 1;

        if(rarity == 1) {
            newRock.loadTexture('rareRock' + whichRock, 0);
            newRock.scale.setTo(0.5 * scaler, 0.5 * scaler)
            newRock.animations.add('shine');
            newRock.animations.play('shine', 10, true);
            newRock.alpha = 0.99;
        }
    }

    placeExisting() {
        var tempB = [];
        if(this.allObjects != null) {
            this.alreadySet = true;
            // console.log(this.allObjects);
            for(var a = 1; a < this.allObjects.length; a++) {
                for(var b = 0; b < 4; b++) {
                    // console.log(this.allObjects[a][b]);
                    if(!(typeof this.allObjects[a][b] === 'undefined') || this.allObjects[a][b] != null) {
                        // console.log("in b with: " + this.allObjects[a][b].key);
                        var newSecRock = game.add.sprite(this.allObjects[a][b].x, this.allObjects[a][b].y, this.allObjects[a][b].key);
                        this.allObjects[a][b] = newSecRock;
                        if(this.allObjects[a][b].key == "rock2") newSecRock.scale.setTo(0.5 * 1.5, 0.5 * 1.5);
                        if(this.allObjects[a][b].key == "rock1") newSecRock.scale.setTo(0.5, 0.5);
                        if(this.allObjects[a][b].key == "rock3") newSecRock.scale.setTo(0.5 * 1.25, 0.5 * 1.25);
                        if(this.allObjects[a][b].key == "rock4") newSecRock.scale.setTo(0.5, 0.5);
                        if(this.allObjects[a][b].key == "rock5") newSecRock.scale.setTo(0.5 * 1.25, 0.5 * 1.25);
                        if(this.allObjects[a][b].key == "rareRock2") newSecRock.scale.setTo(0.5 * 1.5, 0.5 * 1.5);
                        if(this.allObjects[a][b].key == "rareRock1") newSecRock.scale.setTo(0.5, 0.5);
                        if(this.allObjects[a][b].key == "rareRock4") newSecRock.scale.setTo(0.5, 0.5);
                        if(this.allObjects[a][b].key == "rareRock5") newSecRock.scale.setTo(0.5 * 1.25, 0.5 * 1.25);
                        this.decoLayer.add(newSecRock);
                    }
                }
                for(var b = 0; b < 4; b++) {
                    if(this.allObjects[a][b] != null) {
                        tempB[b] = this.allObjects[a][b];
                    }
                }
                var newObject = game.add.sprite(this.allObjects[a].x, this.allObjects[a].y, this.allObjects[a].key);
                this.allObjects[a] = newObject;

                // console.log("in a with: " + this.allObjects[a].key);

                if(this.allObjects[a].key.includes("plant")) newObject.scale.setTo(0.9, 0.9);
                if(this.allObjects[a].key.includes("crack")) newObject.scale.setTo(0.4, 0.4);
                if(this.allObjects[a].key == "rock2") newObject.scale.setTo(0.5 * 1.5, 0.5 * 1.5);
                if(this.allObjects[a].key == "rock1") newObject.scale.setTo(0.5, 0.5);
                if(this.allObjects[a].key == "rock3") newObject.scale.setTo(0.5 * 1.25, 0.5 * 1.25);
                if(this.allObjects[a].key == "rock4") newObject.scale.setTo(0.5, 0.5);
                if(this.allObjects[a].key == "rock5") newObject.scale.setTo(0.5 * 1.25, 0.5 * 1.25);
                if(this.allObjects[a].key == "rareRock2") newObject.scale.setTo(0.5 * 1.5, 0.5 * 1.5);
                if(this.allObjects[a].key == "rareRock1") newObject.scale.setTo(0.5, 0.5);
                if(this.allObjects[a].key == "rareRock4") newObject.scale.setTo(0.5, 0.5);
                if(this.allObjects[a].key == "rareRock5") newObject.scale.setTo(0.5 * 1.25, 0.5 * 1.25);

                for(var x = 0; x < tempB.length; x++) {
                    this.allObjects[a][x] = tempB[x];
                }

                this.decoLayer.add(newObject);
            }
        }
    }

    actionJustUp(){
		return this.actionKeys.space.justUp || this.actionKeys.enter.justUp || this.actionKeys.e.justUp;
	}

}
