class Bugkiller extends Aufgabe{

	constructor(thema, schwierigkeit){
		super(thema, schwierigkeit)

		// --- General
        this.bugkiller;

		// --- Minion related
        this.minion;
        this.minionLayer;
		this.minionSpawned = false;
        this.minionTime = 0;

        this.waveCount = 0;
        this.spawnedAnswers;

        // Parameter set by admin: How fast are the minions?
        this.minionVelocity;

        // Parameter set by admin: How much time is between the spawns?
        this.minionSpawnDelay;

        this.allMinions;
        this.minionCount = 0;

        this.maxMinions;

        // --- Spawn bools
        this.waveDone = true;
        this.startSpawn = false;

		// --- Bullet related
        this.bullet;
		this.bulletLayer;
		this.bulletTime = 0;

        // --- Welcome, Question and Answer related
        // Parameter set by admin: Welcometext and question.
		this.welcomeText;

        // Parameter set by admin: Array of answerRefs
        this.answerRefs;
        this.answerTexts;

        this.answers;
        this.rndRefs;
        this.maxQ;
        this.textLayer;

        // --- NPC related
        this.npcTriggered = false;

        // --- Point System related
        this.points = 0;

        this.right_points= 5;
        this.wrong_points= -5;

        this.pointMultiplicator = 1;

        //hilfsvariable für updateRoom
        this.roomIsInDashboard=false;

    }

	// Method for loading assets
    preload() {
        game.load.json('fragen', 'fragen/multipleChoice/' + this.thema + "/" + this.schwierigkeit + ".json");
        game.load.spritesheet('bug', 'assets/aufgaben/bugkiller/bugAnimationSprite.png', 256, 256, 4);
				game.load.image('bullet', 'assets/aufgaben/bugkiller/throwstone.png');
        game.load.image('street', 'assets/aufgaben/bugkiller/street.png');
        game.load.image('hole', 'assets/aufgaben/bugkiller/hole.png')
        game.load.image('npc', 'assets/female_npc.png');
        game.load.json('npcConvo', 'assets/bugkiller_npc.json');
        game.load.audio('bugsound', 'assets/aufgaben/bugkiller/bugsound.mp3');
        game.load.audio('throw', 'assets/aufgaben/bugkiller/throw.mp3');
        game.load.audio('hit', 'assets/aufgaben/bugkiller/hit.mp3');
        game.load.spritesheet('teleporter', 'assets/room/pressure_plate_bonus.png', 70, 70);
	}

	create(raum) {

        this.raum.deco.setRockRect(0, 100, 100, 300, 333);
        this.raum.deco.setRockRect(1, 600, 100, 800, 333);
        this.raum.deco.placeObjects(5, 'rocks');

        this.raum.deco.setPlantRect(0, 870, 100, 900, 333);
        this.raum.deco.setPlantRect(1, 300, 175, 600, 333);
        this.raum.deco.placeObjects(5, 'plant');

        this.raum.deco.setPuddleRect(0, 100, 100, 300, 333);
        this.raum.deco.placeObjects(2, 'puddle');

        this.fragenJSON = game.cache.getJSON('fragen');

        // the higher the value, the faster the minions
        this.minionVelocity = 150;

        // the lower the value, the smaller the time delay between the minions spawns
        this.minionSpawnDelay = 900;

	    // adding street
	    this.street = game.add.sprite((game.width+this.raum.width_offset) / 17, (2/3) * game.height, 'street');
		this.street.scale.setTo(1.25, 1);
        this.raum.aufgabenLayer.add(this.street);

        // adding hole
        this.startHole = game.add.sprite(this.street.x + 30, this.street.y - 50, 'hole');
        this.endHole = game.add.sprite(this.street.x + 750, this.street.y - 50, 'hole');
        this.startHole.scale.setTo(0.5, 0.5);
        this.endHole.scale.setTo(0.5, 0.5);
        this.raum.aufgabenLayer.add(this.startHole);
        this.raum.aufgabenLayer.add(this.endHole);

		// adding NPC
		this.npc = new NPC(((game.width + this.raum.width_offset) / 2), game.height / 4, this.raum);
		this.npc.setGraphic("npc");
		this.npc.convo = game.cache.getJSON('npcConvo');

        // adding minion group and creating sprite
        this.minionLayer = game.add.group();
		this.minionLayer.enableBody = true;
        this.minionLayer.createMultiple(1, 'bug');
        this.bugAudio = game.add.audio('bugsound');
        this.hitAudio = game.add.audio('hit');

		// adding and configuration of bulletLayer
		this.bulletLayer = game.add.group();
	    this.bulletLayer.enableBody = true;
	    this.bulletLayer.physicsBodyType = Phaser.Physics.ARCADE;
	    this.bulletLayer.createMultiple(10, 'bullet');
	    this.bulletLayer.callAll('events.onOutOfBounds.add', 'events.onOutOfBounds', this.resetBullet, this);
	    this.bulletLayer.setAll('checkWorldBounds', true);
        this.throwAudio = game.add.audio('throw');

		this.keyboard = game.input.keyboard;

        this.allMinions = new Array();
        this.spawnedAnswers = new Array();

        dashboard.setPoints(this.raum.thema,"Bugkiller");

        // adding textLayer
        this.textLayer = game.add.group();
		this.textLayer.enableBody = true;

        this.textStyle = {
            font: "12px Verdana",
            fill: "#ffffff",
            align: "center",
            wordWrap: true,
            wordWrapWidth: 125
        };

        this.rndRefs = new Array();

        if(this.fragenJSON.length > 4) this.maxQ = 4
        else this.maxQ = this.fragenJSON.length;

        var done = false;

        for(var i = 0; i < this.maxQ; i++) {
            while(done == false) {
                var generatedNumber = Math.floor(Math.random() * (this.fragenJSON.length));
                if(this.rndRefs.includes(generatedNumber)) {
                    done = false;
                } else {
                    this.rndRefs.push(generatedNumber);
                    done = true;
                }
            }
            done = false;
        }

        this.answers = new Array(this.fragenJSON[this.rndRefs[0]].answers, this.fragenJSON[this.rndRefs[1]].answers, this.fragenJSON[this.rndRefs[2]].answers, this.fragenJSON[this.rndRefs[3]].answers);
        this.answerRefs = new Array(this.fragenJSON[this.rndRefs[0]].choices, this.fragenJSON[this.rndRefs[1]].choices, this.fragenJSON[this.rndRefs[2]].choices, this.fragenJSON[this.rndRefs[3]].choices);
        this.answerTexts = new Array(new Array());

        for(var a = 0; a < this.answerRefs.length; a++) {
            var result = [];
            for(var i = 0; i < Object.keys(this.fragenJSON[this.rndRefs[a]].choices).length; i++) {
                result.push(this.answerRefs[a][String.fromCharCode(97 + i)]);
            }
            this.answerTexts.push(result);
        }

        for(var i = 0; i < this.answerRefs.length; i++) {
            var result = [];
            for(var answer in this.answerRefs[i]) {
                result.push(answer);
            }
            this.answerRefs[i] = result;
        }

        this.maxMinions = this.answerRefs[0].length;

        for(var a = 0; a < this.answerTexts.length; a++) {
            for(var i = 0; i < this.answerTexts[a].length; i++) {
                this.answerTexts[a][i] = game.add.text(1000, 1000, this.answerTexts[a][i], this.textStyle);
                this.textLayer.add(this.answerTexts[a][i]);
            }
        }

        this.raum.spieler.addAction(this.shoot, this);

		this.actionKeys = this.raum.spieler.actionKeys;

	}

	update() {

		if(this.teleporter != null) this.teleporter.listen((this.thema+'BonusRun'), null, null, this.teleporter);
		else if(this.raum.dungeonCompleted)	this.teleporter = new Pressure_Plate(game.width - 180 + this.raum.width_offset, (game.height/2)-100, 'teleporter', this.raum);

        // destroy minions from last update (prevent phaser from destroying a null reference and running into an exception). Only needed for collision and overlap detection (see overlap with bullet and minion)
        for(var i=0; i < this.allMinions.length; i++) {
            if(this.allMinions[i] != null && this.allMinions[i].destroy_in_next_tick) {
                this.allMinions[i].destroy(true);
                this.allMinions[i] = null;
            }
        }

        //let start = this.actionKeys.space.justDown || this.actionKeys.enter.justDown || this.actionKeys.e.justDown;
        
        if(this.waveDone && (this.actionJustUp()))
            this.activateNPC(this.waveCount);

        if(this.npcTriggered == true && this.answerRefs[this.waveCount] != null && this.waveDone == true && this.startSpawn == false && this.actionJustUp) {
            this.raum.closeDoors();
            this.startSpawn = true;
            this.npcTriggered = false;
            this.allMinions = [];
            this.waveCount += 1;
            this.maxMinions = this.answerRefs[this.waveCount - 1].length;

            // console.log("Welle: " + this.waveCount);
            // console.log("In dieser Welle kommen " + this.maxMinions + " Minions!");

            switch(this.waveCount) {
                case 1:
                    this.setDifficulty('easy');
                    // console.log("Schwierigkeit der Welle ist: easy!");
                break;
                case 2:
                    this.setDifficulty('medium');
                    // console.log("Schwierigkeit der Welle ist: medium!");
                break;
                case 3:
                    this.setDifficulty('hard');
                    // console.log("Schwierigkeit der Welle ist: hard!");
                break;
                default:
                    this.setDifficulty('insane');
                    // console.log("Schwierigkeit der Welle ist: insane!");
                break;
            }

            this.waveDone = false;
        }

        // start spawning minions, if task has been started
        if(this.startSpawn == true) {
            this.spawnMinions();
            if(this.minionCount >= this.maxMinions) {
                this.startSpawn = false;
            }
        }

        var tempPoints = this.points;

        // iterate through minion list, check if minion collides with either the end of the room or a bullet, destroy the minion and count points
        for(var i = 0; i < this.allMinions.length; i++) {
            if(this.allMinions[i] != null && this.allMinions[i].x > (this.endHole.x + 50)) {
                if(this.answers[this.waveCount-1].includes(this.allMinions[i].text)) {

                    this.points += (1 * this.pointMultiplicator);
                    dashboard.updatePoints(this.right_points * this.pointMultiplicator);

                } else {

                    this.points -= 1;
                    dashboard.updatePoints(this.wrong_points);
                }
                this.allMinions[i].destroy();
                this.allMinions[i].realText.kill();
                this.allMinions[i] = null;
            }

            if(this.allMinions[i] != null) {
                game.physics.arcade.overlap(this.bulletLayer, this.allMinions[i], () => {
                    this.hitAudio.play();

                    if(this.answers[this.waveCount-1].includes(this.allMinions[i].text)) {
                        this.points -= 1;
                        dashboard.updatePoints(this.wrong_points);

                    } else {

                        this.points += (1 * this.pointMultiplicator);
                        dashboard.updatePoints(this.right_points * this.pointMultiplicator);

                    }
                    this.allMinions[i].destroy_in_next_tick = true;
                    this.allMinions[i].realText.destroy();
                });
            }

            if(this.points < 0) {
                this.points = 0;
            }
        }

        // BUG: If no minion is on the field, this thing gets triggered and the wave ends too early
        if(this.waveDone == false && this.allMinions.length == this.maxMinions && this.allMinions[this.allMinions.length - 1] == null) {
            this.waveDone = true;
            this.raum.openDoors();
            if(!this.roomIsInDashboard){
                dashboard.updateRoom();
                this.roomIsInDashboard=true;
            }
            this.spawnedAnswers = [];
            this.startSpawn = false;
            this.minionCount = 0;
            // console.log("Welle " + this.waveCount + " zuende!");
        }

        // TODO: Open Doors after every wave, close it when wave begins
        // ATM: Open doors after last wave
        if(this.waveCount == this.answerRefs.length && this.allMinions[this.allMinions.length - 1] == null && !this.raum.isSolved && this.allMinions.length == this.maxMinions){
            this.raum.openDoors();
            if(!this.roomIsInDashboard){
                dashboard.updateRoom();
            }
        }

	}

    // Method creates minions and gets it related parts and variables
	spawnMinions() {
        if (game.time.now > this.minionTime)
    	{
            this.minion = this.minionLayer.getFirstExists(false);

            if(this.minion) {

                // Generates a random number, also checks, if the number was already generated earlier. In that case a new number gets generated
                let randomNumber = Math.floor(Math.random() * this.answerRefs[this.waveCount - 1].length);
                let alreadyIn = true;

                while(alreadyIn) {
                    if(this.spawnedAnswers.includes(randomNumber)) {
                        randomNumber = Math.floor(Math.random() * this.answerRefs[this.waveCount - 1].length);
                    } else {
                        alreadyIn = false;
                    }
                }
                this.spawnedAnswers[this.minionCount] = randomNumber;

                // Minion with random answer gets spawned
                var newMinion = new Minion(this, this.startHole.x + 50, (2/3) * game.height, this.answerRefs[this.waveCount - 1][randomNumber], this.answerTexts[this.waveCount][randomNumber]);
                newMinion.anchor.set(0.5);
                newMinion.scale.setTo(0.75, 0.75);
                var walk = newMinion.animations.add('walk');
                newMinion.animations.play('walk', 10, true);

                if(!this.bugAudio.isPlaying)
                    this.bugAudio.play();

                newMinion.body.width = 64;
                newMinion.body.height = 20;
                newMinion.body.y += 20;
                this.allMinions[this.minionCount] = newMinion;
                this.minionCount++;

                this.minionSpawned = true;
                newMinion.body.velocity.x = this.minionVelocity;

                newMinion.realText.anchor.set(0.5);
                newMinion.realText.x = newMinion.x;
                newMinion.realText.y = newMinion.y;
                newMinion.realText.body.velocity.x = this.minionVelocity;

                this.minionTime = game.time.now + this.minionSpawnDelay;
            }
        }

    }

	// Method for shooting bullets
    shoot(vx, vy, xoffset, yoffset) {
        if(!this.waveDone) {

        vx = 0; vy = 800; xoffset = -10; yoffset = -25;

		// prevent spamming (attack speed regulation)
		if (game.time.now > this.bulletTime)
    	{
    		// find first existing bullet, that doesnt exist in world context for now
	        this.bullet = this.bulletLayer.getFirstExists(false);

	        // fire bullet
	        if (this.bullet) {
	            this.bullet.reset(this.raum.spieler.x + xoffset, this.raum.spieler.y + yoffset);
                this.bullet.anchor.set(0.5);
                this.bullet.scale.set(0.2, 0.2);
	            this.bullet.body.velocity.x = vx;
	            this.bullet.body.velocity.y = vy;
                this.throwAudio.play();
	            // the attack delay (actual attack speed)
	            this.bulletTime = game.time.now + 500;
	        }
    	}
        }
	}

	// Method for destroying a bullet
	resetBullet(bullet) {
    	bullet.kill();
	}

    setDifficulty(dif) {
        switch(dif) {
            case 'easy':
                this.minionVelocity = 150;
                this.minionSpawnDelay = 2000;
                this.pointMultiplicator = 1;
            break;
            case 'medium':
                this.minionVelocity = 200;
                this.minionSpawnDelay = 1300;
                this.pointMultiplicator = 2;
            break;
            case 'hard':
                this.minionVelocity = 250;
                this.minionSpawnDelay = 1000;
                this.pointMultiplicator = 3;
            break;
            case 'insane':
                this.minionVelocity = 300;
                this.minionSpawnDelay = 900;
                this.pointMultiplicator = 5;
            break;
            default:
                this.minionVelocity = 150;
                this.minionSpawnDelay = 900;
                this.pointMultiplicator = 1;
            break;

        }
    }

    activateNPC(wave) {
        var statementPre = 'statement_';
        var statementNumber = 0;
        var statement;

        if(game.physics.arcade.distanceBetween(this.raum.spieler, this.npc) < 70){

            // Set where the Conversation should end at each wave (or: Statement Number, where the quastion is stored)
            switch(wave) {
                case 0: statementNumber = 6;
                    break;
                case 1: statementNumber = 10;
                    break;
                case 2: statementNumber = 12;
                    break;
                case 3: statementNumber = 14;
                    break;
                default: statementNumber = 14;
                    wave = 3;
                    break;
            }

            statement = "" + statementPre + statementNumber;

            this.npc.changeText(statement, this.fragenJSON[this.rndRefs[wave]].question);

            // Update Conversation at the start of each wave
            if(this.waveCount == wave && this.waveDone && this.npcTriggered == false) {
                this.npc.togglePauseConvo(false);
                this.npc.updateConvo();
            }
            if(this.waveCount == wave && this.npc.convoAt == statement && this.npc.convoPaused == false) {
                this.npcTriggered = true;
                this.npc.togglePauseConvo(true);
            }
            // End Conversation
            if(this.waveCount == 4 && this.waveDone) {
                this.npc.togglePauseConvo(false);
                this.npc.updateConvo();
            }
		}
    }

	actionJustUp(){
		return this.actionKeys.space.justDown || this.actionKeys.enter.justDown || this.actionKeys.e.justDown;
	}

    render() {
        if (game.config.enableDebug) {
            game.debug.text(this.points, 150, 800);
        }
    }

}
