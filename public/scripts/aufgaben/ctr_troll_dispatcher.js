class Troll_Dispatcher{
    
    
    constructor(raum,ctr){
        this.raum=raum;
        this.spieler=this.raum.spieler;
        this.ctr=ctr;
        
        this.npc1;
		this.npc1_x;
		this.npc1_y;
		this.npc2;
		this.npc2_x;
		this.npc2_y;
	
		this.npcarray;
        this.npcLayer=game.add.group();
        this.npcarray_old;
        this.activenpc = 0;
        
        this.zielX;
        this.zielY;
        
        this.message_audio = game.add.audio('message');
    }
    
    playPunchAnimation(){
        
        if(this.raum.spieler.spawnPos=="down"){
           this.npcarray[this.activenpc].sprite.animations.play('hit_down'); 
        }
        else if(this.raum.spieler.spawnPos=="up"){
              this.npcarray[this.activenpc].sprite.animations.play('hit_up');    
                }
         else if(this.raum.spieler.spawnPos=="left"){
              this.npcarray[this.activenpc].sprite.animations.play('hit_left');    
                }
         else if(this.raum.spieler.spawnPos=="right"){
              this.npcarray[this.activenpc].sprite.animations.play('hit_right');    
                }
        
        
        
       
        
        
    }
    
    removeIsTriggered(){
       
    }
    
     saveCurrentPositions(){
         
         
                this.ctr.npc1_x = this.npc1.sprite.x;
				 this.ctr.npc1_y = this.npc1.sprite.y;
				 this.ctr.npc2_x = this.npc2.sprite.x;
				 this.ctr.npc2_y = this.npc2.sprite.y;
		
         
         
      
         
         
    }
    
    getNextActive(){
        if (this.activenpc < this.npcarray.length - 1) {

							this.activenpc = this.activenpc + 1;
							
						} else {
							//letzten NPC gewonnen
							this.ctr.winsGame = true;
							this.activenpc = -1;
                             this.saveCurrentPositions();

						}

						
    }
    
    decrementActiveNPC(){
     
        this.activenpc = this.activenpc -1;
        
       
    }
    
    checkFlag0(){
        	if (this.ctr.flag==0 && this.activenpc >= 0 && Phaser.Math.distance(this.raum.spieler.x, this.raum.spieler.y, this.zielX, this.zielY) < 5) {
					this.raum.spieler.body.velocity.y = 0;
					this.ctr.flag = 1;
				
			
					if (this.activenpc > 0) {
			

						this.activenpc = this.activenpc - 1;

					

					}



					if (dashboard.tries_left == 0) {
						this.activenpc = -1;

						this.ctr.dialog = new Dialog(this.raum.spieler.x, this.raum.spieler.y, "Oh nein!Leider verloren.", "", "", "", "");
						game.time.events.add(Phaser.Timer.SECOND * 1, this.ctr.hideHeader, this);
						this.ctr.winsGame = false;
						this.raum.quake.start();
						this.raum.openDoors();
                        dashboard.updateRoom();
                        
                      this.saveCurrentPositions();

						this.ctr.flag = 1;
					}

					this.raum.spieler.isPushed = false;
					this.raum.spieler.isMovable = true;

				}
    }
    
    checkPlayerWentTooFar(){

        	if (
                ( 
                    this.raum.spieler.spawnPos == "up" && ((this.raum.spieler.y+48) > this.npcarray[this.activenpc].sprite.y)&&!((this.npcarray[this.activenpc].sprite.x<this.raum.spieler.x&&this.raum.spieler.x<(this.npcarray[this.activenpc].sprite.x+this.npcarray[this.activenpc].sprite.width))&&this.playerIsNearNPC()) 
                )||
                (
                    this.raum.spieler.spawnPos == "down" &&((this.raum.spieler.y-48)< this.npcarray[this.activenpc].sprite.y)&&!((this.npcarray[this.activenpc].sprite.x<this.raum.spieler.x&&this.raum.spieler.x<(this.npcarray[this.activenpc].sprite.x+this.npcarray[this.activenpc].sprite.width))&&this.playerIsNearNPC()) 
                )||
                (
                    this.raum.spieler.spawnPos == "right" && ((this.raum.spieler.x-100) < this.npcarray[this.activenpc].sprite.x)&&!((((this.npcarray[this.activenpc].sprite.y-20)<this.raum.spieler.y)&&(this.raum.spieler.y<(this.npcarray[this.activenpc].sprite.y+35)))&&this.playerIsNearNPC()) 
                )
                ||
                (
                    this.raum.spieler.spawnPos == "left" && ((this.raum.spieler.x+100) > this.npcarray[this.activenpc].sprite.x)&&!((((this.npcarray[this.activenpc].sprite.y-20)<this.raum.spieler.y)&&(this.raum.spieler.y<(this.npcarray[this.activenpc].sprite.y+35)))&&this.playerIsNearNPC()) 
                )
            ) {
					
        
					this.ctr.flag = -5;

					this.message_audio.play('', 0, 0.3, false, true);
					
				
                    this.npcarray[this.activenpc].trigger();
                   

					this.raum.spieler.isMovable = false;
				}
    }
    
    
   
    
    playerIsNearNPC()
    {
        return(Phaser.Math.distance(this.raum.spieler.x, this.raum.spieler.y, this.npcarray[this.activenpc].sprite.x, this.npcarray[this.activenpc].sprite.y) < 80 && this.raum.spieler.isMovable);
    }
    
    collide(){
            game.physics.arcade.collide(this.raum.spieler, this.npc1.sprite);
			game.physics.arcade.collide(this.raum.spieler, this.npc2.sprite);

    }

				

    
    
    setTrollsReturnChest(){
        var chest2;
        
		if (!this.raum.isSolved) {
          
			


			var lootypos = game.height / 2;
			var lootxpos = (game.width + this.raum.width_offset) / 2;

			if (this.raum.spieler.y == 30) {
            
                 this.npc1=new Troll((game.width + this.raum.width_offset) / 4, game.height -260,this.raum,this.ctr);
                 this.npc2=new Troll(2 * (game.width + this.raum.width_offset) / 4, game.height - 330,this.raum,this.ctr);
          
                this.npcarray = [ this.npc2, this.npc1];
				lootypos = game.height - 200;
				this.raum.spieler.spawnPos = "up";
                
            
		
                
			} else if (this.raum.spieler.y == game.height - 40) {
                
                
                 this.npc1=new Troll((game.width + this.raum.width_offset) / 4, 370,this.raum,this.ctr);
                 this.npc2=new Troll(3 * (game.width + this.raum.width_offset) / 4,300,this.raum,this.ctr);
             
                
				this.raum.spieler.spawnPos = "down";
                this.npcarray = [this.npc1, this.npc2];
				lootypos = 150;
                lootxpos=(game.width+offset)/2-200;
			} else if (this.raum.spieler.x == (game.width + this.raum.width_offset) - 30 && this.raum.spieler.y == game.height / 2) {
                
                
                 
                 this.npc1=new Troll((2 * (game.width + this.raum.width_offset) / 3) - 300, game.height / 2,this.raum,this.ctr);
                 this.npc2=new Troll((2 * (game.width + this.raum.width_offset) / 3) - 150, game.height / 2,this.raum,this.ctr);
               
                
				this.raum.spieler.spawnPos = "right";
			

			
                this.npcarray = [this.npc2, this.npc1];
	

				lootxpos = 200;


			} else {
				this.raum.spieler.spawnPos = "left";

                     this.npc1=new Troll((2 * (game.width + this.raum.width_offset) / 3) - 300, game.height / 2,this.raum,this.ctr);
                 this.npc2=new Troll((2 * (game.width + this.raum.width_offset) / 3) - 150, game.height / 2,this.raum,this.ctr);
        
                this.npcarray = [this.npc1, this.npc2];
	
				lootxpos = (game.width + this.raum.width_offset) - 200;
			}


			this.lootgroup = game.add.group();
			this.lootgroup.enableBody = true;


			chest2 = new Chest(lootxpos, lootypos, false, this.raum);


		

		}
    
        else {

		
			dashboard.setPoints(this.raum.thema,"Cross the Room");
            this.npc1 = new Troll( this.ctr.npc1_x, this.ctr.npc1_y, this.ctr.raum,this.ctr);
            this.npc2 = new Troll( this.ctr.npc2_x,  this.ctr.npc2_y, this.ctr.raum,this.ctr);

		}
        
          this.npcLayer.add(this.npc1.sprite);
        this.npcLayer.add(this.npc2.sprite);
        
        this.raum.aufgabenLayer.add(this.npcLayer);
      
        
        
        return chest2;
    }
    
    triggerActiveNPC(){
        this.message_audio.play();
        return this.npcarray[this.activenpc].trigger();
    }
    
    
    
    
}