class IceRun extends Aufgabe {


    constructor(raum) {
        super(raum)

    
        this.hole_middle;
        this.ice_neutral;
        this.ice_wrong;
        this.ice_right;

        this.realHole;
        this.holeLeft;
        this.holeRight;

        this.iceGrid
        this.goal;

        this.chest;
        this.chestSpawned = false;
		this.teleporter;


    }


    preload() {
           
        game.load.image('ice_neutral1', 'assets/aufgaben/ice_run/ice_neutral1.png');
        game.load.image('ice_wrong1', 'assets/aufgaben/ice_run/ice_wrong1.png');
        game.load.image('ice_right1', 'assets/aufgaben/ice_run/ice_right1.png');
        
        game.load.image('ice_neutral2', 'assets/aufgaben/ice_run/ice_neutral2.png');
        game.load.image('ice_wrong2', 'assets/aufgaben/ice_run/ice_wrong2.png');
        game.load.image('ice_right2', 'assets/aufgaben/ice_run/ice_right2.png');
        
        game.load.image('ice_neutral3', 'assets/aufgaben/ice_run/ice_neutral3.png');
        game.load.image('ice_wrong3', 'assets/aufgaben/ice_run/ice_wrong3.png');
        game.load.image('ice_right3', 'assets/aufgaben/ice_run/ice_right3.png');
        
        game.load.image('realHole', 'assets/aufgaben/ice_run/holeRoom.png')
        game.load.image('holeLeft', 'assets/aufgaben/ice_run/holeLeft.png')
        game.load.image('holeRight', 'assets/aufgaben/ice_run/holeRight.png')
        game.load.spritesheet('teleporter', 'assets/room/pressure_plate_bonus.png', 70, 70);
        game.load.spritesheet('chest', 'assets/truhe.png', 128, 128);

    }



    create() {
        //randomize function
        function randomInt(min, max) {
            return Math.floor(Math.random() * (max - min + 1) + min);
        }

        this.raum.spieler.setHealth(100);

        //ziel definieren
        this.goal = this.raum.aufgabenLayer.create();
        this.goal.body.setSize(240, 10);
        this.goal.x = 392;

        let chestY;

        if (this.raum.spieler.y > game.height / 2) {
            this.goal.y = 150;
            chestY = 125;
        } else {
            this.goal.y = 400;
            chestY = 430;
        }


        //true
		
		let keys = Object.keys(this.raum.connections);
		let conCount = 0;
		// Jeder Tür Prüfen
		keys.forEach((key) => {

			// prüfen ob hinter der Tür ein Raum ist
			if (this.raum.connections[key] != null &&
				this.raum.connections[key] != 'blocked') 
			{
				conCount++;
			}

		});
		
		if (!this.raum.isSolved && conCount == 1) {
            this.chest = new Chest(300, chestY, false, this.raum);
            this.chestSpawned = true;
        }
        // loch bauen

        this.realHole = game.add.group();
        this.realHole.enableBody = true;

        this.holeLeft = game.add.group();
        this.holeLeft.enableBody = true;
        this.holeRight = game.add.group();
        this.holeRight.enableBody = true;


        //ice neutral bauen
        this.ice_neutral = game.add.group();
        this.ice_neutral.enableBody = true;


        //ice_wrong bauen
        this.ice_wrong = game.add.group();
        this.ice_wrong.enableBody = true;


        //ice_right bauen
        this.ice_right = game.add.group();
        this.ice_right.enableBody = true;

        // aufgaben elemente in layer übertragen
     //   this.raum.aufgabenLayer.add(this.hole);
     //   this.raum.aufgabenLayer.add(this.hole_middle);
        this.raum.aufgabenLayer.add(this.realHole);
        this.raum.aufgabenLayer.add(this.holeLeft);
        this.raum.aufgabenLayer.add(this.holeRight);
        this.raum.aufgabenLayer.add(this.ice_wrong);
        this.raum.aufgabenLayer.add(this.ice_right);
        this.raum.aufgabenLayer.add(this.ice_neutral);



        //hole erstellen
        this.holeLeft.create(86, 160, 'holeLeft');
        this.holeRight.create(632, 160, 'holeRight');
        //this.hole_middle.create(392, 160, 'hole_middle');

        this.realHole.create(86, 160, 'realHole');

        //sichtbare löcher kann man nicht verschieben
       // this.hole.setAll('body.immovable', true);

        this.realHole.setAll('body.immovable', true);
        this.holeLeft.setAll('body.immovable', true);
        this.holeRight.setAll('body.immovable', true);


        //position der ersten eisfläche
        var xstart = 392;
        var ystart = 160;
        
        

        if (!this.raum.isSolved) {
            //initiere icegrid
            this.iceGrid = new Array(6);
            for (var i = 0; i <= 5; i++) {
                this.iceGrid[i] = new Array(6);
            }
            //icegrid bauen mit alle flächen falsch
            for (var x = 0; x <= 5; x++) {
                for (var y = 0; y <= 5; y++) {
                    this.iceGrid[x][y] = 'ice_wrong'+randomInt(1,3);
                }
            }




            //einen weg mit richtigen flächen bauen
            var y = 0;
            var x = randomInt(0, 5);
            this.iceGrid[x][y] = 'ice_right';
            var xrnd;
            while (y < 5) {
                if (x == 0) {
                    xrnd = randomInt(x, x + 1);
                    if (x == xrnd) {
                        y++;
                        this.iceGrid[x][y] = 'ice_right';
                    } else {
                        x = xrnd;
                        this.iceGrid[x][y] = 'ice_right';
                    }
                } else if (x == 5) {
                    xrnd = randomInt(x - 1, x);
                    if (x == xrnd) {
                        y++;
                        this.iceGrid[x][y] = 'ice_right';
                    } else {
                        x = xrnd;
                        this.iceGrid[x][y] = 'ice_right';
                    }
                } else {
                    xrnd = randomInt(x - 1, x + 1);
                    if (x == xrnd) {
                        y++;
                        this.iceGrid[x][y] = 'ice_right';
                    } else {
                        x = xrnd;
                        this.iceGrid[x][y] = 'ice_right';
                    }
                }
            }
            //eisfläche mit neutralem ice überdecken
            for (var x = 0; x <= 5; x++) {

                for (var y = 0; y <= 5; y++) {

                    var xcoord = xstart + x * 40;

                    var ycoord = ystart + y * 40;

                    this.ice_neutral.create(xcoord, (ycoord), 'ice_neutral'+randomInt(1,3));

                }
            }

            this.raum.spieler.body.moves = false;
            this.ice_neutral.visible = false;


            game.time.events.add(2000, () => {
                this.raum.spieler.body.moves = true
                this.ice_neutral.visible = true;
            }, this)

        }


        //eisfläche spawnen
        for (var x = 0; x <= 5; x++) {
            for (var y = 0; y <= 5; y++) {
                var xcoord = xstart + x * 40;
                var ycoord = ystart + y * 40;
                if (this.iceGrid[x][y] == 'ice_right') {
                    this.ice_right.create(xcoord, (ycoord), 'ice_right'+randomInt(1,3));
                } else {

                    this.ice_wrong.create(xcoord, (ycoord), 'ice_wrong'+randomInt(1,3));
                }
            }
        }

        dashboard.setPoints(this.raum.thema, "IceRun");
		
		//teleporter
		if (this.teleporter != null) this.teleporter = new Pressure_Plate(800, 120, 'teleporter', this.raum);

    }



    update() {
//teleporter
        if (this.teleporter != null) this.teleporter.listen((this.thema + 'BonusRun'), null, null, this.teleporter);
        else if (this.raum.dungeonCompleted) this.teleporter = new Pressure_Plate(800, 120, 'teleporter', this.raum);
        
        // goal interaktion
        game.physics.arcade.overlap(this.raum.spieler, this.goal, () => {
            if (!this.raum.isSolved) {
                // lets the earthquake begin
                this.raum.quake.start();
                dashboard.updatePoints(20);
                dashboard.updateRoom();
                this.raum.openDoors();
                if (this.chestSpawned) {
                    this.chest.toggleUnlock(true);
                }
            }


        })

        // collision mit loch

        game.physics.arcade.collide(this.raum.spieler, this.holeLeft);
        game.physics.arcade.collide(this.raum.spieler, this.holeRight);

        //collision mit true
        if (this.chestSpawned) {
            game.physics.arcade.collide(this.raum.spieler, this.chest.chest);

            if (game.physics.arcade.distanceBetween(this.raum.spieler, this.chest) < 70) {
                if (this.chest.unlocked && !this.chest.opened) {
                    this.chest.open();
                    dashboard.updatePoints(20);
                    //dashboard.updateRoom();

                    let style = {
                        font: "25px Verdana",
                        fill: "#000000",
                        align: "center",
                        wordWrapWidth: 300,
                    }
                    this.popup = game.add.text((game.width + this.raum.width_offset) / 2, (game.height / 2) - 50, "20 Bonus Punkte!", style);
                    this.popup.anchor.set(0.5);
                    this.popup.alpha = 0;

                    game.add.tween(this.popup).to({
                        alpha: 1
                    }, 800, null, true, 0, 0, false);
                    game.add.tween(this.popup).to({
                        y: (game.height / 2)
                    }, 1000, null, true, 0, 0, false);

                }
            }

        }
        game.physics.arcade.overlap(this.raum.spieler, this.ice_neutral, (spieler, ice) => {
            ice.visible = false;

        });

        game.physics.arcade.overlap(this.raum.spieler, this.ice_wrong, (spieler, ice) => {
            //
            this.raum.spieler.body.moves = true;
            ice.visible = false;
            this.raum.spieler.damage(10);
            this.raum.spieler.alpha = this.raum.spieler.alpha - 0.1;

            if (!this.raum.spieler.alive) this.raum.state.restart();

            console.log('life: ' + this.raum.spieler.Health);
        })

    }

    render() {

        this.ice_right.forEach(ice => {
            game.debug.body(ice, 'rgba(0,255,0,0.5)');
        });

        game.debug.body(this.goal, 'rgba(0,255,0,0.5)');


    }

    blockMe(raum) {
        raum.connections.left = 'blocked';
        raum.connections.right = 'blocked';
    }



}