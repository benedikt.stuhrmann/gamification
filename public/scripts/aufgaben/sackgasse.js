class DeadEnd extends Raum{
  constructor(key){
    super();
    this.down = key;
    this.isSolved = false;
    this.aufgabe = null;
  }

	preload() {
		super.preload();
	}

	create() {
    super.create();

    var style = {
      font: "Verdana",
      fontSize: "30px",
      fill: "#ffffff",
      align: "center",
      wordWrapWidth: 300,
      boundsAlignH: "center",
      boundsAlignV: "middle"
    }
    var msg = game.add.text((game.width+offset)/2, (game.height/2)-50, "Game Over! \n Falsche Antwort!", style);
    msg.anchor.set(0.5);
    let msgTween = game.add.tween(msg).to({y: (game.height/2)}, 1000, null, true, 0, 0, false);

    game.time.events.add(Phaser.Timer.SECOND * 2, this.reset, this);

    let count = Math.floor(Math.random() * 7) + 1;
    this.deco.setRockRect(0, 140, 140, 1024-140, 576-140);
    this.deco.placeObjects(count, 'rocks');
    count = Math.floor(Math.random() * 4) + 1;
    this.deco.setPlantRect(0, 140, 140, 1024-140, 576-140);
    this.deco.placeObjects(4, 'plant');
    count = Math.floor(Math.random() * 2) + 1;
    this.deco.setPuddleRect(0, 140, 140, 1024-140, 576-140);
    this.deco.placeObjects(1, 'puddle');
  }

  update() {
    super.update();
  }

  reset(){
    this.openDoor("down", true);
  }

  blockMe(raum){
    raum.connections.up = 'blocked';
    raum.connections.right = 'blocked';
    raum.connections.left = 'blocked';
  }
}
