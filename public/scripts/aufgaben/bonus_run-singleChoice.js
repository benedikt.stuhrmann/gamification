class SingleChoice_BR extends Aufgabe{
  constructor(key, question){
    super();

    this.bonusRunKey = key;
    this.nextRoom = this.bonusRunKey;
    this.prevRoom = this.bonusRunKey;

    this.question = question;
    this.signs;
    this.triggers;
    this.answeredCorrectly = null;
    this.timeLeft;
    this.entranceID = 2;
    this.correctExit;
    this.timer;

    this.opened = false;
  }

  init(params){
    this.answeredCorrectly = this.raum.params.correctAnswers;
    this.timeLeft = this.raum.params.timeLeft;
    this.entranceID = this.entranceDoorID(this.raum.params.from);
  }

  preload() {
		super.preload();

    game.load.image('sign', 'assets/sign.png');

    game.load.audio('error', 'assets/audio/code_puzzle/Error.wav');
    game.load.audio('gong', 'assets/audio/code_puzzle/Gong.wav');
    game.load.audio('ticking', 'assets/audio/bonus_run/timer_ticking.wav');
    game.load.audio('alarm', 'assets/audio/bonus_run/alarm.wav');
	}

  create() {
    super.create();

    this.errorSFX = game.add.audio('error');
    this.gongSFX = game.add.audio('gong');
    this.tickingSFX = game.add.audio('ticking');
    this.alarmSFX = game.add.audio('alarm');

    this.spawnPlayer();

    this.placeSigns();

    this.setEntrance();
    this.setExit();
    this.setDeadEnds();

    this.showQuestion();

    this.triggers = this.raum.roomTrigger.children;

    this.raum.spieler.speed = 350;

    dashboard.setPoints(this.raum.thema,"BonusRun");
    dashboard.enable(2,this.timeLeft,this);

    let count = Math.floor(Math.random() * 7) + 1;
    this.raum.deco.setRockRect(0, 140, 140, 1024-140, 576-140);
    this.raum.deco.placeObjects(count, 'rocks');
    count = Math.floor(Math.random() * 4) + 1;
    this.raum.deco.setPlantRect(0, 140, 140, 1024-140, 576-140);
    this.raum.deco.placeObjects(4, 'plant');
    count = Math.floor(Math.random() * 2) + 1;
    this.raum.deco.setPuddleRect(0, 140, 140, 1024-140, 576-140);
    this.raum.deco.placeObjects(1, 'puddle');

    this.tickingSFX.play("", 0, 10, true);
	}

	update() {
    super.update();

    if(!this.opened){
      this.doors();
      this.opened = true;
    }

    for(var i=0; i<this.signs.length; i++)
      this.signs[i].update();

    //Trigger testen
    for(var i=0; i<this.triggers.length; i++){
      this.triggers[i].body.onOverlap = new Phaser.Signal();

      switch(i){
        case 0: this.triggers[i].body.onOverlap.add(this.triggerdTopDoor, this);
          break;
        case 1: this.triggers[i].body.onOverlap.add(this.triggerdRightDoor, this);
          break;
        case 2: this.triggers[i].body.onOverlap.add(this.triggerdBottomDoor, this);
          break;
        case 3: this.triggers[i].body.onOverlap.add(this.triggerdLeftDoor, this);
          break;
      }
    }

    dashboard.updateTimer(this);
  }

  triggerdTopDoor(){
    if(this.correctExit == 0){
      this.answeredCorrectly++;
      dashboard.updatePoints(5);
      this.gongSFX.play();
    }
    else
      this.errorSFX.play();

    this.updateParams();
    this.raum.setLastRoom();
    game.state.start(this.raum.connections.up, true, false, this.raum.params);
  }

  triggerdRightDoor(){
    if(this.correctExit == 1){
      this.answeredCorrectly++;
      dashboard.updatePoints(5);
      this.gongSFX.play();
    }
    else
      this.errorSFX.play();

    this.updateParams();
    this.raum.setLastRoom();
    game.state.start(this.raum.connections.right, true, false, this.raum.params);
  }

  triggerdBottomDoor(){
    if(this.correctExit == 2){
      this.answeredCorrectly++;
      dashboard.updatePoints(5);
      this.gongSFX.play();
    }
    else
      this.errorSFX.play();

    this.updateParams();
    this.raum.setLastRoom();
    game.state.start(this.raum.connections.down, true, false, this.raum.params);
  }

  triggerdLeftDoor(){
    if(this.correctExit == 3){
      this.answeredCorrectly++;
      dashboard.updatePoints(5);
      this.gongSFX.play();
    }
    else
      this.errorSFX.play();

    this.updateParams();
    this.raum.setLastRoom();
    game.state.start(this.raum.connections.left, true, false, this.raum.params);
  }

  getQuestion(){
    return this.question;
  }

  outOfTime(){
    this.hasEnded=true;
    this.raum.closeDoors();

    this.alarmSFX.play();

    var style = {
      font: "Verdana",
      fontSize: "30px",
      fill: "#ffffff",
      align: "center",
      wordWrapWidth: 300,
      boundsAlignH: "center",
      boundsAlignV: "middle"
    }
    this.qText.destroy();
    var msg = game.add.text((game.width+this.raum.width_offset)/2, (game.height/2)-50, "Deine Zeit ist um! \nZurück zum Start!", style);
    msg.anchor.set(0.5);
    let msgTween = game.add.tween(msg).to({y: (game.height/2)}, 1000, null, true, 0, 0, false);

    game.time.events.add(Phaser.Timer.SECOND * 3, this.reset, this);
    this.tickingSFX.stop();
  }

  reset(){
    this.raum.setLastRoom();
    game.state.start(this.bonusRunKey, true, false, this.raum.params);
  }

  updateParams(){
    this.raum.params.correctAnswers = this.answeredCorrectly;
    this.raum.params.timeLeft = this.timeLeft;
    this.raum.params.from = this.correctExit;
    this.tickingSFX.stop();
  }

  formatTime(s) {
      var minutes = "0" + Math.floor(s / 60);
      var seconds = "0" + (s - minutes * 60);
      return minutes.substr(-2) + ":" + seconds.substr(-2);
  }

  setEntrance(entrance){
    this.entranceID = entrance;
  }

  placeSigns(){
    this.answerTexts = [
      this.question.getAnswer(0),
      this.question.getAnswer(1),
      this.question.getAnswer(2)
    ];
    this.answerNums = [
      Math.random(0,100),
      Math.random(0,100),
      Math.random(0,100)
    ];
    this.signs = [
      new Sign(((game.width+this.raum.width_offset)/2) + 100, 40, 0, this.raum.spieler, this.raum, 'sign'), //oben
      new Sign((game.width+this.raum.width_offset)- 40, (game.height/2) + 100, 90, this.raum.spieler, this.raum, 'sign'), //rechts
      new Sign(((game.width+this.raum.width_offset)/2) - 100, game.height - 40, 180, this.raum.spieler, this.raum, 'sign'), //unten
      new Sign(40, (game.height/2) - 100, -90, this.raum.spieler, this.raum, 'sign'), //links
    ];

    var exitSet = false;
    for(var i=0; i<4; i++){
      if(i != this.entranceID){
        //Frage platzieren

        //Dem Schild den Text der Antwort mit dem höchsten Wert in answerNums geben
        let index = this.indexOfMax(this.answerNums);
        if(index == 0 && !exitSet){
          this.correctExit = i;
          exitSet = true;
        }
        this.signs[i].setText(this.answerTexts[index]);
        this.answerTexts.splice(index, 1);
        this.answerNums.splice(index, 1);
      }else{
        //weiter
      }
    }

    this.signs[this.entranceID].destroy();
    this.signs.splice(this.entranceID, 1);
  }

  indexOfMax(array) {
    var max = array[0];
    var maxID = 0;

    for (var i=1; i<array.length; i++) {
        if (array[i] > max) {
            maxID = i;
            max = array[i];
        }
    }

    return maxID;
  }

  entranceDoorID(answerID){
    switch(answerID){
      case 0: return 2;
        break;
      case 1: return 3;
        break;
      case 2: return 0;
        break;
      case 3: return 1;
        break;
      default: return -1;
    }
  }

  getCorrectExit(){
    return this.correctExit;
  }

  setNextRoom(key){
    this.nextRoom = key;
  }

  setPrevRoom(key){
    this.prevRoom = key;
  }

  setEntrance(){
    switch(this.entranceID) {
      case 0: this.raum.up = this.prevRoom;
        break;
      case 1: this.raum.right = this.prevRoom;
        break;
      case 2: this.raum.down = this.prevRoom;
        break;
      case 3: this.raum.left = this.prevRoom;
        break;
    }
  }

  setExit(){
    switch(this.correctExit) {
      case 0: this.raum.up = this.nextRoom;
        break;
      case 1: this.raum.right = this.nextRoom;
        break;
      case 2: this.raum.down = this.nextRoom;
        break;
      case 3: this.raum.left = this.nextRoom;
        break;
    }
  }

  setDeadEnds(){
    //Sackgasse erstellen
		var deadEnd = new DeadEnd(this.bonusRunKey);
		game.state.add('Sackgasse', deadEnd);

    //Verbindungsnamen in Array speichern
		let keys = Object.keys(this.raum.connections);
		//Jede Tür Prüfen
		keys.forEach((key) => {
			//prüfen, ob bereits ein Raum hinter dieser Tür liegt
			if (this.raum.connections[key] == null)
        //wenn nicht: Sackgasse
				this.raum.connections[key] = 'Sackgasse';
		});
  }

  showQuestion(){
    //Fragestellung anzeigen
    var style = {
      font: "Verdana",
      fontSize: "25px",
      fill: "#ffffff",
      //backgroundColor: "rgba(67, 72, 79, 0.8)",
      align: "center",
      wordWrap: true,
      wordWrapWidth: 700,
      boundsAlignH: "center",
      boundsAlignV: "middle"
    }

    this.qText = game.add.text(((game.width+this.raum.width_offset)/2), (game.height/2) - 10, this.question.getQuestion(), style);
    this.qText.anchor.set(0.5);
    this.qText.alpha = 0;
    game.add.tween(this.qText).to({alpha: 1}, 300, null, true, 0, 0, false);
    game.add.tween(this.qText).to({y: (game.height/2)}, 500, null, true, 0, 0, false);
  }

  doors(){
    //Türen öffnen
    if(this.nextRoom != this.bonusRunKey){
      if(this.entranceID != 0)
        this.raum.openDoor('up', false);
      if(this.entranceID != 1)
        this.raum.openDoor('right', false);
      if(this.entranceID != 2)
        this.raum.openDoor('down', false);
      if(this.entranceID != 3)
        this.raum.openDoor('left', false);
    }
    else{
      if(this.entranceID != 0)
        this.raum.openDoor('up', true);
      if(this.entranceID != 1)
        this.raum.openDoor('right', true);
      if(this.entranceID != 2)
        this.raum.openDoor('down', true);
      if(this.entranceID != 3)
        this.raum.openDoor('left', true);
    }
  }

  spawnPlayer(){
    let x;
    let y;
    //Spieler löschen
    //this.raum.spieler.destroy();

    //setze Koordinaten an Eingangstür-Position
		switch (this.entranceID) {
			case 0: //oben
				x = (game.width+this.raum.width_offset) / 2;
				y = 110;
				break;
			case 1: //rechts
				x = (game.width+this.raum.width_offset) - 110;
				y = game.height / 2;
				break;
			case 2: //unten
				x =(game.width+this.raum.width_offset) / 2;
				y = game.height - 100;
				break;
			case 3: //links
				x = 110;
				y = game.height / 2;
				break;
			default:
				x = (game.width+this.raum.width_offset) / 2;
				y = game.height / 2;
		}

    //Spieler Position neu setzen
    this.raum.spieler.x = x;
    this.raum.spieler.y = y;
  }

}
