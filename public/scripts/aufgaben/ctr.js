//Tasks:

//--> Position speichern und bei Wiedereintritt laden

class CTR extends Aufgabe {

	constructor(thema, schwierigkeit) {
		super(thema, schwierigkeit)

       this.dispatcher;


		this.red_btn;
		this.lootgroup;
		this.chest;
		this.chest2;

        this.npc1_x;
        this.npc1_y;

         this.npc2_x;
        this.npc2_y;

         this.npc3_x;
        this.npc3_y;



		this.punkteText;
		this.punkte = 0;
		this.wrong_points = -5;
		this.right_points = 10;

		this.phaserJSON;
		this.questioncounter = 0;

		this.flag = 1;
		this.isFocussed = 1;

		this.keyboard;
		this.cursors;


		this.dialogHeader;
		this.antwort1;
		this.antwort2;
		this.antwort3;
		this.antwort4;
		this.antwortid;
		this.blackbox0;
		this.blackbox;
		this.blackbox2;
		var textbubble;

        this.npcLayer;



		this.zielY;
		this.zielX;

		this.winsGame;

		var message_audio;

		var isPlayed = false;

		var i = 0;

		var booleantest;

		this.dialog;


	}

	preload() {
		game.load.image('lootchest', 'assets/aufgaben/ctr/truhe.png', 32, 32);
		game.load.spritesheet('npc', 'assets/aufgaben/ctr/SpriteTroll.png', 128 ,  256);
        game.load.image('heart', 'assets/heart.png');
		game.load.json('fragen', 'fragen/singleChoice/' + this.thema + '/' + this.schwierigkeit + '.json');
		game.load.image('red_btn', 'assets/aufgaben/red_btn/red_button.png');
		game.load.audio('message', 'assets/audio/troll.wav');
		game.load.audio('win', 'assets/audio/successful.mp3');
		game.load.spritesheet('chest', 'assets/truhe.png', 128, 128);
		game.load.spritesheet('teleporter', 'assets/room/pressure_plate_bonus.png', 70, 70);
	}


	create(raum) {


         this.dispatcher=new Troll_Dispatcher(this.raum,this);
        this.npcLayer = game.add.group();
		this.npcLayer.enableBody = true;
        this.chest2=this.dispatcher.setTrollsReturnChest();

        	this.phaserJSON = game.cache.getJSON('fragen');
			dashboard.setPoints(this.raum.thema,"Cross the Room");
			dashboard.enable(1);
			this.raum.spieler.addAction(this.doAction, this);
            this.keyboard = game.input.keyboard;
			this.cursors = this.keyboard.createCursorKeys();
			this.message_audio = game.add.audio('message');
			this.win_audio = game.add.audio('win');



		    let count = Math.floor(Math.random() * 7) + 1;
             this.raum.deco.setRockRect(0, 140, 140, 280, 576-140);
                this.raum.deco.setRockRect(1, 280, 240, 720, 576-140);
                this.raum.deco.setRockRect(2, 720, 140, 1024-140, 576-140);
            this.raum.deco.placeObjects(count, 'rocks');
            count = Math.floor(Math.random() * 4) + 1;
                this.raum.deco.setRockRect(0, 140, 140, 280, 576-140);
                this.raum.deco.setRockRect(1, 280, 220, 720, 576-140);
                this.raum.deco.setRockRect(2, 720, 140, 1024-140, 576-140);
            this.raum.deco.placeObjects(4, 'plant');
            count = Math.floor(Math.random() * 2) + 1;
                this.raum.deco.setRockRect(0, 140, 140, 280, 576-140);
                this.raum.deco.setRockRect(1, 280, 220, 720, 576-140);
                this.raum.deco.setRockRect(2, 720, 140, 1024-140, 576-140);
            this.raum.deco.placeObjects(1, 'puddle');





	}

	update() {


		if(this.teleporter != null) this.teleporter.listen((this.thema+'BonusRun'), null, null, this.teleporter);
		else if(this.raum.dungeonCompleted)	this.teleporter = new Pressure_Plate(300, 300, 'teleporter', this.raum);

        if(this.chest2!=null)
		game.physics.arcade.collide(this.raum.spieler, this.chest2.chest);

		if (!this.raum.isSolved) {





            	if (this.flag==0 && this.dispatcher.activenpc >= 0 && Phaser.Math.distance(this.raum.spieler.x, this.raum.spieler.y, this.zielX, this.zielY) < 5) {
					this.raum.spieler.body.velocity.y = 0;
					this.flag = 1;

                    this.dispatcher.npcarray[this.dispatcher.activenpc].sprite.animations.stop();
			         this.dispatcher.npcarray[this.dispatcher.activenpc].sprite.frame= this.dispatcher.npcarray[this.dispatcher.activenpc].default_frame;


					if (this.dispatcher.activenpc > 0) {
                        this.dispatcher.decrementActiveNPC();

					}



					if (dashboard.tries_left == 0) {
						this.dispatcher.activenpc = -1;

						this.dialog = new Dialog(this.raum.spieler.x, this.raum.spieler.y, "Oh nein!Leider verloren.", "", "", "", "");
						game.time.events.add(Phaser.Timer.SECOND * 1, this.hideHeader, this);
						this.winsGame = false;
						this.raum.quake.start();
						this.raum.openDoors();
                        dashboard.updateRoom();

                      this.dispatcher.saveCurrentPositions();

						this.flag = 1;
					}

					this.raum.spieler.isPushed = false;
					this.raum.spieler.isMovable = true;

				}

            else {
                	this.dispatcher.collide();
			}
			if (this.dispatcher.activenpc >= 0 && this.flag == 1 && this.flag != -5) {

                this.dispatcher.checkPlayerWentTooFar();

			}



			if (this.flag == -1) {

				if (this.keyboard.isDown(Phaser.KeyCode.ENTER) || this.keyboard.isDown(Phaser.KeyCode.SPACEBAR)   ||  this.keyboard.isDown(Phaser.KeyCode.E) ) {

					if (this.isFocussed == this.antwortid) {
						this.raum.spieler.isMovable = true;
						this.flag = 1;



						this.leben = this.leben + 1;
						this.dialog.hide(1);

						dashboard.updatePoints(this.right_points);

						this.punkte = this.punkte + this.right_points;

						this.textbubble = game.add.text(this.raum.spieler.x, this.raum.spieler.y - 30, dashboard.points_made, {
							fontSize: '32px',
							fill: 'green'
						});

						game.time.events.add(1000, function () {
							game.add.tween(this.textbubble).to({
								y: 0
							}, 1500, Phaser.Easing.Linear.None, true);
							game.add.tween(this.textbubble).to({
								alpha: 0
							}, 1500, Phaser.Easing.Linear.None, true);
						}, this);




						 this.dispatcher.removeIsTriggered();



						game.time.events.add(Phaser.Timer.SECOND * 2, this.hideHeader, this);



                        this.dispatcher.getNextActive();

					}
                    else {
						this.flag = 0;
						dashboard.updatePoints(this.wrong_points);

						this.textbubble = game.add.text(this.raum.spieler.x, this.raum.spieler.y - 30, dashboard.points_made, {
							fontSize: '32px',
							fill: 'red'
						});


						this.dialog.hide(0);
						game.time.events.add(Phaser.Timer.SECOND * 1, this.hideHeader, this);

						game.time.events.add(1000, function () {
							game.add.tween(this.textbubble).to({
								y: 0
							}, 1500, Phaser.Easing.Linear.None, true);
							game.add.tween(this.textbubble).to({
								alpha: 0
							}, 1500, Phaser.Easing.Linear.None, true);
						}, this);

						this.raum.spieler.body.velocity.x = 0;
						this.raum.spieler.body.velocity.y = 0;

						this.zielX = this.raum.spieler.x;
						this.zielY = this.raum.spieler.y;
                        this.dispatcher.zielX=this.zielX;
                        this.dispatcher.zielY=this.zielY;

						if (this.raum.spieler.spawnPos == "down") {
							this.zielY = this.raum.spieler.y + 80;
						}
						if (this.raum.spieler.spawnPos == "up") {
							this.zielY = this.raum.spieler.y - 80;
						}
						if (this.raum.spieler.spawnPos == "left") {

							this.zielX = this.raum.spieler.x - 180;
						} else if (this.raum.spieler.spawnPos == "right") {
							this.zielX = this.raum.spieler.x + 200;
						}


						this.raum.spieler.isPushed = true;
                        this.dispatcher.playPunchAnimation();



                        game.time.events.add(200,  function () {
							game.physics.arcade.moveToXY(this.raum.spieler, this.zielX, (this.zielY), 0, 500);
						}, this);


					}
				}

				if (this.isFocussed == "a") {
					if (this.cursors.right.isDown||this.keyboard.isDown(Phaser.KeyCode.D)) {
						this.dialog.colorAnswer(2);
						this.isFocussed = "b";
					}
					if (this.cursors.down.isDown||this.keyboard.isDown(Phaser.KeyCode.S)) {
						this.isFocussed = "c";
						this.dialog.colorAnswer(3);
					}
				} else if (this.isFocussed == "b") {
					if (this.cursors.left.isDown||this.keyboard.isDown(Phaser.KeyCode.A)) {
						this.dialog.colorAnswer(1);
						this.isFocussed = "a";
					}
					if (this.cursors.down.isDown||this.keyboard.isDown(Phaser.KeyCode.S)) {
						this.dialog.colorAnswer(4);
						this.isFocussed = "d";
					}
				} else if (this.isFocussed == "c") {
					if (this.cursors.up.isDown||this.keyboard.isDown(Phaser.KeyCode.W)) {
						this.dialog.colorAnswer(1);
						this.isFocussed = "a";
					}
					if (this.cursors.right.isDown||this.keyboard.isDown(Phaser.KeyCode.D)) {
						this.dialog.colorAnswer(4);
						this.isFocussed = "d";
					}
				} else if (this.isFocussed == "d") {
					if (this.cursors.up.isDown||this.keyboard.isDown(Phaser.KeyCode.W)) {
						this.dialog.colorAnswer(2);
						this.isFocussed = "b";
					}
					if (this.cursors.left.isDown||this.keyboard.isDown(Phaser.KeyCode.A)) {
						this.dialog.colorAnswer(3);
						this.isFocussed = "c";
					}
				}

			}





		} else {
			this.dispatcher.collide();
		}

    }



	hideHeader() {


		this.dialog.destroy();
	}

	fadeBubble() {

		while (this.textbubble.y > 0) {

			this.textbubble.y = this.textbubble.y - 1;
			game.time.events.add(1000, this.fadeBubble, this);
		}
	}



	displayQuestion(x,y) {
            var speech=game.rnd.pick(this.phaserJSON);
            this.dialog = new Dialog(x,y,
			speech.question,
			speech.choices.a,
			speech.choices.b,
			speech.choices.c,
			speech.choices.d);




		this.antwortid = game.rnd.pick(speech.answers);
		this.dialog.colorAnswer(1);
		this.isFocussed = "a";


		this.flag = -1;
	}


	doAction() {
		//Ansprechen des NPCs

		if (this.flag != -1 && this.dispatcher.activenpc >= 0 && this.dispatcher.playerIsNearNPC()) {
			this.flag = -5;
            this.dispatcher.triggerActiveNPC();
		}


		//Interaktion mit Truhe
		if (game.physics.arcade.distanceBetween(this.raum.spieler, this.chest2) < 50) {
			if (this.winsGame == true && !this.raum.isSolved) {

				this.chest2.open();

                let style = {
					font: "25px Verdana",
					fill: "#ffffff",
					align: "center",
					wordWrapWidth: 300,
				}
				this.popup = game.add.text((game.width + this.raum.width_offset) / 2, (game.height / 2) - 50, "Spiel gewonnen!\n" + "Deine Belohnung!\n" + "20% Bonus", style);
				this.popup.anchor.set(0.5);
				this.popup.alpha = 0;
				dashboard.setMultiplier(this.raum.thema,2 * 10);
				game.add.tween(this.popup).to({
					alpha: 1
				}, 800, null, true, 0, 0, false);
				game.add.tween(this.popup).to({
					y: (game.height / 2)
				}, 1000, null, true, 0, 0, false);
				game.time.events.add(Phaser.Timer.SECOND * 4, this.hideTween, this);

				dashboard.updatePoints(20);
                 dashboard.updateRoom();
                this.raum.quake.start();
				this.raum.openDoors();

			}
		}
	}




	hideTween() {
		game.add.tween(this.popup).to({
			alpha: (0)
		}, 2000, null, true, 0, 0, false);

	}

}
