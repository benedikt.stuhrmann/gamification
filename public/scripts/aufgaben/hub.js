class Hub extends Aufgabe {

	constructor(raum) {
		super(raum)

		this.spieler;

		this.signs = {
			oop1: {
				npc: null,
				sign: null
			},
			oop2: {
				npc: null,
				sign: null
			},
			mci: {
				npc: null,
				sign: null
			},
			isy: {
				npc: null,
				sign: null
			}
		}

		this.convo = {
			oop1: {
				"question_1": {
					"text": "Wie schwer soll der Dungeon sein?",
					"answer_1": {
						"text": "Einfach",
						"callback": "easy",
						"next": "question_1",
						dungeon: "oop1"
					},
					"answer_2": {
						"text": "Mittel",
						"callback": "medium",
						"next": "question_1",
						dungeon: "oop1"
					},
					"answer_3": {
						"text": "Schwer",
						"callback": "hard",
						"next": "question_1",
						dungeon: "oop1"
					}
				}
			},
			oop2: {
				"question_1": {
					"text": "Wie schwer soll der Dungeon sein?",
					"answer_1": {
						"text": "Einfach",
						"callback": "easy",
						"next": "question_1",
						dungeon: "oop2"
					},
					"answer_2": {
						"text": "Mittel",
						"callback": "medium",
						"next": "question_1",
						dungeon: "oop2"
					},
					"answer_3": {
						"text": "Schwer",
						"callback": "hard",
						"next": "question_1",
						dungeon: "oop2"
					}

				}
			},
			mci: {
				"question_1": {
					"text": "Wie schwer soll der Dungeon sein?",
					"answer_1": {
						"text": "Einfach",
						"callback": "easy",
						"next": "question_1",
						dungeon: "mci"
					},
					"answer_2": {
						"text": "Mittel",
						"callback": "medium",
						"next": "question_1",
						dungeon: "mci"
					},
					"answer_3": {
						"text": "Schwer",
						"callback": "hard",
						"next": "question_1",
						dungeon: "mci"
					}

				}
			},
			isy: {
				"question_1": {
					"text": "Wie schwer soll der Dungeon sein?",
					"answer_1": {
						"text": "Einfach",
						"callback": "easy",
						"next": "question_1",
						dungeon: "isy"
					},
					"answer_2": {
						"text": "Mittel",
						"callback": "medium",
						"next": "question_1",
						dungeon: "isy"
					},
					"answer_3": {
						"text": "Schwer",
						"callback": "hard",
						"next": "question_1",
						dungeon: "isy"
					}
				}
			}
		};

	}

	init(params) {

	}

	preload() {
		game.load.image('sign', 'assets/sign.png');

        //tests für highscoreeintrag
        game.load.nineSlice('input', 'assets/inputfield.png', 15);
        game.load.nineSlice('btn', 'assets/btn_clean.png', 20, 23, 27, 28);
	}

	create() {

		this.spieler = this.raum.spieler;

		this.signs.oop1.npc = new NPC(630, 45, this.raum);
		this.signs.oop1.npc.setGraphic();
		this.signs.oop1.npc.convo = this.convo.oop1;

		this.signs.oop1.sign = new Sign(630, 45, 0, this.spieler, this.raum, 'sign');
		this.signs.oop1.sign.setText('OOP1 Dungeon' + dashboard.exportHighscore("OOP1"));

		this.signs.oop2.npc = new NPC(980, 400, this.raum);
		this.signs.oop2.npc.setGraphic();
		this.signs.oop2.npc.convo = this.convo.oop2;

		this.signs.oop2.sign = new Sign(980, 400, 90, this.spieler, this.raum, 'sign');
		this.signs.oop2.sign.setText('OOP2 Dungeon' + dashboard.exportHighscore("OOP2"));

        //hochgesetzt , wurde bei 525 abgeschnitten
		this.signs.mci.npc = new NPC(390, 460, this.raum);
		this.signs.mci.npc.setGraphic();
		this.signs.mci.npc.convo = this.convo.mci;

		this.signs.mci.sign = new Sign(390, 525, 180, this.spieler, this.raum, 'sign');
		this.signs.mci.sign.setText('MCI Dungeon' + dashboard.exportHighscore("MCI"));

		this.signs.isy.npc = new NPC(45, 180, this.raum);
		this.signs.isy.npc.setGraphic();
		this.signs.isy.npc.convo = this.convo.isy;

		this.signs.isy.sign = new Sign(45, 180, 270, this.spieler, this.raum, 'sign');
		this.signs.isy.sign.setText('ISY Dungeon' + dashboard.exportHighscore("ISY"));

		dashboard.setPoints("", "hub", true);

		this.spieler.addAction(this.talk, this);

		this.spieler.grabRange = 100;

		let style = {
			fontSize: 23,
			fill: '#000000',
			stroke: '#bababa',
			strokeThickness: 2,
			wordWrap: true,
			wordWrapWidth: 250
		}

		let wasd = 'benutze W A S D oder die Pfeiltasten um dich zu bewegen';
		let action = 'mit E, SPACE oder ENTER kannst du interagieren'

		let text = game.add.text(120, 140, wasd + '\n\n\n\n\n\n' + action, style);
		text.alpha = 0.15;
		text.lineSpacing = -8;
		this.raum.bodenLayer.add(text);
	}

	update() {

		// steuerung aufsetzten
		let up = this.raum.cursors.up.justDown || this.raum.wasd.w.justDown;
		let down = this.raum.cursors.down.justDown || this.raum.wasd.s.justDown;
		let left = this.raum.cursors.left.justDown || this.raum.wasd.a.justDown;
		let right = this.raum.cursors.right.justDown || this.raum.wasd.d.justDown;

		let signs = Object.keys(this.signs);

		signs.forEach(sign => {

			if (up)
				this.signs[sign].npc.updateSelection("y-");
			if (left)
				this.signs[sign].npc.updateSelection("x-");
			if (down)
				this.signs[sign].npc.updateSelection("y+");
			if (right)
				this.signs[sign].npc.updateSelection("x+");

			this.signs[sign].sign.update()
		});




	}

	render() {

	}

	blockMe(raum) {

	}

	talk() {

		let signKeys = Object.keys(this.signs);

		signKeys.forEach(key => {
			if (this.spieler.isInRange(this.signs[key].sign.sign)) {
				this.signs[key].npc.togglePauseConvo(false);
				this.signs[key].npc.updateConvo();
			}
		});
	}

	toggleDoors() {

		let dungeonKeys = Object.keys(dungeons);

		dungeonKeys.forEach((key) => dungeons[key].createDungeon(1, 7));

		if (!this.raum.isSolved) {
			this.raum.openDoors();
		} else {
			this.raum.isSolved = false;
			this.raum.closeDoors();
			console.log("close");
		}

	}

	npcCallback(element) {



		switch (element.callback) {
			case "easy":
				dungeons[element.dungeon.toUpperCase()].createDungeon(1, 5);
				break;
			case "medium":
				dungeons[element.dungeon.toUpperCase()].createDungeon(2, 9);
				break;
			case "hard":
				dungeons[element.dungeon.toUpperCase()].createDungeon(3, 15);
				break;
			default:
				break;
		}
		this.signs[element.dungeon].npc.togglePauseConvo(true);

		let conKeys = Object.keys(this.raum.connections);

		conKeys.forEach(key => {
			if (this.raum.connections[key].includes(element.dungeon.toUpperCase()))
				this.raum.openDoor(key);
		});




	}

}
