// Minion class represents an answer to a specific question. A minion has its sprite, position and a state of its answer (if answer is correct or not). The text is set in the bugkiller.js script
class Minion extends Phaser.Sprite {

    constructor(raum, x, y, text, realText) {

		super(game, x, y, 'bug');
        
        this.text = text;
        this.realText = realText;
        
        raum.minionLayer.add(this);
	}
}