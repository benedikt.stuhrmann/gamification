


class Troll {
	constructor(x, y, raum,ctr) {
       
        this.ctr=ctr;
        this.default_frame;
   
        
        if(raum.spieler.spawnPos=="down"){
            this.default_frame=12;
            
        }
        else if(raum.spieler.spawnPos=="up"){
             this.default_frame=4;
     
        }
        else if(raum.spieler.spawnPos=="right"){
             this.default_frame=8;
          
        }
        else{
             this.default_frame=0;
        
        }
        
         this.sprite=game.add.sprite(x,y,'npc',this.default_frame);
        
        
		this.width = 0;
		this.height = 0;
		this.raum = raum;
	  
        
  

        
		this.sprite.anchor.set(0.5, 1);
		this.sprite.animations.add('left', [17, 18, 19, 20,21], 10, true);
        this.sprite.animations.add('right', [28, 29, 30, 31,32], 10, true);
		this.sprite.animations.add('up', [22, 23, 24, 25,26,27], 10, true);
        this.sprite.animations.add('down', [33, 34, 35, 36,37,38], 10, true);
		this.sprite.animations.add('hit_down', [12, 13, 14, 15,16], 10, true);
        this.sprite.animations.add('hit_up', [4,5, 6, 7], 10, true);
         this.sprite.animations.add('hit_right', [8,9, 10, 11], 10, true);
         this.sprite.animations.add('hit_left', [0,1, 2, 3], 10, true);
        
        this.sprite.enableBody = true;
        game.physics.enable(this.sprite, Phaser.Physics.ARCADE);
        this.sprite.body.immovable = true;
                  

        
    
        this.sprite.body.collideWorldBounds = true;
		

       this.sprite.body.setSize(this.sprite.body.width,
                               this.sprite.body.halfHeight/3,
                              0,
                              this.sprite.body.halfHeight+2*this.sprite.body.halfHeight/3);
  this.message_audio = game.add.audio('message');

        


	} 
    
    update(){
        
    }
    
    hideIsTrigered(){


		var distance = Phaser.Math.distance(this.sprite.x, this.sprite.y, this.raum.spieler.x, this.raum.spieler.y);
		var maxtime = 0.2 * (distance / 60) * 1000;

        if(this.raum.spieler.spawnPos=="up"||this.raum.spieler.spawnPos=="down"){
              if(this.sprite.x>this.raum.spieler.x){
                  this.sprite.animations.play('left');
            }
             else{
              this.sprite.animations.play('right');
        }
      
        }
        else{
             if(this.sprite.y>this.raum.spieler.y){
                this.sprite.animations.play('up');
        }
        else{
                this.sprite.animations.play('down');
        } 
        }
        
      
      
       
        
        if(this.raum.spieler.spawnPos==="down"){
             game.physics.arcade.moveToXY(this.sprite, this.raum.spieler.x,  this.raum.spieler.y-48, 60, maxtime);
        }
        else if(this.raum.spieler.spawnPos=="up"){
             game.physics.arcade.moveToXY(this.sprite, this.raum.spieler.x,  this.raum.spieler.y+48, 60, maxtime);
        }
        else if(this.raum.spieler.spawnPos=="right"){
             game.physics.arcade.moveToXY(this.sprite, this.raum.spieler.x-100,  this.raum.spieler.y, 60, maxtime);
        }
        else {
             game.physics.arcade.moveToXY(this.sprite, this.raum.spieler.x+100,  this.raum.spieler.y, 60, maxtime);
        }
       
  
	
        game.time.events.add(maxtime, this.stopMovement, this);



	
    }
    
    trigger(){

        
			this.message_audio.play('', 0, 0.3, false, true);
			

			game.time.events.add(Phaser.Timer.SECOND * 1, this.hideIsTrigered, this);
			this.raum.spieler.isMovable = false;
        
    }
    
    	stopMovement() {
        this.sprite.animations.stop();
		this.sprite.body.immovable = true;
		this.sprite.body.velocity.x = 0;
		this.sprite.body.velocity.y = 0;
         this.sprite.frame=this.default_frame;
          

			this.ctr.displayQuestion(this.sprite.x,this.sprite.y);
	
	}
    
    

    
    


} 
