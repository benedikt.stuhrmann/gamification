class RedBtn extends Aufgabe {

	constructor(raum) {
		super(raum)

		this.red_btn;
	}

	preload() {
		game.load.image('red_btn', 'assets/aufgaben/red_btn/red_button.png');
	}

	create() {
		// red button setzten
		this.red_btn = game.add.sprite((game.width + this.raum.width_offset) / 2, game.height / 4, 'red_btn');
		this.red_btn.anchor.set(0.5);
		this.raum.aufgabenLayer.add(this.red_btn);

		this.spieler = this.raum.spieler;

		// füge spieler aktion hinzu
		this.spieler.addAction(this.yell, this);

		dashboard.setPoints(this.raum.thema,"RedButton");


	}

	update() {
		// red button trigger
		game.physics.arcade.overlap(this.spieler, this.red_btn, () => {


			if (!this.raum.isSolved) {
				// lets the earthquake begin



				this.raum.quake.start();
				dashboard.updatePoints(20);
                 dashboard.updateRoom();



				this.raum.openDoors();

			}


		});

		this.red_btn.body.velocity.y = this.red_btn.body.velocity.y * 0.99;

	}

	// test funktion zum schreien
	yell() {
		console.log('ACKTION!!');


		this.red_btn.body.velocity.y = 40;

		this.raum.closeDoor("up");
		this.raum.isSolved = !this.raum.isSolved;
	}


}
