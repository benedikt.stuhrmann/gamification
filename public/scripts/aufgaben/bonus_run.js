class Bonus_Run extends Aufgabe{

	constructor(thema, schwierigkeit){
		super();
		this.questions;
		this.qRooms = [];
		this.correctAnswers;
		this.trigger;
		this.timeLeft;
		this.npc;
		this.convoStatus;
		this.chest;
		this.key;
		this.thema = thema;
		this.schwierigkeit = schwierigkeit;
		this.closedDoor = false;
		this.entered = false;
		this.timePerQuestion = 12000;
		this.teleporter = null;
		this.percentPerAnswer = 10;
		this.waiting = false;
	}

	init(params){
		if(params == null){
			this.raum.params = {
				correctAnswers : -1,
				timeLeft : 20000,
				from : 0,
				npcConvoStatus : 0,
				done : false
			};
		}
		this.timeLeft = this.raum.params.timeLeft;
		this.correctAnswers = this.raum.params.correctAnswers;
		this.convoStatus = this.raum.params.npcConvoStatus;
		this.raum.isSolved = this.raum.params.done;
	}

	preload() {
		game.load.json('npcConvo', 'assets/bonusRun_npc.json');
		game.load.json('fragen', 'fragen/singleChoice/' + this.thema + '/' + this.schwierigkeit + '.json');

		game.load.spritesheet('npc_spritesheet', 'assets/skeleton_npc.png', 128, 128);
		game.load.spritesheet('chest', 'assets/truhe.png', 128, 128);
		game.load.spritesheet('teleporter', 'assets/room/pressure_plate_home.png', 70, 70);
	}

	create() {
		this.key = this.raum.key;

		//Aufgaben laden
		this.allQuestions = game.cache.getJSON('fragen');
		if(this.schwierigkeit == 1) this.questions = new Array(5);
		else if(this.schwierigkeit == 2) this.questions = new Array(7);
		else if(this.schwierigkeit == 3) this.questions = new Array(10);
		this.percentPerAnswer = Math.round((100/this.questions.length));
		this.raum.params.timeLeft = this.timePerQuestion*this.questions.length;
		for(var i=0; i<this.questions.length; i++){
			let q = game.rnd.pick(this.allQuestions); //zufällig eine Frage auswählen

			//Prüfen ob diese Frage bereits ausgwählt wurde (keine doppelten erlauben) (NUR wenn es mehr Fragen gibt die definiert wurden, als es Röume geben soll)
			if(this.allQuestions.length >= this.questions.length){
				var unique = false;
				while(!unique){

					for (var k=0; k<this.questions.length; k++) {
						if(this.questions[k] != null && this.questions[k].questionText == q.question){ //wenn bereits vorhanden...
							unique = false;
							q = game.rnd.pick(this.allQuestions); //...zufällig eine neue Frage auswählen
							break;
						}
						else if(k == this.questions.length-1) //wenn letze auch nicht = der Frage ist, ist diese noch nicht vorhanden
							unique = true;
					}
				}
			}

			let corrAnsw = game.rnd.pick(q.answers); //zufällig eine der richtige Antworten auswählen
			let choices = new Array(3); //Array für die 3 Möglichen Antworten
			choices[0] = corrAnsw; //richtige Antwort als erstes Element des Arrays

			var choicesKeys = Object.keys(q.choices);
			for(var j=1; j<choices.length; j++){
				unique = false;
				while(!unique){
					let choice = game.rnd.pick(choicesKeys); //zufällige falsche Antwort
					if(choices.indexOf(choice) == -1){ //wenn noch nicht vorhanden
						unique = true;
						choices[j] = choice;
					}
				}
			}
			this.questions[i] = new Question(q.question, q.choices[choices[0]], q.choices[choices[1]], q.choices[choices[2]]);
		}

		if(this.correctAnswers == -1 && !this.raum.isSolved)
			this.createRooms();

		//NPC erstellen
		this.npc = new NPC(((game.width+this.raum.width_offset)/2) + 150, 130, this.raum);
		this.npc.setGraphic("npc_spritesheet");
		this.npc.convo = game.cache.getJSON('npcConvo');

		//Truhe erstellen
		this.chest = new Chest(((game.width+this.raum.width_offset)/2) - 150, 130, false, this.raum);

		if(this.correctAnswers != -1){
			//Spieler betritt Raum, nach spielen des BonusRun
			this.npc.graphic.animations.play('wave');

			if(this.correctAnswers == 0){
				//Spieler hat keine Frage richtig beantwortet
				this.npc.convoJumpTo("success_failed");
      }
			if(this.correctAnswers > 0 && this.correctAnswers < this.questions.length){
				//Spieler hat 1 bis n-1 Fragen richtig beantwortet
				this.npc.convoJumpTo("success_okay");
      }
			if(this.correctAnswers == this.questions.length){
				//Spieler hat alle Fragen richtig beantwortet
				this.npc.convoJumpTo("success_good");
      }
		}
		else{
			//Spieler betritt Raum, ohne BonusRun bereits gespielt zu haben
		}

		if(this.correctAnswers != -1 && !this.raum.isSolved){
			//Spieler kommt aus Frageräumen.
			if(this.correctAnswers == this.questions.length)
				this.raum.up = this.qRooms[this.qRooms.length-1].name;
			else
				this.raum.up = 'Sackgasse';

			this.raum.spieler.destroy();
			this.raum.spieler = new Spieler(this.raum, ((game.width+this.raum.width_offset)/2), 0);
		}
		else if(this.raum.isSolved){
			this.npc.convoJumpTo("end");
		}

		this.entering = false;
    dashboard.setPoints(this.raum.thema,"BonusRun");

		let count = Math.floor(Math.random() * 7) + 1;
    this.raum.deco.setRockRect(0, 140, 140, 280, 576-140);
		this.raum.deco.setRockRect(1, 280, 240, 720, 576-140);
		this.raum.deco.setRockRect(2, 720, 140, 1024-140, 576-140);
    this.raum.deco.placeObjects(count, 'rocks');
    count = Math.floor(Math.random() * 4) + 1;
		this.raum.deco.setRockRect(0, 140, 140, 280, 576-140);
		this.raum.deco.setRockRect(1, 280, 220, 720, 576-140);
		this.raum.deco.setRockRect(2, 720, 140, 1024-140, 576-140);
    this.raum.deco.placeObjects(4, 'plant');
    count = Math.floor(Math.random() * 2) + 1;
		this.raum.deco.setRockRect(0, 140, 140, 280, 576-140);
		this.raum.deco.setRockRect(1, 280, 220, 720, 576-140);
		this.raum.deco.setRockRect(2, 720, 140, 1024-140, 576-140);
    this.raum.deco.placeObjects(1, 'puddle');

		this.closedDoor = false;

		this.raum.spieler.grabRange = 120;
		this.raum.spieler.addAction(this.action, this);
	}

	update() {
		if(this.npc.graphic.animations.currentAnim.name == 'talk' && this.npc.graphic.animations.currentAnim.loopCount == 5)
			this.npc.graphic.animations.stop('talk');
		if(this.npc.graphic.animations.currentAnim.name == 'wave' && this.npc.graphic.animations.currentAnim.isFinished && !this.waiting){
			this.npc.graphic.animations.stop('wave');
			this.npc.graphic.frame = 0;
			this.waiting = true;
			game.time.events.add(Phaser.Timer.SECOND * 2, function(){ this.waiting = false; if(!this.npc.convoEnded && this.raum.spieler.isMovable) this.npc.graphic.animations.play('wave'); }, this);
		}

		if(this.teleporter != null) this.teleporter.listen('Hub', null, null, this.teleporter);
		else if(this.raum.isSolved)
			this.teleporter = new Pressure_Plate(((game.width+this.raum.width_offset)/2)-300, (game.height/2)+80, 'teleporter', this.raum);

    if(this.raum.isSolved&&!dashboard.isFinished){
        this.raum.checkHighscore();
    }


		if(this.correctAnswers != -1 && !this.raum.isSolved && (this.raum.connections.up == this.qRooms[this.qRooms.length-1].name || this.raum.connections.up == 'Sackgasse')){
			if(this.raum.spieler.y < 120 && !this.entered){
				this.raum.spieler.y += 3;
			}
			else{
				this.entered = true;
				if(!this.closedDoor){
					this.raum.closeDoor("up");
					this.closedDoor = true;
				}
			}
		}

		if(this.raum.isSolved)
			this.raum.closeDoor("up");

		game.physics.arcade.overlap(this.raum.spieler, this.trigger);
		game.physics.arcade.collide(this.raum.spieler, this.chest.chest);
		game.physics.arcade.collide(this.raum.spieler, this.npc.graphic);

		// steuerung aufsetzten
		let up = this.raum.cursors.up.justDown || this.raum.wasd.w.justDown;
		let down = this.raum.cursors.down.justDown || this.raum.wasd.s.justDown;
		let left = this.raum.cursors.left.justDown || this.raum.wasd.a.justDown;
		let right = this.raum.cursors.right.justDown || this.raum.wasd.d.justDown;

		if(up) 		this.npc.updateSelection("y-");
		if(left) 	this.npc.updateSelection("x-");
		if(down)	this.npc.updateSelection("y+");
		if(right) this.npc.updateSelection("x+");

		if(this.npc.convoFinsihed() && this.correctAnswers == -1){

			this.raum.params.correctAnswers = this.correctAnswers = 0;
			this.raum.openDoor("up", false);
		}

		if(this.chest.opened){
			if(!this.raum.isSolved){
				this.raum.openDoor("right", true);
				this.raum.openDoor("down", true);
				this.raum.openDoor("left", true);
				this.raum.quake.start();
			}
			this.raum.params.done = this.raum.isSolved;
		}
	}

	action(){



		//NPC Interaktionen
		if(game.physics.arcade.distanceBetween(this.raum.spieler, this.npc) < 70 && !this.npc.convoEnded){
			this.npc.togglePauseConvo(false);
			this.npc.updateConvo();
		}

		//Interaktion mit Truhe
		if(game.physics.arcade.distanceBetween(this.raum.spieler, this.chest) < 70){
      if(this.chest.unlocked && !this.chest.opened){
				this.chest.open();
				let style = {
		      font: "25px Verdana",
		      fill: "#ffffff",
		      align: "center",
		      wordWrapWidth: 300,
		    }

            dashboard.setMultiplier(this.raum.thema,this.correctAnswers*this.percentPerAnswer,"isGlobal");
		  //  dashboard.updateRoom();


		    this.popup = game.add.text((game.width+this.raum.width_offset)/2, (game.height/2)-50, "Deine Belohnung!\n"+this.reward, style);
        this.popup.anchor.set(0.5);
		    this.popup.alpha = 0;

        game.add.tween(this.popup).to({alpha: 1}, 800, null, true, 0, 0, false);
		    game.add.tween(this.popup).to({y: (game.height/2)}, 1000, null, true, 0, 0, false);
		    game.time.events.add(Phaser.Timer.SECOND * 4, this.hideText, this);
        this.raum.checkHighscore();
      }
    }

		if(this.chest.opened){
			if(!this.raum.isSolved){
				this.raum.openDoor("right", true);
				this.raum.openDoor("down", true);
				this.raum.openDoor("left", true);
				this.raum.quake.start();
				//this.raum.checkHighscore();
                //dashboard.checkHighscore(this.thema,this.raum.spieler);
			}
			this.raum.params.done = this.raum.isSolved;
		}
	}

	entranceID(answerID){
		switch(answerID){
			case 0: return 2;
				break;
			case 1: return 3;
				break;
			case 2: return 0;
				break;
			case 3: return 1;
				break;
			default: return -1;
		}
	}

	npcCallback(element){
		//(this.convoStatus == 1) ? 2 : 1;
		//this.raum.params.npcConvoStatus = this.convoStatus;
		if(element.callback == "pause")
			this.npc.togglePauseConvo(true);
		if(element.callback == "success_failed"){
			this.raum.openDoor("right", true);
			this.raum.openDoor("down", true);
			this.raum.openDoor("left", true);
			this.raum.quake.start();
			this.raum.params.done = this.raum.isSolved;

		}
		if(element.callback == "success_okay"){
			this.reward = (this.correctAnswers*10) + "% Punktemultiplikator";
      dashboard.updatePoints(10);
			this.chest.toggleUnlock(true);
		}
		if(element.callback == "success_good"){
			this.reward = (this.correctAnswers*10) + "% Punktemultiplikator";
      dashboard.updatePoints(20);
			this.chest.toggleUnlock(true);

		}
	}

	hideText(){
    game.add.tween(this.popup).to({alpha: (0)}, 2000, null, true, 0, 0, false);
    //game.add.tween(this.popup).to({y: (game.height/2)+50}, 2000, null, true, 0, 0, false);
  }

	createRooms(){
		//Räume erstellen
		for(var i=0; i<this.questions.length; i++){
			let room = new Raum(new SingleChoice_BR(this.key, this.questions[i]));
			let stateName = (i+1) + ". Frageraum";

			this.qRooms[i] = { name: stateName, room: room };

			game.state.add(this.qRooms[i].name, this.qRooms[i].room);
		}

		//Verbindungen unter den Räumen festlegen/mitgeben
		for(var i=0; i<this.qRooms.length; i++){
			if(i+1 < this.qRooms.length) //wenn weiterer Raum vorhanden
				this.qRooms[i].room.aufgabe.setNextRoom(this.qRooms[i+1].name); //dem Raum sagen, wer sein Nachfolger ist
			if(i-1 >= 0) //wenn voriger Raum vorhanden
				this.qRooms[i].room.aufgabe.setPrevRoom(this.qRooms[i-1].name); //dem Raum sagen, wer sein Vorgänger ist
		}

		this.raum.up = this.qRooms[0].name; //Erster Frageraum
	}

	blockMe(raum) {
		raum.connections.up = 'blocked';
	}

}
