class GUI_Evaluator{

  constructor(guiBau, task, pieces, baseElements, guiElements, guiZoneGraphic){
    this.task = task;
    this.pieces = pieces;
    this.baseElements = baseElements;
    this.guiElements = guiElements;
    this.unusedGuiElements = [];
    this.guiZoneGraphic = guiZoneGraphic;
    this.guiZone;
    this.guiZoneGroups;
    this.groups = [];
    this.guiBau = guiBau;

    this.score = 0;
    this.result = 0;

    this.constraints = 0;
    this.positives = 0;
  }//end constructor

  evaluate(){
    this.score = 0;
    this.prepareEvaluation();
    this.checkGroups();
    this.checkPadding();
    this.score -= this.unusedGuiElements.length*50;

    this.constraints += this.guiElements.length;
    // console.log('Alle ' + this.guiElements.length + ' Elemente müssen benutzt werden');
    this.positives += this.guiElements.length - this.unusedGuiElements.length;
    // console.log('davon wurden ' + (this.guiElements.length - this.unusedGuiElements.length) + ' benutzt');

    this.wronglyUsedElements = 0;
    let notUsed = 0;
    for(var i=0; i<this.guiBau.guiChest.guiElementsInChest.length; i++){ //alle Elemente in der Truhe durchgehen
      for(var j=0; j<this.guiBau.additionalElements.length; j++) //checken ob Element in der Truhe ein zusätzliches Element ist
        if(this.guiBau.guiChest.guiElementsInChest[i] === this.guiBau.additionalElements[j]) notUsed += 1;
    }
    this.constraints += this.guiBau.additionalElements.length;
    // console.log(this.guiBau.additionalElements.length + ' zusätzliche Elemente welche nicht benutzt werden sollen');

    this.wronglyUsedElements = this.guiBau.additionalElements.length - notUsed;
    this.positives += this.guiBau.additionalElements.length - this.wronglyUsedElements;

    // console.log(this.guiBau.additionalElements.length - this.wronglyUsedElements + ' davon wurden nicht benutzt');

    this.score -= this.wronglyUsedElements*50;

    // console.log('Constraints:   ' + this.constraints);
    // console.log('Davon erfüllt: ' + this.positives);

    this.result = (100/this.constraints)*this.positives;

    if(this.score < 0) this.score = 0;
    return this.score;
  }//end evaluate

  prepareEvaluation(){
    //Gruppen zu GUI Elementen hinzufügen
    this.groupsForElements(this.task, [{type:"group", name: "root"}]);

    //2dim Array für GUI Elemente
    this.guiZone = new Array(30);
    for (var i=0; i<this.guiZone.length; i++)
      this.guiZone[i] = new Array(16);

    for (var x=0; x<this.guiZone.length; x++) {
      for (var y=0; y<this.guiZone[x].length; y++) {
        var elem = this.tileHoldsGuiElement(x,y);
        if(elem != null)
          this.guiZone[x][y] = elem.item;
        else
          this.guiZone[x][y] = null;
      }
    }

    //2dim Array für Gruppen
    this.guiZoneGroups = new Array(30);
    for (var i=0; i<this.guiZoneGroups.length; i++)
      this.guiZoneGroups[i] = new Array(16);
  }//end prepareEvaluation

  tileHoldsGuiElement(x, y){
    var holds = null;
    let tileCoords = this.getTileCoords(x, y);
    this.pieces.forEach((piece) => {
      if( (tileCoords.x >= piece.left + 8 && tileCoords.x <= piece.right - 8) &&
          (tileCoords.y >= piece.top + 8 && tileCoords.y <= piece.bottom - 8) )
        holds = piece;
    });
    return holds;
  }//end tileHoldsGuiElement

  getTileCoords(x, y){
    let firstTileX = this.guiZoneGraphic.x - (this.guiZoneGraphic.width/2) + 10 + 5;
    let firstTileY = this.guiZoneGraphic.y - (this.guiZoneGraphic.height/2) + 10 + 5;
    return new Phaser.Point(firstTileX + 20*x, firstTileY + 20*y);
  }//end getTileCoords

  checkGroups(){
    this.guiElements.forEach((guiElement) => {
      this.constraints ++;
      // console.log(guiElement.title + ' muss neben einem Gruppenmitglied liegen!');
      if(this.besidesGroupMember(guiElement)){ //Gui Elemente testen
        this.positives ++;
        this.score += 50;
      }
    });

    this.groupCheck(); //Gruppen selbste testen
  }//end checkGroups

  groupsForElements(module, groups){
    if(module.assignment == null) groups.push(module);
    for(var i=0; i<module.elements.length; i++){
      let keys = Object.keys(module.elements[i]);
      keys.forEach((key) => {
        if(key == "type" && module.elements[i].type != "group"){
          this.addGroupsToGuiElement(module.elements[i], groups);
        }
        else if(key == "type" && module.elements[i].type == "group"){
          let myGroups = Object.assign([], groups);
          this.groupsForElements(module.elements[i], myGroups);
        }
      });
    }
  }//end groupsForElements

  addGroupsToGuiElement(guiElement, groups){
    var added = false;
    for (var i=0; i<this.guiElements.length; i++) {
      //entsprechendes GUI Element suchen:
      let keys = Object.keys(guiElement);
      for (var j=0; j<keys.length; j++) {
        if(this.guiElements[i][keys[j]] != guiElement[keys[j]])
          break;
        else if(j == keys.length-1){
          //wenn auch der letze Key gleich war
          let myGroups = Object.assign([], groups);
          this.guiElements[i].groups = myGroups;
          added = true;
        }
      }
      if(added) break;
    }
  }//end addGroupsToGuiElement

  besidesGroupMember(guiElement){
    let elementsToCheck = this.intoGuiElements(this.task.elements);
    let groupMembers = this.getGroupMembers(guiElement, elementsToCheck);

    if(groupMembers != null && groupMembers.length > 1) return this.checkThisElement(guiElement, groupMembers);
    else return true;
  }//end besidesGroupMember

  intoGuiElements(elementsToCheck){
    let transformedElements = [];

    for(var i=0; i<elementsToCheck.length; i++){
      //für alle zu prüfenden Elemente
      for(var j=0; j<this.baseElements.length; j++){
        // gehe Basiselemente durch
        if(this.baseElements[j].type == elementsToCheck[i].type){
          //Erzeuge GUI Element aus Kombination von Basis-Element und Informationen aus Aufgabe
          let guiElement = Object.assign({}, this.baseElements[j]);
          guiElement = Object.assign(guiElement, elementsToCheck[i]);
          transformedElements.push(guiElement);
          break;
        }
      }
    }
    return transformedElements;
  }//end intoGuiElements

  checkThisElement(guiElement, groupMembers){
    // Position des Pieces im !Grid! (obere linke Ecke)
    let elementPos = this.elementPos(guiElement);
    let x = elementPos.x;
    let y = elementPos.y;

    //Abbrechen, da Element nicht auf dem Spielfeld
    if(x == -1 && y == -1) return false;
    //True, wenn keine weiteren Gruppenmitglieder
    if(groupMembers.length == 1) return true;

    //alle Felder des Elements abgehen und jeweils testen
    while(x < this.guiZone.length && y < this.guiZone[x].length && this.guiZone[x][y] == guiElement){
      //Reihe durchgehen
      while(this.guiZone[x][y] == guiElement){
        if(this.doTileTests(x, y, guiElement, groupMembers)){
          return true; //anliegendes Gruppenmitglied gefunden
        }
        x++;
      }
      x = elementPos.x; //x auf Anfang
      y++; //nächste Reihe
    }

    return false;
  }//end checkThisElement

  getGroupMembers(guiElement, elementsToCheck){
    if(this.isHere(guiElement, elementsToCheck))
      return elementsToCheck;

    for(var i=0; i<elementsToCheck.length; i++){
      if(elementsToCheck[i].type == 'group'){
        this.addToArray(elementsToCheck[i], this.groups);
        var groupMembers = this.getGroupMembers(guiElement, this.intoGuiElements(elementsToCheck[i].elements));
        if(groupMembers != null)
          return groupMembers;
      }
    }
    return null;
  }//end getGroupMembers

  isHere(guiElement, elementsToCheck){
    for(var i=0; i<elementsToCheck.length; i++){

      let keys = Object.keys(elementsToCheck[i]);
      for (var j=0; j<keys.length; j++) {
        if(keys[j] != "groups" && elementsToCheck[i][keys[j]] != guiElement[keys[j]])
          break;
        else if(j == keys.length-1){
          //wenn auch der letze Key gleich war
          return true;
        }
      }
    }
    return false;
  }//end isHere

  addToArray(element, array){
    for(var i=0; i<array.length; i++){
      if(element.type != "group"){
        if(this.equals(array[i], element))//JSON.stringify(array[i]) === JSON.stringify(element))
        return;
      }
      else{
        if(element.name == array[i].name)
        return;
      }
    }
    array.push(element);
  }//end addToArray

  elementPos(guiElement){
    if(guiElement.type != "group"){
      for (var x=0; x<this.guiZone.length; x++) {
        for (var y=0; y<this.guiZone[x].length; y++) {
          if(this.guiZone[x][y] == guiElement){
            //Feld enthält das gesuchte Element
            return new Phaser.Point(x,y);
          }
        }
      }
      this.addToArray(guiElement, this.unusedGuiElements);
    }
    else{
      for (var x=0; x<this.guiZoneGroups.length; x++) {
        for (var y=0; y<this.guiZoneGroups[x].length; y++) {
          if(this.guiZoneGroups[x][y] == guiElement.name){
            //Feld enthält das gesuchte Element
            return new Phaser.Point(x,y);
          }
        }
      }
    }

    return new Phaser.Point(-1,-1); //Element befindet sich nicht auf dem Spielfeld
  }//end piecePos

  doTileTests(x, y, guiElement, groupMembers){
    var besidesPartner = false;

    //nach oben testen, ob Gruppenmitglied anliegt
    if(!besidesPartner) besidesPartner = this.testDirection(x, y, 0, -1, guiElement, groupMembers);
    //nach rechts testen, ob Gruppenmitglied anliegt
    if(!besidesPartner) besidesPartner = this.testDirection(x, y, 1, 0, guiElement, groupMembers);
    //nach unten testen, ob Gruppenmitglied anliegt
    if(!besidesPartner) besidesPartner = this.testDirection(x, y, 0, 1, guiElement, groupMembers);
    //nach links testen, ob Gruppenmitglied anliegt
    if(!besidesPartner) besidesPartner = this.testDirection(x, y, -1, 0, guiElement, groupMembers);

    return besidesPartner;
  }//end doTileTests

  testDirection(x, y, xOff, yOff, guiElement, groupMembers){
    var offGrid = false;

    //Es handelt sich um ein GUI-Element
    if(guiElement.type != "group"){
      //solange weitergehen, bis ein Element gefunden wird
      do{
        x += xOff;  y += yOff; //x, y erhöhen
        //noch auf Spielfeld?
        if(!(y>=0 && x>=0 && x<this.guiZone.length-1 && y<this.guiZone[0].length-1)) offGrid = true;
      }while(!offGrid && this.guiZone[x][y] == null)

      //ein Element wurde gefunden
      if(!offGrid){
        for(var i=0; i<groupMembers.length; i++){
          //alle Gruppenmitglieder durchgehen, und checken ob eines davon
          if(!this.equals(this.guiZone[x][y], guiElement) //nicht sich selbst finden
           && this.equals(this.guiZone[x][y], groupMembers[i]) ) //anderes Gruppenmitglied
            return true;
        }
      }
      return false;
    }

    //Es handelt sich um ein Gruppen-Element
    else{
      //solange weitergehen, bis ein Element gefunden wird
      do{
        x += xOff;  y += yOff; //x, y erhöhen
        //noch auf Spielfeld?
        if(!(y>=0 && x>=0 && x<this.guiZoneGroups.length-1 && y<this.guiZoneGroups[0].length-1)) offGrid = true;
      }while(!offGrid && this.guiZoneGroups[x][y] == null)

      //ein Element wurde gefunden
      if(!offGrid){
        if(this.guiZoneGroups[x][y] != guiElement.name)
            return true;
      }
      return false;
    }
  }//end testDirection

  equals(object1, object2){
    let keys = Object.keys(object1);
    for (var i=0; i<keys.length; i++) {
      if(keys[i] != "groups" && object1[keys[i]] != object2[keys[i]])
        break;
      else if(i == keys.length-1){
        //wenn auch der letze Key gleich war
        return true;
      }
    }
    return false;
  }//end equals

  groupCheck(){
    //alle Gruppen testen
    for(var i=0; i<this.groups.length; i++){
      var groupsOnSameLevel = this.getGroupMembers(this.groups[i], this.task.elements); //alle Gruppen der Ebene der zu testenden Gruppe


      //alle Gruppen der Ebene eintragen
      for (var j=0; j<groupsOnSameLevel.length; j++) {
        let dimensions = { //Eckdaten der zu testenden Gruppe
          top: null,
          right: null,
          bottom: null,
          left: null
        };

        for (var x=0; x<this.guiZoneGroups.length; x++) {
          for (var y=0; y<this.guiZoneGroups[x].length; y++) {
              let index = -1;
              if(this.guiZone[x][y] != null){
                for (var k=0; k<this.guiZone[x][y].groups.length; k++) {
                  if(this.guiZone[x][y].groups[k].name == groupsOnSameLevel[j].name){
                    index = k;
                    break;
                  }
                }
              }
              //index = this.guiZone[x][y].groups.indexOf(groupsOnSameLevel[j]);
              if(index != -1){
                //wenn das Feld ein GUI Element beherbergt, welches Teil der gerade betrachteten Gruppe ist
                this.guiZoneGroups[x][y] = this.guiZone[x][y].groups[index].name;
                dimensions = this.updateDimensions(dimensions, x, y);
              }
          }//end y
        }//end x

        for (var f=0; f<this.groups.length; f++) {
          if(this.groups[f].name == groupsOnSameLevel[j].name){
            this.groups[f].dimensions = dimensions;
            this.groups[f].inTest = true;
            break;
          }
        }
      }//alle Gruppen der Ebene

      this.fillGuiZoneGroups();
      //guiZoneGroups enthät jetzt alle Gruppen einer "Ebene" ---> testen ob nebeneinander!
      this.constraints ++;
      // console.log(this.groups[i].name + ' muss neben einer Gruppe der selben Ebene liegen!');
      if(this.checkThisGroup(this.groups[i], this.guiZoneGroups)){
        this.score += 50;
        this.positives ++;
      }

      this.groups.forEach((group) => {
        group.inTest = false;
      });

    }//end alle Gruppen
  }//end groupCheck

  updateDimensions(dimensions, x, y){
    if(dimensions.top == null    || y < dimensions.top)    dimensions.top = y;
    if(dimensions.right == null  || x > dimensions.right)  dimensions.right = x;
    if(dimensions.bottom == null || y > dimensions.bottom) dimensions.bottom = y;
    if(dimensions.left == null   || x < dimensions.left)   dimensions.left = x;
    return dimensions;
  }//end updateDimensions

  checkThisGroup(group, groupsOnSameLevel){
    //Gruppe(bzw. Elemente dieser) nicht platziert
    if(!group.inTest || group.dimensions == null || group.dimensions.top == null || group.dimensions.right == null || group.dimensions.bottom == null || group.dimensions.left == null) return false;

    // Position der Gruppe im !Grid! (obere linke Ecke)
    let x = group.dimensions.left;
    let y = group.dimensions.top;

    //Abbrechen, da Element nicht auf dem Spielfeld
    if(x == -1 || y == -1) return false;

    //alle Felder des Elements abgehen und jeweils testen
    while(this.guiZoneGroups[x][y] == group.name){
      //Reihe durchgehen
      while(this.guiZoneGroups[x][y] == group.name){
        if(this.doTileTests(x, y, group, groupsOnSameLevel)){
          return true; //anliegendes Gruppenmitglied gefunden
        }
        x++;
      }
      x = group.dimensions.left; //x auf Anfang
      y++; //y auf nächste Reihe
    }

    return false;
  }//end checkThisGroup

  fillGuiZoneGroups(){
    for (var x=0; x<this.guiZoneGroups.length; x++) {
      for (var y=0; y<this.guiZoneGroups[x].length; y++) {
        this.guiZoneGroups[x][y] = null;
      }
    }
    this.groups.forEach((group) => {
      if(group.inTest != null && group.inTest){
        for (var x=0; x<this.guiZoneGroups.length; x++) {
          for (var y=0; y<this.guiZoneGroups[x].length; y++) {
            if(x>=group.dimensions.left && x<=group.dimensions.right && y>=group.dimensions.top && y<=group.dimensions.bottom)
              this.guiZoneGroups[x][y] = group.name;
          }
        }
      }
    });
  }//end fillGuiZoneGroups

  checkPadding(){
    var guiElementsAndGroups = [];
    for(var i=0; i<this.guiElements.length; i++)
      guiElementsAndGroups.push(this.guiElements[i]);
    for(var i=0; i<this.groups.length; i++)
      guiElementsAndGroups.push(this.groups[i]);

    guiElementsAndGroups.forEach((guiElement) => {
      //Abstand des GUI Elements zum nächsten Element finden
      let topPadding    = this.paddingTop(guiElement);
      let rightPadding  = this.paddingRight(guiElement);
      let bottomPadding = this.paddingBottom(guiElement);
      let leftPadding   = this.paddingLeft(guiElement);

      this.constraints += 4;
      // console.log(guiElement.type + " muss alle 4 Paddings einhalten");

      if(topPadding != 'error' && rightPadding != 'error' && bottomPadding != 'error' && leftPadding != 'error'){
        // nur wenn Element auf Spielfeld platziert, sonst überspringen
        if((topPadding >= parseInt(guiElement.padding.top.split(',')[0])
         && topPadding <= parseInt(guiElement.padding.top.substr(guiElement.padding.top.indexOf(',')+1)))
         || topPadding < 0){
          this.score += 15;
          this.positives++;
        }
        if((rightPadding >= parseInt(guiElement.padding.right.split(',')[0])
         && rightPadding <= parseInt(guiElement.padding.right.substr(guiElement.padding.right.indexOf(',')+1)))
         || rightPadding < 0){
          this.score += 15;
          this.positives++;
        }
        if((bottomPadding >= parseInt(guiElement.padding.bottom.split(',')[0])
         && bottomPadding <= parseInt(guiElement.padding.bottom.substr(guiElement.padding.bottom.indexOf(',')+1)))
         || bottomPadding < 0){
          this.score += 15;
          this.positives++;
        }
        if((leftPadding >= parseInt(guiElement.padding.left.split(',')[0])
         && leftPadding <= parseInt(guiElement.padding.left.substr(guiElement.padding.left.indexOf(',')+1)))
         || leftPadding < 0){
          this.score += 15;
          this.positives++;
        }
      }

      if(topPadding < 0) topPadding = -topPadding;
      if(rightPadding < 0) rightPadding = -rightPadding;
      if(bottomPadding < 0) bottomPadding = -bottomPadding;
      if(leftPadding < 0) leftPadding = -leftPadding;
      this.besidesCheckGuiElement(guiElement, topPadding, rightPadding, bottomPadding, leftPadding);

    });
  }//end checkPadding

  elementWidth(guiElement){
    var width = 0;
    if(guiElement.type != "group"){
      let elementPos = this.elementPos(guiElement);
      if(elementPos.x == -1 || elementPos.y == -1) return 0;
      let x = elementPos.x;

      //alle Felder des Elements abgehen und jeweils testen
      while(x>=0 && x<=this.guiZone.length && this.guiZone[x][elementPos.y] == guiElement){
        width++;
        x++;
      }
    }
    else{
      let elementPos = new Phaser.Point(guiElement.dimensions.left, guiElement.dimensions.top);
      if(elementPos.x == -1 || elementPos.y == -1) return 0;
      width = guiElement.dimensions.right - guiElement.dimensions.left + 1;
    }

    return width;
  }//end elementWidth

  elementHeight(guiElement){
    var height = 0;

    if(guiElement.type != "group"){
      let elementPos = this.elementPos(guiElement);
      if(elementPos.x == -1 || elementPos.y == -1) return 0;
      let y = elementPos.y;

      //alle Felder des Elements abgehen und jeweils testen
      while(y>=0 && y<=this.guiZone[0].length && this.guiZone[elementPos.x][y] == guiElement){
        height++;
        y++;
      }
    }
    else{
      let elementPos = new Phaser.Point(guiElement.dimensions.left, guiElement.dimensions.top);
      if(elementPos.x == -1 || elementPos.y == -1) return 0;
      height = guiElement.dimensions.bottom - guiElement.dimensions.top + 1;
    }

    return height;
  }//end elementHeight

  paddingTop(guiElement){
    // Position des Pieces im !Grid! (obere linke Ecke)
    let elementPos;
    if(guiElement.type != "group") elementPos = this.elementPos(guiElement);
    else if(guiElement.dimensions.left != null && guiElement.dimensions.top != null)
      elementPos = new Phaser.Point(guiElement.dimensions.left, guiElement.dimensions.top);

    if(elementPos == null || elementPos.x<0 || elementPos.y<0) return 'error';
    var padding = 0;
    var width = this.elementWidth(guiElement);

    let x = elementPos.x;
    let y = elementPos.y - 1;

    //alle oberen Felder des Elements abgehen und jeweils testen
    while(y>=0 && x<this.guiZone.length && this.guiZone[x][y] == null){
      if(x < elementPos.x || x > elementPos.x+width-1){ //nicht mehr über dem Element
        y--; //nächste Zeile
        padding++; //Ganze Reihe war leer, also Padding erhöhen
        x = elementPos.x;
      }
      else
        x++; //Reihe durchgehen
    }

    if(y<0) padding = padding*(-1);

    return padding;
  }//end paddingTop

  paddingRight(guiElement){
    // Position des Pieces im !Grid! (obere linke Ecke)
    let elementPos;
    if(guiElement.type != "group") elementPos = this.elementPos(guiElement);
    else if(guiElement.dimensions.left != null && guiElement.dimensions.top != null)
      elementPos = new Phaser.Point(guiElement.dimensions.left, guiElement.dimensions.top);

    if(elementPos == null || elementPos.x<0 || elementPos.y<0) return 'error';
    var padding = 0;
    var width = this.elementWidth(guiElement);
    var height = this.elementHeight(guiElement);

    let x = elementPos.x+width;
    let y = elementPos.y;

    //alle rechten Felder des Elements abgehen und jeweils testen
    while(x<this.guiZone.length && y<this.guiZone[0].length && this.guiZone[x][y] == null){
      if(y < elementPos.y || y > elementPos.y+height-1){ //nicht mehr rechts neben dem Element
        x++; //nächste Spalte
        padding++; //Ganze Spalte war leer, also Padding erhöhen
        y = elementPos.y;
      }
      else
        y++; //Spalte durchgehen
    }

    if(x>=this.guiZone.length) padding = padding*(-1);

    return padding;
  }//end paddingRight

  paddingBottom(guiElement){
    // Position des Pieces im !Grid! (obere linke Ecke)
    let elementPos;
    if(guiElement.type != "group") elementPos = this.elementPos(guiElement);
    else if(guiElement.dimensions.left != null && guiElement.dimensions.top != null)
      elementPos = new Phaser.Point(guiElement.dimensions.left, guiElement.dimensions.top);

    if(elementPos == null || elementPos.x<0 || elementPos.y<0) return 'error';
    var padding = 0;
    var width = this.elementWidth(guiElement);
    var height = this.elementHeight(guiElement);

    let x = elementPos.x;
    let y = elementPos.y+height;

    //alle unteren Felder des Elements abgehen und jeweils testen
    while(y<this.guiZone[0].length && x<this.guiZone.length && this.guiZone[x][y] == null){
      if(x < elementPos.x || x > elementPos.x+width-1){ //nicht mehr unter dem Element
        y++; //nächste Zeile
        padding++; //Ganze Reihe war leer, also Padding erhöhen
        x = elementPos.x;
      }
      else
        x++; //Reihe durchgehen
    }

    if(y>=this.guiZone[0].length) padding = padding*(-1);

    return padding;
  }//end paddingBottom

  paddingLeft(guiElement){
    // Position des Pieces im !Grid! (obere linke Ecke)
    let elementPos;
    if(guiElement.type != "group") elementPos = this.elementPos(guiElement);
    else if(guiElement.dimensions.left != null && guiElement.dimensions.top != null)
      elementPos = new Phaser.Point(guiElement.dimensions.left, guiElement.dimensions.top);

    if(elementPos == null || elementPos.x<0 || elementPos.y<0) return 'error';
    var padding = 0;
    var height = this.elementHeight(guiElement);

    let x = elementPos.x-1;
    let y = elementPos.y;

    //alle linken Felder des Elements abgehen und jeweils testen
    while(x>=0 && y<this.guiZone[0].length && this.guiZone[x][y] == null){
      if(y < elementPos.y || y > elementPos.y+height-1){ //nicht mehr links neben dem Element
        x--; //nächste Spalte
        padding++; //Ganze Spalte war leer, also Padding erhöhen
        y = elementPos.y;
      }
      else
        y++; //Spalte durchgehen
    }

    if(x<0) padding = padding*(-1);

    return padding;
  }//end paddingLeft

  besidesCheckGuiElement(guiElement, topPadding, rightPadding, bottomPadding, leftPadding){
    let elementPos
    if(guiElement.type != "group") elementPos = this.elementPos(guiElement);
    else elementPos = new Phaser.Point(guiElement.dimensions.left, guiElement.dimensions.top);

    if(guiElement.above != null){
      this.constraints ++;
      // console.log(guiElement.type + ' muss über einem bestimmten Element liegen');
      let x1 = elementPos.x;
      let y1 = elementPos.y+this.elementHeight(guiElement)+bottomPadding;
      let x2 = x1+this.elementWidth(guiElement)-1;
      let y2 = y1;

      if( x1 >= 0 && x2 >= 0 && x1 <= this.guiZone.length-1 && x2 <= this.guiZone.length-1
       && y1 >= 0 && y2 >= 0 && y1 <= this.guiZone[0].length-1 && y2 <= this.guiZone[0].length-1)
      {
        let elems = this.elementsAt(guiElement, elementPos, x1, y1, x2, y2);
        elems.forEach((elem) => {
          if(elem.id == guiElement.above) {this.score += 20; this.positives++}
        });
      }
    }
    if(guiElement.right_of != null){
      this.constraints ++;
      // console.log(guiElement.type + ' muss rechts von einem bestimmten Element liegen');
      let x1 = elementPos.x-1-leftPadding;
      let y1 = elementPos.y;
      let x2 = x1;
      let y2 = y1+this.elementHeight(guiElement)-1;

      if( x1 >= 0 && x2 >= 0 && x1 <= this.guiZone.length-1 && x2 <= this.guiZone.length-1
       && y1 >= 0 && y2 >= 0 && y1 <= this.guiZone[0].length-1 && y2 <= this.guiZone[0].length-1)
      {
        let elems = this.elementsAt(guiElement, elementPos, x1, y1, x2, y2);
        elems.forEach((elem) => {
          if(elem.id == guiElement.right_of) {this.score += 20; this.positives++}
        });
      }
    }
    if(guiElement.under != null){
      this.constraints ++;
      // console.log(guiElement.type + ' muss unter einem bestimmten Element liegen');
      let x1 = elementPos.x;
      let y1 = elementPos.y-topPadding-1;
      let x2 = x1+this.elementWidth(guiElement)-1;
      let y2 = y1;

      if( x1 >= 0 && x2 >= 0 && x1 <= this.guiZone.length-1 && x2 <= this.guiZone.length-1
       && y1 >= 0 && y2 >= 0 && y1 <= this.guiZone[0].length-1 && y2 <= this.guiZone[0].length-1)
      {
        let elems = this.elementsAt(guiElement, elementPos, x1, y1, x2, y2);
        elems.forEach((elem) => {
          if(elem.id == guiElement.under) {this.score += 20; this.positives++}
        });
      }
    }
    if(guiElement.left_of != null){
      this.constraints ++;
      // console.log(guiElement.type + ' muss links von einem bestimmten Element liegen');
      let x1 = elementPos.x+this.elementWidth(guiElement)+rightPadding;
      let y1 = elementPos.y;
      let x2 = x1;
      let y2 = y1+this.elementHeight(guiElement)-1;

      if( x1 >= 0 && x2 >= 0 && x1 <= this.guiZone.length-1 && x2 <= this.guiZone.length-1
       && y1 >= 0 && y2 >= 0 && y1 <= this.guiZone[0].length-1 && y2 <= this.guiZone[0].length-1)
      {
        let elems = this.elementsAt(guiElement, elementPos, x1, y1, x2, y2);
        elems.forEach((elem) => {
          if(elem.id == guiElement.left_of) {this.score += 20; this.positives++}
        });
      }
    }

  }//end besidesCheckGuiElement

  elementsAt(guiElement, elementPos, x1, y1, x2, y2){
    var elements = [];
    let above = true;
    let besides = true;
    let x = x1;
    let y = y1;

    while(above || besides){
      // Für GUI-Elemente
      if(guiElement.type != "group"){
        if(this.guiZone[x][y] != null){
          //testen ob bereits gefunden
          var alreadyIn = false;
          elements.forEach((elem) => {
            if(this.equals(elem, this.guiZone[x][y])) alreadyIn = true;
          });
          //wenn nicht, hinzufügen
          if(!alreadyIn)  elements.push(this.guiZone[x][y]);
        }
      }
      // Für Gruppen-Elemente
      else{
        //Jede Gruppe durchgehen
        this.groups.forEach((group) => {
          // und testen, ob das Feld (x,y) in Bereich der Gruppe fällt
          if((x >= group.dimensions.left && x <= group.dimensions.right)
           &&(y >= group.dimensions.top  && y <= group.dimensions.bottom)){
            var alreadyIn = false;
            elements.forEach((elem) => {
              if(this.equals(elem, group)) alreadyIn = true;
            });
            //wenn nicht, hinzufügen
            if(!alreadyIn)  elements.push(group);
          }
        });
      }

      if(x1 != x2) x++;
      if(y1 != y2) y++;
      above = (x >= elementPos.x && x <= elementPos.x+this.elementWidth(guiElement)-1);
      besides = (y >= elementPos.y && y <= elementPos.y+this.elementHeight(guiElement)-1);
    }

    return elements;
  }//end elementsAt



}//end GUI_Evaluator
