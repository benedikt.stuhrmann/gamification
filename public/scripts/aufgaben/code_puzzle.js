class CodePuzzle extends Aufgabe {

	constructor(thema, schwierigkeit) {
		super(thema, schwierigkeit);

		this.SCALE_FACTOR = 1.08;

		this.spieler;

		this.env;

		this.pieces;
		this.correctOrder;
		this.randOrder;

		this.hold;

		this.eventOverlay;
		this.targetOverlay;

		this.puzzles;

		this.puzzle;

		this.solved;

		this.pickup_sfx;
		this.drop_sfx;
		this.error_sfx;
		this.grind_sfx;
		this.gong_sfx;

		this.width = 1024;
		this.height = 576;
	}

	init(params) {
		this.hold = null;
		this.solved = false;
		this.correctOrder = [];
	}

	preload() {
		game.load.image('puzzlepiece_tile', 'assets/aufgaben/codePuzzle/PuzzlePiece_tile.png');
		game.load.image('puzzlepiece_end', 'assets/aufgaben/codePuzzle/PuzzlePiece_end.png');

		game.load.json('fragen', 'fragen/codePuzzle/' + this.thema + '/' + this.schwierigkeit + '.json');


		game.load.audio('pickup', 'assets/audio/code_puzzle/Pickup.mp3');
		game.load.audio('drop', 'assets/audio/code_puzzle/Drop.wav');
		game.load.audio('error', 'assets/audio/code_puzzle/Error.wav');
		game.load.audio('grind', 'assets/audio/code_puzzle/Grind.wav');
		game.load.audio('gong', 'assets/audio/code_puzzle/Gong.wav');

		game.load.spritesheet('teleporter', 'assets/room/pressure_plate_bonus.png', 70, 70);
	}

	create() {
		this.pickup_sfx = game.add.audio('pickup');
		this.drop_sfx = game.add.audio('drop');
		this.error_sfx = game.add.audio('error');
		this.gong_sfx = game.add.audio('gong');
		this.grind_sfx = game.add.audio('grind');
		this.grind_sfx.loop = true;


		this.puzzles = game.cache.getJSON('fragen');
		console.log(this.puzzles);
		if (!this.raum.isSolved) {
			this.puzzle = game.rnd.pick(this.puzzles);
		}



		this.eventOverlay = game.add.graphics();
		this.targetOverlay = game.add.graphics();

		this.env = [this.raum.wallLayer, this.raum.doorLayer, this.raum.doorbowLayer];
		this.spieler = this.raum.spieler;

		this.pieces = game.add.physicsGroup();

		this.raum.aufgabenLayer.add(this.pieces);

		this.createPieces();

		this.spieler.addAction(this.action, this);

		dashboard.setPoints(this.raum.thema,"Codepuzzle");

		game.physics.arcade.OVERLAP_BIAS = 20;


		this.raum.deco.setRockRect(0, 140, 140, 1024-140, 576-140);
		this.raum.deco.placeObjects(game.rnd.between(5, 8), 'rocks');

		this.raum.deco.setPlantRect(0, 140, 140, 1024-140, 576-140);
		this.raum.deco.placeObjects(game.rnd.between(2, 5), 'plant');

		this.raum.deco.setPuddleRect(0, 140, 140, 1024-140, 576-140);
		this.raum.deco.placeObjects(game.rnd.between(0, 2), 'puddle');

		this.showQuestion();

	}

	render() {
		this.pieces.forEach(piece => {
			game.debug.body(piece, 'rgba(0,255,0,0.5)');
		});

	}

	createPieces() {

		if (!this.raum.isSolved) {
			this.puzzle.pieces.forEach(caption => {
				this.correctOrder.push(this.createPiece(0, 0, caption));
			});
			this.randOrder = this.correctOrder.slice();
			while (this.correctOrder.equals(this.randOrder))
				this.randOrder.shuffle();
			let margin = 86;
			let stepSize = (this.height - margin * 2) / this.randOrder.length;
			for (let i = 0; i < this.randOrder.length; i++) {
				let piece = this.randOrder[i];
				piece.x = margin + piece.width / 2 + (this.width - margin * 2 - piece.width) * game.rnd.frac();
				piece.y = margin + piece.height / 2 + stepSize * i + (stepSize - piece.height) * game.rnd.frac();
				this.pieces.add(piece);
			}
		} else {

			this.randOrder.forEach((piece, index, arr) => {

				arr[index] = this.pieces.add(this.createPiece(piece.x, piece.y, piece.caption.text));
				arr[index].tint = 0xAAFFAA;

			});

		}

	}

	createPiece(x, y, caption) {
		var padding = 20;

		var captionTxt = game.add.text(0, 2, caption, {
			font: 'normal 16px copystructnormal', fill: '#000000'
		});
		captionTxt.anchor.set(0.5);



		var piece = game.add.tileSprite(0, 0, captionTxt.width + padding * 2, 48, 'puzzlepiece_tile');
		piece.anchor.set(.5);
		var rand = game.rnd.realInRange(0, 256);
		piece.tilePosition.x = rand;
		piece.caption = captionTxt;

		/*var shadow = game.add.tileSprite(0, 0, piece.width, piece.height, 'puzzlepiece_tile');
		shadow.anchor.set(.5);
		shadow.tint = 0x000000;
		shadow.alpha = .33;
		shadow.tilePosition.x = rand;*/

		var leftEnd = game.add.sprite(-piece.width / 2 + 4, 0, 'puzzlepiece_end');
		leftEnd.anchor.set(.5);
		leftEnd.scale.x = game.rnd.sign();
		var rightEnd = game.add.sprite(piece.width / 2 - 4, 0, 'puzzlepiece_end');
		rightEnd.anchor.set(.5);
		rightEnd.scale.x = game.rnd.sign();

		piece.addChild(leftEnd);
		piece.addChild(rightEnd);
		piece.addChild(captionTxt);


		piece.x = x;
		piece.y = y;

		game.physics.arcade.enable(piece);

		piece.body.maxVelocity = 1;

		return piece;
	}

	elevate(piece) {
		piece.width *= this.SCALE_FACTOR;
		piece.height *= this.SCALE_FACTOR;
		piece.tileScale.multiply(this.SCALE_FACTOR, this.SCALE_FACTOR);
		piece.body.width *= this.SCALE_FACTOR;
		piece.body.height *= this.SCALE_FACTOR;
		piece.children.forEach((el) => {
			el.scale.multiply(this.SCALE_FACTOR, this.SCALE_FACTOR);
			el.position.multiply(this.SCALE_FACTOR, this.SCALE_FACTOR);
		});

		this.spieler.addChild(piece);
		piece.x -= this.spieler.x;
		piece.y -= this.spieler.y;
		piece.body.velocity = new Phaser.Point(0, 0);
		piece.body.immovable = true;

	}

	ground(piece) {
		piece.width /= this.SCALE_FACTOR;
		piece.height /= this.SCALE_FACTOR;
		piece.tileScale.divide(this.SCALE_FACTOR, this.SCALE_FACTOR);
		piece.body.width /= this.SCALE_FACTOR;
		piece.body.height /= this.SCALE_FACTOR;
		piece.children.forEach((el) => {
			el.scale.divide(this.SCALE_FACTOR, this.SCALE_FACTOR);
			el.position.divide(this.SCALE_FACTOR, this.SCALE_FACTOR);
		});
		this.pieces.add(piece);

		piece.x += this.spieler.x;
		piece.y += this.spieler.y;
		piece.body.immovable = false;
	}

	update() {
		if(this.teleporter != null) this.teleporter.listen((this.thema+'BonusRun'), null, null, this.teleporter);
		else if(this.raum.dungeonCompleted)	this.teleporter = new Pressure_Plate(200, 200, 'teleporter', this.raum);

		this.targetOverlay.clear();

		game.physics.arcade.collide(this.pieces, this.env.concat([this.raum.spielerLayer]));
		game.physics.arcade.collide(this.pieces);

		var piecesMoving = false;

		// Slow down pieces
		this.pieces.forEach((piece) => {
			piecesMoving |= piece.body.velocity.getMagnitude() > 10;
			if (piece != this.hold) piece.body.velocity.multiply(.8, .8);
		});


		// Play sounds
		if (this.grind_sfx.isDecoded) {
			if (piecesMoving) {
				this.grind_sfx.isFadingOut = false;
				if (!this.grind_sfx.isPlaying) this.grind_sfx.fadeIn(200);
			} else if (!this.grind_sfx.isFadingOut) {
				this.grind_sfx.fadeOut(400);
				this.grind_sfx.isFadingOut = true;
			}
		}

		// Draw target overlay
		let firstFound = false;
		if (!this.hold) game.physics.arcade.overlap(this.spieler.grabArea, this.pieces, (spieler, piece) => {
			if (firstFound) return;
			this.targetOverlay.lineStyle(3, 0xFFFFFF, .2);
			this.targetOverlay.drawRect(piece.left, piece.top, piece.width, piece.height);
			firstFound = true;
		});


		this.evaluate();

	}

	action() {
		if (!this.hold) {
			game.physics.arcade.overlap(this.spieler.grabArea, this.pieces, (spieler, piece) => {
				if (this.hold != null) return;
				this.elevate(piece);
				this.hold = piece;
				this.pickup_sfx.play();
			});
		} else {
			// Draw obstacle
			let overlapping = game.physics.arcade.overlap(
				this.hold, this.env.concat([this.pieces]), (hold, obs) => {
					this.eventOverlay.beginFill(0xFF0000, .1);
					this.eventOverlay.drawRect(obs.left, obs.top, obs.width, obs.height);
					this.eventOverlay.endFill();
					setTimeout(() => {
						this.eventOverlay.clear();
					}, 200);
				}
			);
			if (!overlapping) {
				this.ground(this.hold);
				this.hold = null;
				this.drop_sfx.play();
			} else this.error_sfx.play();
		}
	}

	evaluate() {
		if (this.solved || this.correctOrder.length < 2 || this.hold) return;

		if (this.isSolved()) {
			this.gong_sfx.play();

			this.pieces.forEach(piece => {
				piece.tint = 0xAAFFAA;
			});
			dashboard.updatePoints(20);

             dashboard.updateRoom();
            	this.raum.openDoors();
			this.solved = true;
		}
	}


	isSolved() {
		let lastPiece = this.correctOrder[0];
		for (let i = 1; i < this.correctOrder.length; i++) {
			let piece = this.correctOrder[i];
			if (lastPiece.y > piece.y) return false;
			lastPiece = piece;
		}

		return true;
	}

	shutdown() {
		this.grind_sfx.stop();
	}

	showQuestion() {
		    var popup = game.add.text(
				(game.width+this.raum.width_offset)/2,
				(game.height/2)-50,
				this.puzzle.frage,
				{
					font: "25px Verdana",
					fill: "#ffffff",
					align: "center",
					wordWrapWidth: 300,
		    	}
			);

            popup.anchor.set(0.5);
		    popup.alpha = 0;

            game.add.tween(popup).to({alpha: 1}, 800, null, true, 0, 0, false);
		    game.add.tween(popup).to({y: (game.height/2)}, 1000, null, true, 0, 0, false);
			setTimeout(
				() => game.add.tween(popup).to({alpha: 0}, 800).start(),
				4000
			);
	}

}








Array.prototype.equals = function (array) {
	if (!array)
		return false;

	if (this.length != array.length)
		return false;

	for (var i = 0, l = this.length; i < l; i++) {
		if (this[i] instanceof Array && array[i] instanceof Array) {
			if (!this[i].equals(array[i]))
				return false;
		} else if (this[i] != array[i]) {
			return false;
		}
	}
	return true;
}
Object.defineProperty(Array.prototype, "equals", {
	enumerable: false
});







Array.prototype.shuffle = function () {
	var currentIndex = this.length,
		temporaryValue, randomIndex;
	while (0 !== currentIndex) {
		randomIndex = Math.floor(Math.random() * currentIndex);
		currentIndex -= 1;
		temporaryValue = this[currentIndex];
		this[currentIndex] = this[randomIndex];
		this[randomIndex] = temporaryValue;
	}
}
Object.defineProperty(Array.prototype, "shuffle", {
	enumerable: false
});
