class Question{
  constructor(question, answerA, answerB, answerC){
    this.questionText = question;
    this.answers = [answerA, answerB, answerC];
    this.correctAnswer = 0; //immer die erste Antwort, die beim erstellen angegeben wird, ist die richtige
  }

  getQuestion(){
    return this.questionText;
  }

  getAnswers(){
    return this.answers;
  }

  getAnswer(id){
    return this.answers[id];
  }

  getCorrectAnswer(){
    return this.correctAnswer;
  }

}
